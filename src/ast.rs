use std::{borrow::Cow, fmt::Display, path::PathBuf, rc::Rc};

use num::BigInt;
use owo_colors::OwoColorize;

use crate::tt::{
    Identifier, Keyword, Literal, PeekUntilError, PrivateIdentifier, Punctuator, Span, Token,
    TokenTree, TokenWithSpan,
};

macro_rules! handle_adj_token {
    ($tt:ident, $res:ident, $res_expr:ident, $span_name:ident, $(($t:expr, $op:expr)),+) => {
        while let Some($res_expr) = $res {
            match $tt.peek_next() {
                $(Some(TokenWithSpan { token, span }) if token == $t => {
                    #[allow(unused)]
                    let $span_name: Span = span;
                    $op
                },)+
                x => {
                    if x.is_some() {
                        $tt.unpeek(1);
                    }

                    $res = Some($res_expr);
                    break;
                },
            }
        }
    };
    ($tt:ident, $span_name:ident, $(($t:expr, $op:expr)),+) => {
        loop {
            match $tt.peek_next() {
                $(Some(TokenWithSpan { token, span }) if token == $t => {
                    let $span_name: Span = span;
                    $op
                },)+
                x => {
                    if x.is_some() {
                        $tt.unpeek(1);
                    }

                    break;
                },
            }
        }
    };
}

macro_rules! expect_token {
    ($t:ident, $token:expr) => {
        match $t.peek_next() {
            Some(TokenWithSpan { token, span }) if token == $token => span,
            Some(TokenWithSpan { span, .. }) => {
                $t.unpeek(1);
                return Err(NodeParseError::MissingExpectedToken($token).span(
                    $t.file_path.clone(),
                    $t.source.clone(),
                    span,
                ));
            }
            None => {
                return Err(NodeParseError::MissingExpectedToken($token)
                    .spanless($t.file_path.clone(), $t.source.clone()))
            }
        }
    };
}

macro_rules! peek_identifier {
    ($t:ident) => {
        match $t.peek_next() {
            Some(TokenWithSpan {
                token: Token::Identifier(i),
                ..
            }) => i,
            Some(TokenWithSpan { span, .. }) => {
                $t.unpeek(1);
                return Err(NodeParseError::MissingIdentifier.span(
                    $t.file_path.clone(),
                    $t.source.clone(),
                    span,
                ));
            }
            None => {
                return Err(NodeParseError::MissingIdentifier
                    .spanless($t.file_path.clone(), $t.source.clone()));
            }
        }
    };
}

#[derive(Debug, Clone, PartialEq)]
pub struct NodeParseErrorWithSpan {
    file_path: Option<Rc<PathBuf>>,
    source: Rc<String>,
    pub error: NodeParseError,
    pub span: Option<Span>,
}

impl Display for NodeParseErrorWithSpan {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self.span {
            Some(span) => {
                let mut line = 1;
                let mut chars_before_line_start = 0;
                let mut end_of_line = 0;
                let mut current_line_chars = 0;
                for (iter, c) in self.source.chars().enumerate() {
                    if iter >= span.source_start {
                        if c == '\n' {
                            break;
                        }
                        end_of_line += 1;
                    } else {
                        current_line_chars += 1;
                        end_of_line += 1;
                        if c == '\n' {
                            line += 1;
                            chars_before_line_start += current_line_chars;
                            current_line_chars = 0;
                        }
                    }
                }
                let pstr_start = span
                    .source_start
                    .checked_sub(40)
                    .unwrap_or_default()
                    .max(chars_before_line_start);
                let pstr_end = (span.source_end + 10)
                    .min(end_of_line)
                    .min(self.source.len());

                let pstr = &self.source.as_str()[pstr_start..pstr_end];
                let mut nl = String::from(" ");

                for _ in 0..(span.source_start - pstr_start) {
                    nl += " ";
                }

                for _ in span.source_start..span.source_end {
                    nl += "^".red().bold().to_string().as_str();
                }

                let error_str = self.error.to_string();

                let mut line_prefix = String::from(" ");

                for _ in 0..line.to_string().len() {
                    line_prefix += " ";
                }

                line_prefix += " |".bold().purple().to_string().as_str();

                let col = span.source_start + 1 - chars_before_line_start;

                let file_path_line = match self.file_path.as_ref().map(|x| x.as_os_str().to_str()) {
                    Some(Some(file_path)) => {
                        format!("{}: {}:{}:{}\n", "File".bold(), file_path, line, col)
                    }
                    _ => String::new(),
                };

                f.write_fmt(format_args!(
                    "{}\n{}{}: {}\n{}: {}\n{}\n {} {} {}\n{}{} {}",
                    "Parsing Error".bold().red(),
                    file_path_line,
                    "Line".bold(),
                    line,
                    "Column".bold(),
                    col,
                    line_prefix,
                    line.bold().purple(),
                    "|".bold().purple(),
                    pstr,
                    line_prefix,
                    nl,
                    error_str.red().bold()
                ))
            }
            None => self.error.fmt(f),
        }
    }
}

impl std::error::Error for NodeParseErrorWithSpan {}

impl NodeParseErrorWithSpan {
    pub fn new(
        file_path: Option<Rc<PathBuf>>,
        source: Rc<String>,
        error: NodeParseError,
        span: Option<Span>,
    ) -> Self {
        Self {
            file_path,
            source,
            error,
            span,
        }
    }
}

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub enum PropertyNameValue {
    /// Any valid identifier, even if supplied as a string
    Identifier(Identifier),
    /// Non-valid identifiers, have to be provided by string
    String(Rc<String>),
    /// All numbers without a decimal place
    Numeric(BigInt),
}

impl PropertyNameValue {
    pub fn normalise(&self) -> Cow<'_, Self> {
        match self {
            PropertyNameValue::String(s) => Cow::Owned(match Identifier::new(s.clone()) {
                Some(identifier) => PropertyNameValue::Identifier(identifier),
                None => PropertyNameValue::String(s.clone()),
            }),
            PropertyNameValue::Numeric(_) | PropertyNameValue::Identifier(_) => Cow::Borrowed(self),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct ArrayLiteral {
    pub items: Vec<Rc<AssignmentExpression>>,
}

impl NodeParse for ArrayLiteral {
    fn node_parse(token_tree: &mut TokenTree) -> Result<Self, NodeParseErrorWithSpan> {
        let mut items = Vec::new();

        while let Some(expr) = AssignmentExpression::node_parse_opt(token_tree)? {
            items.push(Rc::new(expr));

            if !token_tree.peek_is_next(&Token::Punctuator(Punctuator::Comma)) {
                break;
            }
        }

        expect_token!(
            token_tree,
            Token::Punctuator(Punctuator::RightSquareBracket)
        );
        Ok(Self { items })
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct ObjectLiteral {
    pub items: Vec<ObjectLiteralItem>,
}

#[derive(Debug, Clone, PartialEq)]
pub enum ObjectLiteralItem {
    Identifier(Identifier),
    // This isn't really correct but it works.
    Assignment(Rc<PrimaryExpression>, Rc<AssignmentExpression>),
    ComputedPropertyNameAssignment(Rc<AssignmentExpression>, Rc<AssignmentExpression>),
}

impl NodeParseOpt for ObjectLiteralItem {
    fn node_parse_opt(token_tree: &mut TokenTree) -> Result<Option<Self>, NodeParseErrorWithSpan> {
        let next = match token_tree.peek_next() {
            Some(t) => t,
            None => return Ok(None),
        };

        let name = match next.token {
            Token::Identifier(ident) => {
                if !token_tree.peek_is_next(&Token::Punctuator(Punctuator::Colon)) {
                    return Ok(Some(Self::Identifier(ident)));
                } else {
                    token_tree.unpeek(1);
                    Rc::new(PrimaryExpression::Identifier(ident))
                }
            }
            Token::Literal(Literal::String(string_literal)) => Rc::new(PrimaryExpression::Value(
                AstValue::String(Rc::new(string_literal)),
            )),
            Token::Literal(Literal::Numeric(num_literal)) => Rc::new(PrimaryExpression::Value(
                AstValue::String(Rc::new(num_literal.to_string())),
            )),
            Token::Literal(Literal::BigInt(bigint_literal)) => Rc::new(PrimaryExpression::Value(
                AstValue::String(Rc::new(bigint_literal.to_string())),
            )),
            Token::Punctuator(Punctuator::LeftSquareBracket) => {
                let tokens = token_tree
                    .peek_until(
                        &Token::Punctuator(Punctuator::RightSquareBracket),
                        Some(&Token::Punctuator(Punctuator::LeftSquareBracket)),
                        &[],
                    )
                    .map_err(|_| {
                        NodeParseError::ComputedPropertyNameMissingClosingBracket.span(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            next.span,
                        )
                    })?;

                let mut sub_tt = TokenTree::new(
                    token_tree.file_path.clone(),
                    token_tree.source.clone(),
                    tokens,
                );
                let ls = Rc::new(
                    AssignmentExpression::node_parse_opt(&mut sub_tt)?.ok_or_else(|| {
                        NodeParseError::ComputedPropertyNameMissingExpression.span(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            next.span,
                        )
                    })?,
                );

                expect_token!(token_tree, Token::Punctuator(Punctuator::Colon));
                let val_expr =
                    AssignmentExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                        NodeParseError::ObjectItemMissingValue.span(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            next.span,
                        )
                    })?;

                return Ok(Some(Self::ComputedPropertyNameAssignment(
                    ls,
                    Rc::new(val_expr),
                )));
            }
            _ => {
                token_tree.unpeek(1);
                return Ok(None);
            }
        };

        expect_token!(token_tree, Token::Punctuator(Punctuator::Colon));
        let val_expr = AssignmentExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
            NodeParseError::ObjectItemMissingValue.span(
                token_tree.file_path.clone(),
                token_tree.source.clone(),
                next.span,
            )
        })?;

        Ok(Some(Self::Assignment(name, Rc::new(val_expr))))
    }
}

impl NodeParse for ObjectLiteral {
    fn node_parse(token_tree: &mut TokenTree) -> Result<Self, NodeParseErrorWithSpan> {
        let start_span = match token_tree.peek_prev() {
            Some(prev) => prev.span,
            None => token_tree
                .peek_next()
                .map(|x| {
                    token_tree.unpeek(1);
                    x.span
                })
                .unwrap_or_default(),
        };

        let tree = token_tree
            .peek_until(
                &Token::Punctuator(Punctuator::RightBrace),
                Some(&Token::Punctuator(Punctuator::LeftBrace)),
                &[&Token::Punctuator(Punctuator::Semicolon)],
            )
            .map_err(|_| {
                NodeParseError::ObjectMissingClosingBrace.span(
                    token_tree.file_path.clone(),
                    token_tree.source.clone(),
                    start_span,
                )
            })?;
        let mut sub_tt = TokenTree::new(
            token_tree.file_path.clone(),
            token_tree.source.clone(),
            tree,
        );

        let mut items = Vec::new();

        while let Some(item) = ObjectLiteralItem::node_parse_opt(&mut sub_tt)? {
            items.push(item);

            if !sub_tt.peek_is_next(&Token::Punctuator(Punctuator::Comma)) {
                break;
            }
        }

        sub_tt.commit_peek();

        if sub_tt.offset != sub_tt.end {
            let remaining = sub_tt.remaining();
            let span = match remaining.len() {
                0 => unreachable!(),
                1 => remaining[0].span,
                l => Span {
                    source_start: remaining[0].span.source_start,
                    source_end: remaining[l - 1].span.source_end,
                },
            };
            dbg!(items);
            return Err(NodeParseError::LeftoverTokens(remaining.to_vec()).span(
                token_tree.file_path.clone(),
                token_tree.source.clone(),
                span,
            ));
        }

        Ok(Self { items })
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum AstValue {
    /// Only has one value: `undefined`
    Undefined,
    /// Only has one value: `null`
    Null,
    /// Has two possible values:
    ///
    /// 1. `true`
    /// 2. `false`
    Boolean(bool),
    /// UTF-16 String
    String(Rc<String>),
    /// f64
    Numeric(f64),
    /// BigInt
    BigInt(BigInt),
    /// Array
    Array(ArrayLiteral),
    /// Object
    Object(ObjectLiteral),
    /// Function
    Function(Function),
}

impl From<AssignmentExpression> for Expression {
    fn from(e: AssignmentExpression) -> Self {
        Self::Assignment(e)
    }
}

impl From<AstValue> for Expression {
    fn from(e: AstValue) -> Self {
        Self::Assignment(e.into())
    }
}

impl From<AstValue> for AssignmentExpression {
    fn from(val: AstValue) -> Self {
        AssignmentExpression::Conditional(ConditionalExpression::ShortCircuit(
            ShortCircuitExpression::LogicalOr(LogicalOrExpression::LogicalAnd(
                LogicalAndExpression::BitwiseOr(BitwiseOrExpression::BitwiseXor(
                    BitwiseXorExpression::BitwiseAnd(BitwiseAndExpression::Equality(
                        EqualityExpression::Relational(RelationalExpression::Shift(
                            ShiftExpression::Additive(AdditiveExpression::Multiplicative(
                                MultiplicativeExpression::Exponentiation(
                                    ExponentiationExpression::Unary(UnaryExpression::Update(
                                        UpdateExpression::LeftSide(LeftSideExpression::New(
                                            NewExpression::Member(MemberExpression::Primary(
                                                PrimaryExpression::Value(val),
                                            )),
                                        )),
                                    )),
                                ),
                            )),
                        )),
                    )),
                )),
            )),
        ))
    }
}

impl From<Literal> for AstValue {
    fn from(literal: Literal) -> Self {
        match literal {
            Literal::Undefined => Self::Undefined,
            Literal::Null => Self::Null,
            Literal::Boolean(b) => Self::Boolean(b),
            Literal::String(s) => Self::String(Rc::new(s)),
            Literal::Numeric(value) => Self::Numeric(value),
            Literal::BigInt(value) => Self::BigInt(value),
            Literal::Regex => unimplemented!(),
            Literal::TemplateString => unimplemented!(),
        }
    }
}

impl Default for AstValue {
    fn default() -> Self {
        Self::Undefined
    }
}

impl AstValue {
    pub const UNDEFINED_STR: &'static str = "undefined";
    pub const NULL_STR: &'static str = "null";
    pub const BOOLEAN_STR: &'static str = "boolean";
    pub const STRING_STR: &'static str = "string";
    pub const NUMERIC_STR: &'static str = "number";
    pub const BIGINT_STR: &'static str = "bigint";
    pub const OBJECT_STR: &'static str = "object";
    pub const FUNCTION_STR: &'static str = "function";

    pub fn type_as_str(&self) -> &'static str {
        match self {
            AstValue::Undefined => Self::UNDEFINED_STR,
            AstValue::Null => Self::NULL_STR,
            AstValue::Boolean(_) => Self::BOOLEAN_STR,
            AstValue::String(_) => Self::STRING_STR,
            AstValue::Numeric(_) => Self::NUMERIC_STR,
            AstValue::BigInt(_) => Self::BIGINT_STR,
            AstValue::Object(_) => Self::OBJECT_STR,
            AstValue::Function(_) => Self::FUNCTION_STR,
            AstValue::Array(_) => Self::OBJECT_STR,
        }
    }

    pub fn is_undefined(&self) -> bool {
        matches!(self, Self::Undefined)
    }

    pub fn is_null(&self) -> bool {
        matches!(self, Self::Null)
    }

    pub fn is_boolean(&self) -> bool {
        matches!(self, Self::Boolean(_))
    }

    pub fn is_string(&self) -> bool {
        matches!(self, Self::String(_))
    }

    pub fn is_bigint(&self) -> bool {
        matches!(self, Self::BigInt(_))
    }

    pub fn is_number(&self) -> bool {
        matches!(self, Self::Numeric(_))
    }

    pub fn is_object(&self) -> bool {
        matches!(self, Self::Object(_))
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct FunctionParameter {
    pub ident: Identifier,
    pub value: Rc<AssignmentExpression>,
}

impl FunctionParameter {
    pub fn new(ident: Identifier, value: Rc<AssignmentExpression>) -> FunctionParameter {
        FunctionParameter { ident, value }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Function {
    pub params: Vec<FunctionParameter>,
    pub body: Block,
}

impl Function {
    pub fn new(params: Vec<FunctionParameter>, body: Block) -> Self {
        Self { params, body }
    }
}

impl NodeParse for Function {
    fn node_parse(token_tree: &mut TokenTree) -> Result<Self, NodeParseErrorWithSpan> {
        let span_open = expect_token!(token_tree, Token::Punctuator(Punctuator::LeftBracket));

        let mut params: Vec<FunctionParameter> = Vec::new();

        loop {
            let (arg_ident, span) = match token_tree.peek_next() {
                Some(TokenWithSpan {
                    token: Token::Identifier(i),
                    span,
                }) => (i, span),
                Some(TokenWithSpan {
                    token: Token::Punctuator(Punctuator::RightBracket),
                    ..
                }) => {
                    token_tree.unpeek(1);
                    break;
                }
                Some(t) => {
                    token_tree.unpeek(1);
                    return Err(NodeParseError::MissingIdentifier.span(
                        token_tree.file_path.clone(),
                        token_tree.source.clone(),
                        t.span,
                    ));
                }
                None => {
                    return Err(NodeParseError::MissingIdentifier
                        .spanless(token_tree.file_path.clone(), token_tree.source.clone()));
                }
            };

            if params.iter().any(|arg| arg.ident == arg_ident) {
                return Err(
                    NodeParseError::DuplicateFunctionParameterIdent(arg_ident).span(
                        token_tree.file_path.clone(),
                        token_tree.source.clone(),
                        span,
                    ),
                );
            }

            let expr = match token_tree.peek_next() {
                Some(TokenWithSpan {
                    token: Token::Punctuator(Punctuator::Assignment),
                    span,
                }) => Some(
                    AssignmentExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                        NodeParseError::AssignmentMissingRsExpression.span(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            span,
                        )
                    })?,
                ),
                None => {
                    return Err(NodeParseError::MissingExpectedToken(Token::Punctuator(
                        Punctuator::RightBracket,
                    ))
                    .span(
                        token_tree.file_path.clone(),
                        token_tree.source.clone(),
                        span_open,
                    ))
                }
                Some(_) => {
                    token_tree.unpeek(1);
                    None
                }
            };

            params.push(FunctionParameter::new(
                arg_ident,
                Rc::new(expr.unwrap_or_else(|| AssignmentExpression::from(AstValue::Undefined))),
            ));

            if !token_tree.peek_is_next(&Token::Punctuator(Punctuator::Comma)) {
                break;
            }
        }

        expect_token!(token_tree, Token::Punctuator(Punctuator::RightBracket));
        let span = expect_token!(token_tree, Token::Punctuator(Punctuator::LeftBrace));
        let body = Block::node_parse_opt(token_tree)?.ok_or_else(|| {
            NodeParseError::FunctionMissingBody.span(
                token_tree.file_path.clone(),
                token_tree.source.clone(),
                span,
            )
        })?;

        Ok(Self { params, body })
    }
}

pub trait NodeParse: Sized {
    fn node_parse(token_tree: &mut TokenTree) -> Result<Self, NodeParseErrorWithSpan>;
}

pub trait NodeParseOpt: Sized {
    fn node_parse_opt(token_tree: &mut TokenTree) -> Result<Option<Self>, NodeParseErrorWithSpan>;
}

#[derive(Debug, Clone, PartialEq, thiserror::Error)]
pub enum NodeParseError {
    #[error("Expected semicolon but it is missing")]
    MissingSemicolon,
    #[error("Expected identifier but it is missing")]
    MissingIdentifier,
    #[error("Expected token {0:?} but it is missing")]
    MissingExpectedToken(Token),
    #[error("Unexpected token found: {0:?}")]
    UnexpectedToken(Token),
    #[error("loop statement missing condition")]
    LoopMissingCondition,
    #[error("loop statement missing body")]
    LoopMissingStatement,
    #[error("`if` statement missing condition")]
    IfMissingCondition,
    #[error("`if` statement missing body")]
    IfMissingStatement,
    #[error("`else` statement missing body")]
    ElseMissingStatement,
    #[error("Declaration is missing an identifier")]
    DeclarationMissingIdent,
    #[error("Comparison is missing an expression on the right side")]
    ComparisonMissingRsExpression,
    #[error("Assignment is missing an expression on the right side")]
    AssignmentMissingRsExpression,
    #[error("Leftover tokens: {0:?}")]
    LeftoverTokens(Vec<TokenWithSpan>),
    #[error("Function arguments contain a duplicate element: {0:?}")]
    DuplicateFunctionParameterIdent(Identifier),
    #[error("Property access using brackets missing expression")]
    PropertyAccessMissingExpression,
    #[error("Property access missing identifiero")]
    PropertyAccessMissingIdent,
    #[error("Grouping operator missing expression")]
    GroupingOperatorMissingExpression,
    #[error("Item in object definition is missing a value")]
    ObjectItemMissingValue,
    #[error("Object definition missing closing brace")]
    ObjectMissingClosingBrace,
    #[error("Computed property name missing closing bracket")]
    ComputedPropertyNameMissingClosingBracket,
    #[error("Computed property name missing expression")]
    ComputedPropertyNameMissingExpression,
    #[error("Function missing body")]
    FunctionMissingBody,
    #[error("Missing expression")]
    MissingExpression,
    #[error("New expression missing right side")]
    NewExpressionMissingRightSide,
    #[error("Prefix update expression missing right side")]
    PrefixUpdateExpressionMissingRightSide,
}

impl NodeParseError {
    pub fn span(
        self,
        file_path: Option<Rc<PathBuf>>,
        source: Rc<String>,
        span: Span,
    ) -> NodeParseErrorWithSpan {
        NodeParseErrorWithSpan::new(file_path, source, self, Some(span))
    }

    pub fn spanless(
        self,
        file_path: Option<Rc<PathBuf>>,
        source: Rc<String>,
    ) -> NodeParseErrorWithSpan {
        NodeParseErrorWithSpan::new(file_path, source, self, None)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ShiftOperator {
    /// `<<`
    LeftShift,
    /// `>>`
    RightShift,
    /// `>>>`
    UnsignedRightShift,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum EqualityOperator {
    /// `==`
    Equal,
    /// `!=`
    NotEqual,
    /// `===`
    StrictEqual,
    /// `!==`
    StrictNotEqual,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum MultiplicativeOperator {
    /// `*`
    Multiplication,
    /// `/`
    Division,
    /// `%`
    Remainder,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum AdditiveOperator {
    /// `+`
    Addition,
    /// `-`
    Subtraction,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum UnaryOperator {
    /// `typeof`
    Typeof,
    /// `+`
    Positive,
    /// `-`
    Negative,
    /// `~`
    BitwiseNot,
    /// `!`
    LogicalNot,
}

#[derive(Debug, Clone, PartialEq)]
pub enum PrimaryExpression {
    This,
    Identifier(Identifier),
    Value(AstValue),
    Function(Option<Identifier>, Function),
    Grouping(Rc<Expression>),
}

impl NodeParseOpt for PrimaryExpression {
    fn node_parse_opt(token_tree: &mut TokenTree) -> Result<Option<Self>, NodeParseErrorWithSpan> {
        let next = match token_tree.peek_next() {
            Some(next) => next,
            None => return Ok(None),
        };

        let opt_res = match next.token {
            Token::Keyword(Keyword::This) => Some(Self::This),
            Token::Identifier(ident) => Some(Self::Identifier(ident)),
            Token::Literal(literal) => Some(Self::Value(literal.into())),
            Token::Keyword(Keyword::Function) => {
                let ident = match token_tree.peek_next().map(|x| x.token) {
                    Some(Token::Identifier(i)) => Some(i),
                    x => {
                        if x.is_some() {
                            token_tree.unpeek(1);
                        }
                        None
                    }
                };
                let func = Function::node_parse(token_tree)?;

                Some(Self::Function(ident, func))
            }
            Token::Punctuator(Punctuator::LeftBracket) => {
                let tokens = token_tree
                    .peek_until(
                        &Token::Punctuator(Punctuator::RightBracket),
                        Some(&Token::Punctuator(Punctuator::LeftBracket)),
                        &[&Token::Punctuator(Punctuator::Semicolon)],
                    )
                    .map_err(|_| {
                        NodeParseError::MissingExpectedToken(Token::Punctuator(
                            Punctuator::RightBracket,
                        ))
                        .span(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            next.span,
                        )
                    })?;

                let mut sub_tt = TokenTree::new(
                    token_tree.file_path.clone(),
                    token_tree.source.clone(),
                    tokens,
                );
                let expr = Expression::node_parse_opt(&mut sub_tt)?.ok_or_else(|| {
                    NodeParseError::GroupingOperatorMissingExpression.span(
                        token_tree.file_path.clone(),
                        token_tree.source.clone(),
                        next.span,
                    )
                })?;
                Some(Self::Grouping(Rc::new(expr)))
            }
            Token::Punctuator(Punctuator::LeftBrace) => {
                let object_literal = ObjectLiteral::node_parse(token_tree)?;
                Some(Self::Value(AstValue::Object(object_literal)))
            }
            Token::Punctuator(Punctuator::LeftSquareBracket) => {
                let array_literal = ArrayLiteral::node_parse(token_tree)?;
                Some(Self::Value(AstValue::Array(array_literal)))
            }
            _ => {
                token_tree.unpeek(1);
                None
            }
        };

        Ok(opt_res)
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum MemberExpression {
    Primary(PrimaryExpression),
    PropertyAccessBrackets(Rc<MemberExpression>, Rc<Expression>),
    PropertyAccess(Rc<MemberExpression>, Identifier),
    PrivatePropertyAccess(Rc<MemberExpression>, PrivateIdentifier),
}

impl NodeParseOpt for MemberExpression {
    fn node_parse_opt(token_tree: &mut TokenTree) -> Result<Option<Self>, NodeParseErrorWithSpan> {
        if let Some(p) = PrimaryExpression::node_parse_opt(token_tree)? {
            let mut res = Self::Primary(p);

            handle_adj_token!(
                token_tree,
                _span,
                (Token::Punctuator(Punctuator::LeftSquareBracket), {
                    let expr = Expression::node_parse_opt(token_tree)?.ok_or_else(|| {
                        NodeParseErrorWithSpan::new(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            NodeParseError::PropertyAccessMissingExpression,
                            token_tree.peek_prev().map(|x| x.span),
                        )
                    })?;
                    expect_token!(
                        token_tree,
                        Token::Punctuator(Punctuator::RightSquareBracket)
                    );
                    res = Self::PropertyAccessBrackets(Rc::new(res), Rc::new(expr));
                }),
                (Token::Punctuator(Punctuator::Dot), {
                    match token_tree.peek_next() {
                        Some(TokenWithSpan {
                            token: Token::Identifier(ident),
                            ..
                        }) => {
                            res = Self::PropertyAccess(Rc::new(res), ident);
                        }
                        Some(TokenWithSpan {
                            token: Token::PrivateIdentifier(private_ident),
                            ..
                        }) => res = Self::PrivatePropertyAccess(Rc::new(res), private_ident),
                        x => {
                            if x.is_some() {
                                token_tree.unpeek(1);
                            }

                            return Err(NodeParseErrorWithSpan::new(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                NodeParseError::PropertyAccessMissingIdent,
                                token_tree.peek_prev().map(|x| x.span),
                            ));
                        }
                    }
                })
            );

            Ok(Some(res))
        } else {
            Ok(None)
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum NewExpression {
    Member(MemberExpression),
    New(Rc<NewExpression>),
}

impl NodeParseOpt for NewExpression {
    fn node_parse_opt(token_tree: &mut TokenTree) -> Result<Option<Self>, NodeParseErrorWithSpan> {
        if let Some(member) = MemberExpression::node_parse_opt(token_tree)? {
            Ok(Some(Self::Member(member)))
        } else {
            let next = match token_tree.peek_next() {
                Some(n) => n,
                None => return Ok(None),
            };

            match next {
                TokenWithSpan {
                    token: Token::Keyword(Keyword::New),
                    span,
                } => {
                    let right_side = Self::node_parse_opt(token_tree)?.ok_or_else(|| {
                        NodeParseError::NewExpressionMissingRightSide.span(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            span,
                        )
                    })?;

                    Ok(Some(Self::New(Rc::new(right_side))))
                }
                _ => {
                    token_tree.unpeek(1);
                    Ok(None)
                }
            }
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum CallExpression {
    CallMember(MemberExpression, Vec<Rc<AssignmentExpression>>),
    Call(Rc<Self>, Vec<Rc<AssignmentExpression>>),
    PropertyAccessBracketsCall(Rc<Self>, Rc<Expression>),
    PropertyAccessCall(Rc<Self>, Identifier),
    PrivatePropertyAccessCall(Rc<Self>, PrivateIdentifier),
}

impl NodeParseOpt for CallExpression {
    fn node_parse_opt(token_tree: &mut TokenTree) -> Result<Option<Self>, NodeParseErrorWithSpan> {
        let original_offset = token_tree.offset;
        let original_peek_offset = token_tree.peek_offset;
        if let Some(member) = MemberExpression::node_parse_opt(token_tree)? {
            if token_tree.peek_is_next(&Token::Punctuator(Punctuator::LeftBracket)) {
                let mut params: Vec<Rc<AssignmentExpression>> = Vec::new();

                while let Some(expr) = AssignmentExpression::node_parse_opt(token_tree)? {
                    params.push(Rc::new(expr));

                    if !token_tree.peek_is_next(&Token::Punctuator(Punctuator::Comma)) {
                        break;
                    }
                }

                expect_token!(token_tree, Token::Punctuator(Punctuator::RightBracket));
                let mut res = Some(Self::CallMember(member, params));

                handle_adj_token!(
                    token_tree,
                    res,
                    res_val,
                    span,
                    (Token::Punctuator(Punctuator::LeftBracket), {
                        let mut params: Vec<Rc<AssignmentExpression>> = Vec::new();

                        while let Some(expr) = AssignmentExpression::node_parse_opt(token_tree)? {
                            params.push(Rc::new(expr));

                            if !token_tree.peek_is_next(&Token::Punctuator(Punctuator::Comma)) {
                                break;
                            }
                        }

                        expect_token!(token_tree, Token::Punctuator(Punctuator::RightBracket));
                        res = Some(Self::Call(Rc::new(res_val), params));
                    }),
                    (Token::Punctuator(Punctuator::LeftSquareBracket), {
                        let expr = Expression::node_parse_opt(token_tree)?.ok_or_else(|| {
                            NodeParseError::PropertyAccessMissingExpression.span(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                span,
                            )
                        })?;
                        expect_token!(
                            token_tree,
                            Token::Punctuator(Punctuator::RightSquareBracket)
                        );
                        res = Some(Self::PropertyAccessBracketsCall(
                            Rc::new(res_val),
                            Rc::new(expr),
                        ));
                    }),
                    (Token::Punctuator(Punctuator::Dot), {
                        match token_tree.peek_next().map(|x| x.token) {
                            Some(Token::Identifier(ident)) => {
                                res = Some(Self::PropertyAccessCall(Rc::new(res_val), ident));
                            }
                            Some(Token::PrivateIdentifier(private_ident)) => {
                                res = Some(Self::PrivatePropertyAccessCall(
                                    Rc::new(res_val),
                                    private_ident,
                                ));
                            }
                            x => {
                                if x.is_some() {
                                    token_tree.unpeek(1);
                                }
                                return Err(NodeParseError::PropertyAccessMissingIdent.span(
                                    token_tree.file_path.clone(),
                                    token_tree.source.clone(),
                                    span,
                                ));
                            }
                        }
                    })
                );

                Ok(res)
            } else {
                token_tree.offset = original_offset;
                token_tree.peek_offset = original_peek_offset;
                Ok(None)
            }
        } else {
            Ok(None)
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum LeftSideExpression {
    New(NewExpression),
    Call(CallExpression),
}

impl NodeParseOpt for LeftSideExpression {
    fn node_parse_opt(token_tree: &mut TokenTree) -> Result<Option<Self>, NodeParseErrorWithSpan> {
        if let Some(call) = CallExpression::node_parse_opt(token_tree)? {
            Ok(Some(Self::Call(call)))
        } else if let Some(new) = NewExpression::node_parse_opt(token_tree)? {
            Ok(Some(Self::New(new)))
        } else {
            Ok(None)
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum UpdateExpression {
    LeftSide(LeftSideExpression),
    SuffixIncrement(LeftSideExpression),
    SuffixDecrement(LeftSideExpression),
    PrefixIncrement(Rc<UnaryExpression>),
    PrefixDecrement(Rc<UnaryExpression>),
}

impl NodeParseOpt for UpdateExpression {
    fn node_parse_opt(token_tree: &mut TokenTree) -> Result<Option<Self>, NodeParseErrorWithSpan> {
        if let Some(left_side) = LeftSideExpression::node_parse_opt(token_tree)? {
            let expr = match token_tree.peek_next().map(|x| x.token) {
                Some(Token::Punctuator(Punctuator::Increment)) => Self::SuffixIncrement(left_side),
                Some(Token::Punctuator(Punctuator::Decrement)) => Self::SuffixDecrement(left_side),
                Some(_) => {
                    token_tree.unpeek(1);
                    Self::LeftSide(left_side)
                }
                None => Self::LeftSide(left_side),
            };

            Ok(Some(expr))
        } else {
            let (increment, span) = match token_tree.peek_next() {
                Some(TokenWithSpan {
                    token: Token::Punctuator(Punctuator::Increment),
                    span,
                }) => (true, span),
                Some(TokenWithSpan {
                    token: Token::Punctuator(Punctuator::Decrement),
                    span,
                }) => (false, span),
                Some(_) => {
                    token_tree.unpeek(1);
                    return Ok(None);
                }
                None => return Ok(None),
            };

            let right_side = UnaryExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                NodeParseError::PrefixUpdateExpressionMissingRightSide.span(
                    token_tree.file_path.clone(),
                    token_tree.source.clone(),
                    span,
                )
            })?;

            Ok(Some(match increment {
                true => Self::PrefixIncrement(Rc::new(right_side)),
                false => Self::PrefixDecrement(Rc::new(right_side)),
            }))
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum UnaryExpression {
    Update(UpdateExpression),
    Unary(UnaryOperator, Rc<Self>),
}

impl NodeParseOpt for UnaryExpression {
    fn node_parse_opt(token_tree: &mut TokenTree) -> Result<Option<Self>, NodeParseErrorWithSpan> {
        if let Some(update) = UpdateExpression::node_parse_opt(token_tree)? {
            Ok(Some(Self::Update(update)))
        } else if let Some(span) = token_tree.peek_is_next_span(&Token::Keyword(Keyword::Typeof)) {
            Ok(Some(Self::Unary(
                UnaryOperator::Typeof,
                Rc::new(Self::node_parse_opt(token_tree)?.ok_or_else(|| {
                    NodeParseError::MissingExpression.span(
                        token_tree.file_path.clone(),
                        token_tree.source.clone(),
                        span,
                    )
                })?),
            )))
        } else if let Some(span) =
            token_tree.peek_is_next_span(&Token::Punctuator(Punctuator::Addition))
        {
            Ok(Some(Self::Unary(
                UnaryOperator::Positive,
                Rc::new(Self::node_parse_opt(token_tree)?.ok_or_else(|| {
                    NodeParseError::MissingExpression.span(
                        token_tree.file_path.clone(),
                        token_tree.source.clone(),
                        span,
                    )
                })?),
            )))
        } else if let Some(span) =
            token_tree.peek_is_next_span(&Token::Punctuator(Punctuator::Subtraction))
        {
            Ok(Some(Self::Unary(
                UnaryOperator::Negative,
                Rc::new(Self::node_parse_opt(token_tree)?.ok_or_else(|| {
                    NodeParseError::MissingExpression.span(
                        token_tree.file_path.clone(),
                        token_tree.source.clone(),
                        span,
                    )
                })?),
            )))
        } else if let Some(span) =
            token_tree.peek_is_next_span(&Token::Punctuator(Punctuator::BitwiseNot))
        {
            Ok(Some(Self::Unary(
                UnaryOperator::BitwiseNot,
                Rc::new(Self::node_parse_opt(token_tree)?.ok_or_else(|| {
                    NodeParseError::MissingExpression.span(
                        token_tree.file_path.clone(),
                        token_tree.source.clone(),
                        span,
                    )
                })?),
            )))
        } else if let Some(span) =
            token_tree.peek_is_next_span(&Token::Punctuator(Punctuator::LogicalNot))
        {
            Ok(Some(Self::Unary(
                UnaryOperator::LogicalNot,
                Rc::new(Self::node_parse_opt(token_tree)?.ok_or_else(|| {
                    NodeParseError::MissingExpression.span(
                        token_tree.file_path.clone(),
                        token_tree.source.clone(),
                        span,
                    )
                })?),
            )))
        } else {
            Ok(None)
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum ExponentiationExpression {
    Unary(UnaryExpression),
    Exponent(UpdateExpression, Rc<Self>),
}

impl NodeParseOpt for ExponentiationExpression {
    fn node_parse_opt(token_tree: &mut TokenTree) -> Result<Option<Self>, NodeParseErrorWithSpan> {
        if let Some(update) = UpdateExpression::node_parse_opt(token_tree)? {
            if let Some(span) =
                token_tree.peek_is_next_span(&Token::Punctuator(Punctuator::Exponentiation))
            {
                Ok(Some(Self::Exponent(
                    update,
                    Rc::new(Self::node_parse_opt(token_tree)?.ok_or_else(|| {
                        NodeParseError::MissingExpression.span(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            span,
                        )
                    })?),
                )))
            } else {
                Ok(Some(Self::Unary(UnaryExpression::Update(update))))
            }
        } else if let Some(unary) = UnaryExpression::node_parse_opt(token_tree)? {
            Ok(Some(Self::Unary(unary)))
        } else {
            Ok(None)
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum MultiplicativeExpression {
    Exponentiation(ExponentiationExpression),
    Multiplicative(Rc<Self>, MultiplicativeOperator, ExponentiationExpression),
}

impl NodeParseOpt for MultiplicativeExpression {
    fn node_parse_opt(token_tree: &mut TokenTree) -> Result<Option<Self>, NodeParseErrorWithSpan> {
        if let Some(exp) = ExponentiationExpression::node_parse_opt(token_tree)? {
            let mut res = Self::Exponentiation(exp);

            handle_adj_token!(
                token_tree,
                span,
                (Token::Punctuator(Punctuator::Multiplication), {
                    res = Self::Multiplicative(
                        Rc::new(res),
                        MultiplicativeOperator::Multiplication,
                        ExponentiationExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                            NodeParseError::MissingExpression.span(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                span,
                            )
                        })?,
                    );
                }),
                (Token::Punctuator(Punctuator::Division), {
                    res = Self::Multiplicative(
                        Rc::new(res),
                        MultiplicativeOperator::Division,
                        ExponentiationExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                            NodeParseError::MissingExpression.span(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                span,
                            )
                        })?,
                    );
                }),
                (Token::Punctuator(Punctuator::Remainder), {
                    res = Self::Multiplicative(
                        Rc::new(res),
                        MultiplicativeOperator::Remainder,
                        ExponentiationExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                            NodeParseError::MissingExpression.span(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                span,
                            )
                        })?,
                    );
                })
            );

            Ok(Some(res))
        } else {
            Ok(None)
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum AdditiveExpression {
    Multiplicative(MultiplicativeExpression),
    Additive(Rc<Self>, AdditiveOperator, MultiplicativeExpression),
}

impl NodeParseOpt for AdditiveExpression {
    fn node_parse_opt(token_tree: &mut TokenTree) -> Result<Option<Self>, NodeParseErrorWithSpan> {
        if let Some(mul) = MultiplicativeExpression::node_parse_opt(token_tree)? {
            let mut res = Self::Multiplicative(mul);

            handle_adj_token!(
                token_tree,
                span,
                (Token::Punctuator(Punctuator::Addition), {
                    res = Self::Additive(
                        Rc::new(res),
                        AdditiveOperator::Addition,
                        MultiplicativeExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                            NodeParseError::MissingExpression.span(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                span,
                            )
                        })?,
                    );
                }),
                (Token::Punctuator(Punctuator::Subtraction), {
                    res = Self::Additive(
                        Rc::new(res),
                        AdditiveOperator::Subtraction,
                        MultiplicativeExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                            NodeParseError::MissingExpression.span(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                span,
                            )
                        })?,
                    );
                })
            );

            Ok(Some(res))
        } else {
            Ok(None)
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum ShiftExpression {
    Additive(AdditiveExpression),
    Left(Rc<Self>, AdditiveExpression),
    Right(Rc<Self>, AdditiveExpression),
    RightUnsigned(Rc<Self>, AdditiveExpression),
}

impl NodeParseOpt for ShiftExpression {
    fn node_parse_opt(token_tree: &mut TokenTree) -> Result<Option<Self>, NodeParseErrorWithSpan> {
        if let Some(add) = AdditiveExpression::node_parse_opt(token_tree)? {
            let mut res = Self::Additive(add);

            handle_adj_token!(
                token_tree,
                span,
                (Token::Punctuator(Punctuator::LeftShift), {
                    res = Self::Left(
                        Rc::new(res),
                        AdditiveExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                            NodeParseError::MissingExpression.span(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                span,
                            )
                        })?,
                    );
                }),
                (Token::Punctuator(Punctuator::RightShift), {
                    res = Self::Right(
                        Rc::new(res),
                        AdditiveExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                            NodeParseError::MissingExpression.span(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                span,
                            )
                        })?,
                    );
                }),
                (Token::Punctuator(Punctuator::UnsignedRightShift), {
                    res = Self::RightUnsigned(
                        Rc::new(res),
                        AdditiveExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                            NodeParseError::MissingExpression.span(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                span,
                            )
                        })?,
                    );
                })
            );

            Ok(Some(res))
        } else {
            Ok(None)
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum RelationalExpression {
    Shift(ShiftExpression),
    LessThan(Rc<Self>, ShiftExpression),
    GreaterThan(Rc<Self>, ShiftExpression),
    LessThanEqual(Rc<Self>, ShiftExpression),
    GreaterThanEqual(Rc<Self>, ShiftExpression),
    Instanceof(Rc<Self>, ShiftExpression),
}

impl NodeParseOpt for RelationalExpression {
    fn node_parse_opt(token_tree: &mut TokenTree) -> Result<Option<Self>, NodeParseErrorWithSpan> {
        if let Some(shift) = ShiftExpression::node_parse_opt(token_tree)? {
            let mut res = Self::Shift(shift);

            handle_adj_token!(
                token_tree,
                span,
                (Token::Punctuator(Punctuator::LessThan), {
                    res = Self::LessThan(
                        Rc::new(res),
                        ShiftExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                            NodeParseError::MissingExpression.span(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                span,
                            )
                        })?,
                    );
                }),
                (Token::Punctuator(Punctuator::GreaterThan), {
                    res = Self::GreaterThan(
                        Rc::new(res),
                        ShiftExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                            NodeParseError::MissingExpression.span(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                span,
                            )
                        })?,
                    );
                }),
                (Token::Punctuator(Punctuator::LessThanEqual), {
                    res = Self::LessThanEqual(
                        Rc::new(res),
                        ShiftExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                            NodeParseError::MissingExpression.span(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                span,
                            )
                        })?,
                    );
                }),
                (Token::Punctuator(Punctuator::GreaterThanEqual), {
                    res = Self::GreaterThanEqual(
                        Rc::new(res),
                        ShiftExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                            NodeParseError::MissingExpression.span(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                span,
                            )
                        })?,
                    );
                }),
                (Token::Keyword(Keyword::Instanceof), {
                    res = Self::Instanceof(
                        Rc::new(res),
                        ShiftExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                            NodeParseError::MissingExpression.span(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                span,
                            )
                        })?,
                    );
                })
            );

            Ok(Some(res))
        } else {
            Ok(None)
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum EqualityExpression {
    Relational(RelationalExpression),
    Equality(Rc<Self>, EqualityOperator, RelationalExpression),
}

impl NodeParseOpt for EqualityExpression {
    fn node_parse_opt(token_tree: &mut TokenTree) -> Result<Option<Self>, NodeParseErrorWithSpan> {
        if let Some(rel) = RelationalExpression::node_parse_opt(token_tree)? {
            let mut res = Self::Relational(rel);

            handle_adj_token!(
                token_tree,
                span,
                (Token::Punctuator(Punctuator::Equality), {
                    res = Self::Equality(
                        Rc::new(res),
                        EqualityOperator::Equal,
                        RelationalExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                            NodeParseError::MissingExpression.span(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                span,
                            )
                        })?,
                    );
                }),
                (Token::Punctuator(Punctuator::Inequality), {
                    res = Self::Equality(
                        Rc::new(res),
                        EqualityOperator::NotEqual,
                        RelationalExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                            NodeParseError::MissingExpression.span(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                span,
                            )
                        })?,
                    );
                }),
                (Token::Punctuator(Punctuator::StrictEquality), {
                    res = Self::Equality(
                        Rc::new(res),
                        EqualityOperator::StrictEqual,
                        RelationalExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                            NodeParseError::MissingExpression.span(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                span,
                            )
                        })?,
                    );
                }),
                (Token::Punctuator(Punctuator::StrictInequality), {
                    res = Self::Equality(
                        Rc::new(res),
                        EqualityOperator::StrictNotEqual,
                        RelationalExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                            NodeParseError::MissingExpression.span(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                span,
                            )
                        })?,
                    );
                })
            );

            Ok(Some(res))
        } else {
            Ok(None)
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum BitwiseAndExpression {
    Equality(EqualityExpression),
    BitwiseAnd(Rc<Self>, EqualityExpression),
}

impl NodeParseOpt for BitwiseAndExpression {
    fn node_parse_opt(token_tree: &mut TokenTree) -> Result<Option<Self>, NodeParseErrorWithSpan> {
        if let Some(eq) = EqualityExpression::node_parse_opt(token_tree)? {
            let mut res = Self::Equality(eq);

            handle_adj_token!(
                token_tree,
                span,
                (Token::Punctuator(Punctuator::BitwiseAnd), {
                    res = Self::BitwiseAnd(
                        Rc::new(res),
                        EqualityExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                            NodeParseError::MissingExpression.span(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                span,
                            )
                        })?,
                    )
                })
            );

            Ok(Some(res))
        } else {
            Ok(None)
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum BitwiseXorExpression {
    BitwiseAnd(BitwiseAndExpression),
    BitwiseXor(Rc<Self>, BitwiseAndExpression),
}

impl NodeParseOpt for BitwiseXorExpression {
    fn node_parse_opt(token_tree: &mut TokenTree) -> Result<Option<Self>, NodeParseErrorWithSpan> {
        if let Some(op) = BitwiseAndExpression::node_parse_opt(token_tree)? {
            let mut res = Self::BitwiseAnd(op);

            handle_adj_token!(
                token_tree,
                span,
                (Token::Punctuator(Punctuator::BitwiseXor), {
                    res = Self::BitwiseXor(
                        Rc::new(res),
                        BitwiseAndExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                            NodeParseError::MissingExpression.span(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                span,
                            )
                        })?,
                    )
                })
            );

            Ok(Some(res))
        } else {
            Ok(None)
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum BitwiseOrExpression {
    BitwiseXor(BitwiseXorExpression),
    BitwiseOr(Rc<Self>, BitwiseXorExpression),
}

impl NodeParseOpt for BitwiseOrExpression {
    fn node_parse_opt(token_tree: &mut TokenTree) -> Result<Option<Self>, NodeParseErrorWithSpan> {
        if let Some(op) = BitwiseXorExpression::node_parse_opt(token_tree)? {
            let mut res = Self::BitwiseXor(op);

            handle_adj_token!(
                token_tree,
                span,
                (Token::Punctuator(Punctuator::BitwiseOr), {
                    res = Self::BitwiseOr(
                        Rc::new(res),
                        BitwiseXorExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                            NodeParseError::MissingExpression.span(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                span,
                            )
                        })?,
                    )
                })
            );

            Ok(Some(res))
        } else {
            Ok(None)
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum LogicalAndExpression {
    BitwiseOr(BitwiseOrExpression),
    LogicalAnd(Rc<Self>, BitwiseOrExpression),
}

impl NodeParseOpt for LogicalAndExpression {
    fn node_parse_opt(token_tree: &mut TokenTree) -> Result<Option<Self>, NodeParseErrorWithSpan> {
        if let Some(op) = BitwiseOrExpression::node_parse_opt(token_tree)? {
            let mut res = Self::BitwiseOr(op);

            handle_adj_token!(
                token_tree,
                span,
                (Token::Punctuator(Punctuator::LogicalAnd), {
                    res = Self::LogicalAnd(
                        Rc::new(res),
                        BitwiseOrExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                            NodeParseError::MissingExpression.span(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                span,
                            )
                        })?,
                    )
                })
            );

            Ok(Some(res))
        } else {
            Ok(None)
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum LogicalOrExpression {
    LogicalAnd(LogicalAndExpression),
    LogicalOr(Rc<Self>, LogicalAndExpression),
}

impl NodeParseOpt for LogicalOrExpression {
    fn node_parse_opt(token_tree: &mut TokenTree) -> Result<Option<Self>, NodeParseErrorWithSpan> {
        if let Some(op) = LogicalAndExpression::node_parse_opt(token_tree)? {
            let mut res = Self::LogicalAnd(op);

            handle_adj_token!(
                token_tree,
                span,
                (Token::Punctuator(Punctuator::LogicalOr), {
                    res = Self::LogicalOr(
                        Rc::new(res),
                        LogicalAndExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                            NodeParseError::MissingExpression.span(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                span,
                            )
                        })?,
                    )
                })
            );

            Ok(Some(res))
        } else {
            Ok(None)
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum CoalesceExpression {
    BitwiseOr(BitwiseOrExpression),
    Coalesce(Rc<Self>, BitwiseOrExpression),
}

impl NodeParseOpt for CoalesceExpression {
    fn node_parse_opt(token_tree: &mut TokenTree) -> Result<Option<Self>, NodeParseErrorWithSpan> {
        if let Some(or) = BitwiseOrExpression::node_parse_opt(token_tree)? {
            let mut res = Self::BitwiseOr(or);

            handle_adj_token!(
                token_tree,
                span,
                (Token::Punctuator(Punctuator::NullishCoalescing), {
                    res = Self::Coalesce(
                        Rc::new(res),
                        BitwiseOrExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                            NodeParseError::MissingExpression.span(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                span,
                            )
                        })?,
                    );
                })
            );

            Ok(Some(res))
        } else {
            Ok(None)
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum ShortCircuitExpression {
    LogicalOr(LogicalOrExpression),
    Coalesce(CoalesceExpression),
}

impl NodeParseOpt for ShortCircuitExpression {
    fn node_parse_opt(token_tree: &mut TokenTree) -> Result<Option<Self>, NodeParseErrorWithSpan> {
        let original_offset = token_tree.offset;
        let original_peek_offset = token_tree.peek_offset;
        if let Some(c) = CoalesceExpression::node_parse_opt(token_tree)? {
            if matches!(c, CoalesceExpression::BitwiseOr(_)) {
                token_tree.offset = original_offset;
                token_tree.peek_offset = original_peek_offset;
                let span = token_tree.peek_prev().map(|t| t.span);
                Ok(Some(Self::LogicalOr(
                    LogicalOrExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                        NodeParseErrorWithSpan::new(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            NodeParseError::MissingExpression,
                            span,
                        )
                    })?,
                )))
            } else {
                Ok(Some(Self::Coalesce(c)))
            }
        } else if let Some(or) = LogicalOrExpression::node_parse_opt(token_tree)? {
            Ok(Some(Self::LogicalOr(or)))
        } else {
            Ok(None)
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum ConditionalExpression {
    ShortCircuit(ShortCircuitExpression),
    Conditional(
        ShortCircuitExpression,
        Rc<AssignmentExpression>,
        Rc<AssignmentExpression>,
    ),
}

impl NodeParseOpt for ConditionalExpression {
    fn node_parse_opt(token_tree: &mut TokenTree) -> Result<Option<Self>, NodeParseErrorWithSpan> {
        if let Some(sc_expr) = ShortCircuitExpression::node_parse_opt(token_tree)? {
            if let Some(span) =
                token_tree.peek_is_next_span(&Token::Punctuator(Punctuator::Conditional))
            {
                let left_side = Rc::new(
                    AssignmentExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                        NodeParseError::MissingExpression.span(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            span,
                        )
                    })?,
                );
                expect_token!(token_tree, Token::Punctuator(Punctuator::Colon));
                let right_side = Rc::new(
                    AssignmentExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                        NodeParseError::MissingExpression.span(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            span,
                        )
                    })?,
                );
                Ok(Some(Self::Conditional(sc_expr, left_side, right_side)))
            } else {
                Ok(Some(Self::ShortCircuit(sc_expr)))
            }
        } else {
            Ok(None)
        }
    }
}

#[allow(clippy::large_enum_variant)]
#[derive(Debug, Clone, PartialEq)]
pub enum AssignmentExpression {
    Conditional(ConditionalExpression),
    Assign(LeftSideExpression, AssignmentOperator, Rc<Self>),
}

impl NodeParseOpt for AssignmentExpression {
    fn node_parse_opt(token_tree: &mut TokenTree) -> Result<Option<Self>, NodeParseErrorWithSpan> {
        let original_offset = token_tree.offset;
        let original_peek_offset = token_tree.peek_offset;
        if let Some(ls) = LeftSideExpression::node_parse_opt(token_tree)? {
            if let Some(span) =
                token_tree.peek_is_next_span(&Token::Punctuator(Punctuator::Assignment))
            {
                Ok(Some(Self::Assign(
                    ls,
                    AssignmentOperator::Assign,
                    Rc::new(Self::node_parse_opt(token_tree)?.ok_or_else(|| {
                        NodeParseError::MissingExpression.span(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            span,
                        )
                    })?),
                )))
            } else if let Some(span) = token_tree
                .peek_is_next_span(&Token::Punctuator(Punctuator::MultiplicationAssignment))
            {
                Ok(Some(Self::Assign(
                    ls,
                    AssignmentOperator::Multiply,
                    Rc::new(Self::node_parse_opt(token_tree)?.ok_or_else(|| {
                        NodeParseError::MissingExpression.span(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            span,
                        )
                    })?),
                )))
            } else if let Some(span) =
                token_tree.peek_is_next_span(&Token::Punctuator(Punctuator::DivisionAssignment))
            {
                Ok(Some(Self::Assign(
                    ls,
                    AssignmentOperator::Divide,
                    Rc::new(Self::node_parse_opt(token_tree)?.ok_or_else(|| {
                        NodeParseError::MissingExpression.span(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            span,
                        )
                    })?),
                )))
            } else if let Some(span) =
                token_tree.peek_is_next_span(&Token::Punctuator(Punctuator::RemainderAssignment))
            {
                Ok(Some(Self::Assign(
                    ls,
                    AssignmentOperator::Remainder,
                    Rc::new(Self::node_parse_opt(token_tree)?.ok_or_else(|| {
                        NodeParseError::MissingExpression.span(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            span,
                        )
                    })?),
                )))
            } else if let Some(span) =
                token_tree.peek_is_next_span(&Token::Punctuator(Punctuator::AdditionAssignment))
            {
                Ok(Some(Self::Assign(
                    ls,
                    AssignmentOperator::Addition,
                    Rc::new(Self::node_parse_opt(token_tree)?.ok_or_else(|| {
                        NodeParseError::MissingExpression.span(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            span,
                        )
                    })?),
                )))
            } else if let Some(span) =
                token_tree.peek_is_next_span(&Token::Punctuator(Punctuator::SubtractionAssignment))
            {
                Ok(Some(Self::Assign(
                    ls,
                    AssignmentOperator::Subtraction,
                    Rc::new(Self::node_parse_opt(token_tree)?.ok_or_else(|| {
                        NodeParseError::MissingExpression.span(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            span,
                        )
                    })?),
                )))
            } else if let Some(span) =
                token_tree.peek_is_next_span(&Token::Punctuator(Punctuator::LeftShiftAssignment))
            {
                Ok(Some(Self::Assign(
                    ls,
                    AssignmentOperator::LeftShift,
                    Rc::new(Self::node_parse_opt(token_tree)?.ok_or_else(|| {
                        NodeParseError::MissingExpression.span(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            span,
                        )
                    })?),
                )))
            } else if let Some(span) =
                token_tree.peek_is_next_span(&Token::Punctuator(Punctuator::RightShiftAssignment))
            {
                Ok(Some(Self::Assign(
                    ls,
                    AssignmentOperator::RightShift,
                    Rc::new(Self::node_parse_opt(token_tree)?.ok_or_else(|| {
                        NodeParseError::MissingExpression.span(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            span,
                        )
                    })?),
                )))
            } else if let Some(span) = token_tree
                .peek_is_next_span(&Token::Punctuator(Punctuator::UnsignedRightShiftAssignment))
            {
                Ok(Some(Self::Assign(
                    ls,
                    AssignmentOperator::UnsignedRightShift,
                    Rc::new(Self::node_parse_opt(token_tree)?.ok_or_else(|| {
                        NodeParseError::MissingExpression.span(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            span,
                        )
                    })?),
                )))
            } else if let Some(span) =
                token_tree.peek_is_next_span(&Token::Punctuator(Punctuator::BitwiseAndAssignment))
            {
                Ok(Some(Self::Assign(
                    ls,
                    AssignmentOperator::And,
                    Rc::new(Self::node_parse_opt(token_tree)?.ok_or_else(|| {
                        NodeParseError::MissingExpression.span(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            span,
                        )
                    })?),
                )))
            } else if let Some(span) =
                token_tree.peek_is_next_span(&Token::Punctuator(Punctuator::BitwiseXorAssignment))
            {
                Ok(Some(Self::Assign(
                    ls,
                    AssignmentOperator::Xor,
                    Rc::new(Self::node_parse_opt(token_tree)?.ok_or_else(|| {
                        NodeParseError::MissingExpression.span(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            span,
                        )
                    })?),
                )))
            } else if let Some(span) =
                token_tree.peek_is_next_span(&Token::Punctuator(Punctuator::BitwiseOrAssignment))
            {
                Ok(Some(Self::Assign(
                    ls,
                    AssignmentOperator::Or,
                    Rc::new(Self::node_parse_opt(token_tree)?.ok_or_else(|| {
                        NodeParseError::MissingExpression.span(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            span,
                        )
                    })?),
                )))
            } else if let Some(span) = token_tree
                .peek_is_next_span(&Token::Punctuator(Punctuator::ExponentiationAssignment))
            {
                Ok(Some(Self::Assign(
                    ls,
                    AssignmentOperator::Exponent,
                    Rc::new(Self::node_parse_opt(token_tree)?.ok_or_else(|| {
                        NodeParseError::MissingExpression.span(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            span,
                        )
                    })?),
                )))
            } else if let Some(span) =
                token_tree.peek_is_next_span(&Token::Punctuator(Punctuator::LogicalAndAssignment))
            {
                Ok(Some(Self::Assign(
                    ls,
                    AssignmentOperator::LogicalAnd,
                    Rc::new(Self::node_parse_opt(token_tree)?.ok_or_else(|| {
                        NodeParseError::MissingExpression.span(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            span,
                        )
                    })?),
                )))
            } else if let Some(span) =
                token_tree.peek_is_next_span(&Token::Punctuator(Punctuator::LogicalOrAssignment))
            {
                Ok(Some(Self::Assign(
                    ls,
                    AssignmentOperator::LogicalOr,
                    Rc::new(Self::node_parse_opt(token_tree)?.ok_or_else(|| {
                        NodeParseError::MissingExpression.span(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            span,
                        )
                    })?),
                )))
            } else if let Some(span) = token_tree
                .peek_is_next_span(&Token::Punctuator(Punctuator::NullishCoalescingAssignment))
            {
                Ok(Some(Self::Assign(
                    ls,
                    AssignmentOperator::Coalesce,
                    Rc::new(Self::node_parse_opt(token_tree)?.ok_or_else(|| {
                        NodeParseError::MissingExpression.span(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            span,
                        )
                    })?),
                )))
            } else {
                token_tree.offset = original_offset;
                token_tree.peek_offset = original_peek_offset;
                Ok(ConditionalExpression::node_parse_opt(token_tree)?.map(Self::Conditional))
            }
        } else if let Some(conditional) = ConditionalExpression::node_parse_opt(token_tree)? {
            Ok(Some(Self::Conditional(conditional)))
        } else {
            Ok(None)
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum Expression {
    Assignment(AssignmentExpression),
    Comma(Rc<Self>, AssignmentExpression),
}

impl NodeParseOpt for Expression {
    fn node_parse_opt(token_tree: &mut TokenTree) -> Result<Option<Self>, NodeParseErrorWithSpan> {
        if let Some(assignment) = AssignmentExpression::node_parse_opt(token_tree)? {
            let mut res = Self::Assignment(assignment);

            handle_adj_token!(
                token_tree,
                span,
                (Token::Punctuator(Punctuator::Comma), {
                    let right_side =
                        AssignmentExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                            NodeParseError::MissingExpression.span(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                span,
                            )
                        })?;
                    res = Self::Comma(Rc::new(res), right_side);
                })
            );

            token_tree.commit_peek();
            Ok(Some(res))
        } else {
            token_tree.reset_peek();
            Ok(None)
        }
    }
}

/*
impl NodeParseOpt for Expression {
    fn node_parse_opt(token_tree: &mut TokenTree) -> Result<Option<Self>, NodeParseErrorWithSpan> {
        let next = match token_tree.peek_next() {
            Some(t) => t,
            None => {
                return Ok(None);
            }
        };

        let mut res = match next {
            Token::Identifier(ident) => Some(Self::Identifier(ident)),
            Token::Literal(literal) => Some(Self::Value(literal.into())),
            Token::Punctuator(Punctuator::LeftBracket) => {
                let tokens = token_tree
                    .peek_until(
                        &Token::Punctuator(Punctuator::RightBracket),
                        Some(&Token::Punctuator(Punctuator::LeftBracket)),
                        &[&Token::Punctuator(Punctuator::Semicolon)],
                    )
                    .map_err(|_| {
                        NodeParseError::MissingExpectedToken(Token::Punctuator(
                            Punctuator::RightBracket,
                        ))
                    })?;

                let mut sub_tt = TokenTree::new(token_tree.file_path.clone(), tokens);
                let expr = Expression::node_parse_opt(&mut sub_tt)?
                    .ok_or(NodeParseError::GroupingOperatorMissingExpression)?;
                Some(Self::Grouping(Rc::new(expr)))
            }
            Token::Punctuator(Punctuator::LeftBrace) => {
                let object_literal = ObjectLiteral::node_parse(token_tree)?;
                Some(Self::Value(AstValue::Object(object_literal)))
            }
            _ => {
                token_tree.unpeek(1);
                None
            }
        };

        while let Some(res_expr) = res {
            handle_adj_token!(
                token_tree,
                res,
                res_expr,
                (Token::Punctuator(Punctuator::LeftBracket), {
                    let mut params: Vec<Rc<Expression>> = Vec::new();

                    while let Some(expr) = Expression::node_parse_opt(token_tree)? {
                        params.push(Rc::new(expr));

                        if !token_tree.peek_is_next(&Token::Punctuator(Punctuator::Comma)) {
                            break;
                        }
                    }

                    expect_token!(token_tree, Token::Punctuator(Punctuator::RightBracket));
                    res = Some(Self::FunctionCall(Rc::new(res_expr), params));
                }),
                (Token::Punctuator(Punctuator::LeftSquareBracket), {
                    let expr = Expression::node_parse_opt(token_tree)?
                        .ok_or(NodeParseError::PropertyAccessMissingExpression)?;
                    expect_token!(
                        token_tree,
                        Token::Punctuator(Punctuator::RightSquareBracket)
                    );
                    res = Some(Self::PropertyAccessBrackets(
                        Rc::new(res_expr),
                        Rc::new(expr),
                    ));
                }),
                (Token::Punctuator(Punctuator::Dot), {
                    match token_tree.peek_next() {
                        Some(Token::Identifier(ident)) => {
                            res = Some(Self::PropertyAccess(Rc::new(res_expr), ident))
                        }
                        Some(Token::PrivateIdentifier(_private_ident)) => todo!(),
                        x => {
                            if x.is_some() {
                                token_tree.unpeek(1);
                            }
                            return Err(NodeParseError::PropertyAccessMissingIdent);
                        }
                    }
                }),
                (Token::Punctuator(Punctuator::Assignment), {
                    let rs = Expression::node_parse_opt(token_tree)?
                        .ok_or(NodeParseError::AssignmentMissingRsExpression)?;

                    res = Some(Self::Assignment(
                        Rc::new(res_expr),
                        AssignmentOperator::Assign,
                        Rc::new(rs),
                    ));
                }),
                (Token::Punctuator(Punctuator::AdditionAssignment), {
                    let rs = Expression::node_parse_opt(token_tree)?
                        .ok_or(NodeParseError::AssignmentMissingRsExpression)?;

                    res = Some(Self::Assignment(
                        Rc::new(res_expr),
                        AssignmentOperator::Addition,
                        Rc::new(rs),
                    ));
                }),
                (Token::Punctuator(Punctuator::SubtractionAssignment), {
                    let rs = Expression::node_parse_opt(token_tree)?
                        .ok_or(NodeParseError::AssignmentMissingRsExpression)?;

                    res = Some(Self::Assignment(
                        Rc::new(res_expr),
                        AssignmentOperator::Subtraction,
                        Rc::new(rs),
                    ));
                }),
                (Token::Punctuator(Punctuator::MultiplicationAssignment), {
                    let rs = Expression::node_parse_opt(token_tree)?
                        .ok_or(NodeParseError::AssignmentMissingRsExpression)?;

                    res = Some(Self::Assignment(
                        Rc::new(res_expr),
                        AssignmentOperator::Multiply,
                        Rc::new(rs),
                    ));
                }),
                (Token::Punctuator(Punctuator::DivisionAssignment), {
                    let rs = Expression::node_parse_opt(token_tree)?
                        .ok_or(NodeParseError::AssignmentMissingRsExpression)?;

                    res = Some(Self::Assignment(
                        Rc::new(res_expr),
                        AssignmentOperator::Divide,
                        Rc::new(rs),
                    ));
                }),
                (Token::Punctuator(Punctuator::ModuloAssignment), {
                    let rs = Expression::node_parse_opt(token_tree)?
                        .ok_or(NodeParseError::AssignmentMissingRsExpression)?;

                    res = Some(Self::Assignment(
                        Rc::new(res_expr),
                        AssignmentOperator::Modulo,
                        Rc::new(rs),
                    ));
                }),
                (Token::Punctuator(Punctuator::LeftShiftAssignment), {
                    let rs = Expression::node_parse_opt(token_tree)?
                        .ok_or(NodeParseError::AssignmentMissingRsExpression)?;

                    res = Some(Self::Assignment(
                        Rc::new(res_expr),
                        AssignmentOperator::LeftShift,
                        Rc::new(rs),
                    ));
                }),
                (Token::Punctuator(Punctuator::RightShiftAssignment), {
                    let rs = Expression::node_parse_opt(token_tree)?
                        .ok_or(NodeParseError::AssignmentMissingRsExpression)?;

                    res = Some(Self::Assignment(
                        Rc::new(res_expr),
                        AssignmentOperator::RightShift,
                        Rc::new(rs),
                    ));
                }),
                (
                    Token::Punctuator(Punctuator::UnsignedRightShiftAssignment),
                    {
                        let rs = Expression::node_parse_opt(token_tree)?
                            .ok_or(NodeParseError::AssignmentMissingRsExpression)?;

                        res = Some(Self::Assignment(
                            Rc::new(res_expr),
                            AssignmentOperator::UnsignedRightShift,
                            Rc::new(rs),
                        ));
                    }
                ),
                (Token::Punctuator(Punctuator::BitwiseAndAssignment), {
                    let rs = Expression::node_parse_opt(token_tree)?
                        .ok_or(NodeParseError::AssignmentMissingRsExpression)?;

                    res = Some(Self::Assignment(
                        Rc::new(res_expr),
                        AssignmentOperator::And,
                        Rc::new(rs),
                    ));
                }),
                (Token::Punctuator(Punctuator::BitwiseOrAssignment), {
                    let rs = Expression::node_parse_opt(token_tree)?
                        .ok_or(NodeParseError::AssignmentMissingRsExpression)?;

                    res = Some(Self::Assignment(
                        Rc::new(res_expr),
                        AssignmentOperator::Or,
                        Rc::new(rs),
                    ));
                }),
                (Token::Punctuator(Punctuator::BitwiseXorAssignment), {
                    let rs = Expression::node_parse_opt(token_tree)?
                        .ok_or(NodeParseError::AssignmentMissingRsExpression)?;

                    res = Some(Self::Assignment(
                        Rc::new(res_expr),
                        AssignmentOperator::Xor,
                        Rc::new(rs),
                    ));
                }),
                (Token::Punctuator(Punctuator::ExponentiationAssignment), {
                    let rs = Expression::node_parse_opt(token_tree)?
                        .ok_or(NodeParseError::AssignmentMissingRsExpression)?;

                    res = Some(Self::Assignment(
                        Rc::new(res_expr),
                        AssignmentOperator::Exponent,
                        Rc::new(rs),
                    ));
                }),
                (Token::Punctuator(Punctuator::LogicalAndAssignment), {
                    let rs = Expression::node_parse_opt(token_tree)?
                        .ok_or(NodeParseError::AssignmentMissingRsExpression)?;

                    res = Some(Self::Assignment(
                        Rc::new(res_expr),
                        AssignmentOperator::LogicalAnd,
                        Rc::new(rs),
                    ));
                }),
                (Token::Punctuator(Punctuator::LogicalOrAssignment), {
                    let rs = Expression::node_parse_opt(token_tree)?
                        .ok_or(NodeParseError::AssignmentMissingRsExpression)?;

                    res = Some(Self::Assignment(
                        Rc::new(res_expr),
                        AssignmentOperator::LogicalOr,
                        Rc::new(rs),
                    ));
                }),
                (
                    Token::Punctuator(Punctuator::NullishCoalescingAssignment),
                    {
                        let rs = Expression::node_parse_opt(token_tree)?
                            .ok_or(NodeParseError::AssignmentMissingRsExpression)?;

                        res = Some(Self::Assignment(
                            Rc::new(res_expr),
                            AssignmentOperator::Coalesce,
                            Rc::new(rs),
                        ));
                    }
                ),
                (Token::Punctuator(Punctuator::Equality), {
                    let rs = Expression::node_parse_opt(token_tree)?
                        .ok_or(NodeParseError::ComparisonMissingRsExpression)?;

                    res = Some(Self::Equality(
                        Rc::new(res_expr),
                        EqualityOperator::Equal,
                        Rc::new(rs),
                    ));
                }),
                (Token::Punctuator(Punctuator::Inequality), {
                    let rs = Expression::node_parse_opt(token_tree)?
                        .ok_or(NodeParseError::ComparisonMissingRsExpression)?;

                    res = Some(Self::Equality(
                        Rc::new(res_expr),
                        EqualityOperator::NotEqual,
                        Rc::new(rs),
                    ));
                }),
                (Token::Punctuator(Punctuator::StrictEquality), {
                    let rs = Expression::node_parse_opt(token_tree)?
                        .ok_or(NodeParseError::ComparisonMissingRsExpression)?;

                    res = Some(Self::Equality(
                        Rc::new(res_expr),
                        EqualityOperator::StrictEqual,
                        Rc::new(rs),
                    ));
                }),
                (Token::Punctuator(Punctuator::StrictInequality), {
                    let rs = Expression::node_parse_opt(token_tree)?
                        .ok_or(NodeParseError::ComparisonMissingRsExpression)?;

                    res = Some(Self::Equality(
                        Rc::new(res_expr),
                        EqualityOperator::StrictNotEqual,
                        Rc::new(rs),
                    ));
                })
            );
        }

        if res.is_some() {
            token_tree.commit_peek();
        }

        Ok(res)
    }
}
*/

#[derive(Debug, Clone, PartialEq)]
pub struct Block {
    pub child_tree: Rc<NodeTree>,
}

impl NodeParseOpt for Block {
    fn node_parse_opt(token_tree: &mut TokenTree) -> Result<Option<Self>, NodeParseErrorWithSpan> {
        let start_span = token_tree.peek_prev().map(|x| x.span);
        let tokens = match token_tree.peek_until(
            &Token::Punctuator(Punctuator::RightBrace),
            Some(&Token::Punctuator(Punctuator::LeftBrace)),
            &[&Token::Punctuator(Punctuator::Colon)],
        ) {
            Ok(t) => t,
            Err(PeekUntilError::DisallowedToken) => return Ok(None),
            Err(PeekUntilError::NotFound) => {
                return Err(NodeParseErrorWithSpan::new(
                    token_tree.file_path.clone(),
                    token_tree.source.clone(),
                    NodeParseError::MissingExpectedToken(Token::Punctuator(Punctuator::RightBrace)),
                    start_span,
                ))
            }
        };

        let mut subtoken_tree = TokenTree::new(
            token_tree.file_path.clone(),
            token_tree.source.clone(),
            tokens,
        );

        let child_tree = Rc::new(NodeTree::node_parse(&mut subtoken_tree)?);

        token_tree.commit_peek();

        Ok(Some(Self { child_tree }))
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum AssignmentOperator {
    /// `=`
    Assign,
    /// `*=`
    Multiply,
    /// `/=`
    Divide,
    /// `%=`
    Remainder,
    /// `+=`
    Addition,
    /// `-=`
    Subtraction,
    /// `<<=`
    LeftShift,
    /// `>>=`
    RightShift,
    /// `>>>=`
    UnsignedRightShift,
    /// `&=`
    And,
    /// `^=`
    Xor,
    /// `|=`
    Or,
    /// `**=`
    Exponent,
    /// `&&=`
    LogicalAnd,
    // `||=`
    LogicalOr,
    // `??=`
    Coalesce,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Statement {
    Block(Block),
    VarDeclaration(Vec<(Identifier, Option<Rc<AssignmentExpression>>)>),
    ConstDeclaration(Vec<(Identifier, Rc<AssignmentExpression>)>),
    SingleDeclaration(Vec<(Identifier, Option<Rc<AssignmentExpression>>)>),
    FunctionDeclaration(Identifier, Function),
    Return(Option<Rc<Expression>>),
    If {
        condition: Rc<Expression>,
        body: Rc<Statement>,
        else_body: Option<Rc<Statement>>,
    },
    While {
        condition: Rc<Expression>,
        body: Rc<Statement>,
    },
}

impl NodeParseOpt for Statement {
    fn node_parse_opt(token_tree: &mut TokenTree) -> Result<Option<Self>, NodeParseErrorWithSpan> {
        let token = match token_tree.peek_next() {
            Some(t) => t,
            None => return Ok(None),
        };

        let statement_opt = match token {
            TokenWithSpan {
                span,
                token: Token::Punctuator(punctuator),
            } => match punctuator {
                Punctuator::LeftBrace => {
                    Some(Self::Block(match Block::node_parse_opt(token_tree)? {
                        Some(b) => b,
                        None => return Ok(None),
                    }))
                }
                punctuator => {
                    token_tree.reset_peek();
                    return Err(
                        NodeParseError::UnexpectedToken(Token::Punctuator(punctuator)).span(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            span,
                        ),
                    );
                }
            },
            TokenWithSpan {
                span,
                token: Token::Keyword(keyword),
            } => match keyword {
                Keyword::If => {
                    expect_token!(token_tree, Token::Punctuator(Punctuator::LeftBracket));

                    let condition =
                        Rc::new(Expression::node_parse_opt(token_tree)?.ok_or_else(|| {
                            NodeParseError::IfMissingCondition.span(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                span,
                            )
                        })?);

                    expect_token!(token_tree, Token::Punctuator(Punctuator::RightBracket));

                    let body =
                        Rc::new(Statement::node_parse_opt(token_tree)?.ok_or_else(|| {
                            NodeParseError::IfMissingStatement.span(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                span,
                            )
                        })?);

                    let else_body =
                        match token_tree.peek_is_next_span(&Token::Keyword(Keyword::Else)) {
                            Some(span) => Some(Rc::new(
                                Statement::node_parse_opt(token_tree)?.ok_or_else(|| {
                                    NodeParseError::ElseMissingStatement.span(
                                        token_tree.file_path.clone(),
                                        token_tree.source.clone(),
                                        span,
                                    )
                                })?,
                            )),
                            None => None,
                        };

                    Some(Self::If {
                        condition,
                        body,
                        else_body,
                    })
                }
                Keyword::While => {
                    expect_token!(token_tree, Token::Punctuator(Punctuator::LeftBracket));

                    let condition =
                        Rc::new(Expression::node_parse_opt(token_tree)?.ok_or_else(|| {
                            NodeParseError::LoopMissingCondition.span(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                span,
                            )
                        })?);

                    expect_token!(token_tree, Token::Punctuator(Punctuator::RightBracket));

                    let body =
                        Rc::new(Statement::node_parse_opt(token_tree)?.ok_or_else(|| {
                            NodeParseError::LoopMissingStatement.span(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                span,
                            )
                        })?);

                    Some(Self::While { condition, body })
                }
                Keyword::Return => {
                    let expr = Expression::node_parse_opt(token_tree)?.map(Rc::new);

                    if !token_tree.peek_is_next(&Token::Punctuator(Punctuator::Semicolon)) {
                        return Err(NodeParseError::MissingSemicolon.span(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            span,
                        ));
                    }

                    Some(Self::Return(expr))
                }
                Keyword::Let => {
                    let mut out = Vec::new();

                    loop {
                        let ident = match token_tree.peek_next() {
                            Some(TokenWithSpan {
                                token: Token::Identifier(ident),
                                ..
                            }) => ident,
                            _ => {
                                return Err(NodeParseError::DeclarationMissingIdent.span(
                                    token_tree.file_path.clone(),
                                    token_tree.source.clone(),
                                    span,
                                ))
                            }
                        };

                        let e = match token_tree.peek_next() {
                            Some(TokenWithSpan {
                                span,
                                token: Token::Punctuator(Punctuator::Assignment),
                            }) => {
                                let rs = Rc::new(
                                    AssignmentExpression::node_parse_opt(token_tree)?.ok_or_else(
                                        || {
                                            NodeParseError::AssignmentMissingRsExpression.span(
                                                token_tree.file_path.clone(),
                                                token_tree.source.clone(),
                                                span,
                                            )
                                        },
                                    )?,
                                );

                                (ident, Some(rs))
                            }
                            None => (ident, None),
                            Some(TokenWithSpan {
                                token: Token::Punctuator(Punctuator::Semicolon),
                                ..
                            })
                            | Some(TokenWithSpan {
                                token: Token::Punctuator(Punctuator::Comma),
                                ..
                            }) => {
                                token_tree.unpeek(1);
                                (ident, None)
                            }
                            Some(TokenWithSpan { span, token }) => {
                                return Err(NodeParseError::UnexpectedToken(token).span(
                                    token_tree.file_path.clone(),
                                    token_tree.source.clone(),
                                    span,
                                ))
                            }
                        };

                        out.push(e);

                        if !token_tree.peek_is_next(&Token::Punctuator(Punctuator::Comma)) {
                            break;
                        }
                    }

                    if !token_tree.peek_is_next(&Token::Punctuator(Punctuator::Semicolon)) {
                        return Err(NodeParseError::MissingSemicolon.span(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            span,
                        ));
                    }

                    Some(Self::SingleDeclaration(out))
                }
                Keyword::Const => {
                    let mut out = Vec::new();
                    loop {
                        let (ident, span) = match token_tree.peek_next() {
                            Some(TokenWithSpan {
                                token: Token::Identifier(ident),
                                span,
                            }) => (ident, span),
                            _ => {
                                return Err(NodeParseError::DeclarationMissingIdent.span(
                                    token_tree.file_path.clone(),
                                    token_tree.source.clone(),
                                    span,
                                ))
                            }
                        };

                        if !token_tree.peek_is_next(&Token::Punctuator(Punctuator::Assignment)) {
                            return Err(NodeParseError::MissingExpectedToken(Token::Punctuator(
                                Punctuator::Assignment,
                            ))
                            .span(
                                token_tree.file_path.clone(),
                                token_tree.source.clone(),
                                span,
                            ));
                        }

                        let rs = Rc::new(
                            AssignmentExpression::node_parse_opt(token_tree)?.ok_or_else(|| {
                                NodeParseError::AssignmentMissingRsExpression.span(
                                    token_tree.file_path.clone(),
                                    token_tree.source.clone(),
                                    span,
                                )
                            })?,
                        );

                        out.push((ident, rs));

                        if !token_tree.peek_is_next(&Token::Punctuator(Punctuator::Comma)) {
                            break;
                        }
                    }

                    if !token_tree.peek_is_next(&Token::Punctuator(Punctuator::Semicolon)) {
                        return Err(NodeParseError::MissingSemicolon.span(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            span,
                        ));
                    }

                    Some(Self::ConstDeclaration(out))
                }
                Keyword::Var => {
                    let mut out = Vec::new();

                    loop {
                        let ident = match token_tree.peek_next() {
                            Some(TokenWithSpan {
                                token: Token::Identifier(ident),
                                ..
                            }) => ident,
                            _ => {
                                return Err(NodeParseError::DeclarationMissingIdent.span(
                                    token_tree.file_path.clone(),
                                    token_tree.source.clone(),
                                    span,
                                ))
                            }
                        };

                        let e = match token_tree.peek_next() {
                            Some(TokenWithSpan {
                                span,
                                token: Token::Punctuator(Punctuator::Assignment),
                            }) => {
                                let rs = Rc::new(
                                    AssignmentExpression::node_parse_opt(token_tree)?.ok_or_else(
                                        || {
                                            NodeParseError::AssignmentMissingRsExpression.span(
                                                token_tree.file_path.clone(),
                                                token_tree.source.clone(),
                                                span,
                                            )
                                        },
                                    )?,
                                );

                                (ident, Some(rs))
                            }
                            None => (ident, None),
                            Some(TokenWithSpan {
                                token: Token::Punctuator(Punctuator::Semicolon),
                                ..
                            })
                            | Some(TokenWithSpan {
                                token: Token::Punctuator(Punctuator::Comma),
                                ..
                            }) => {
                                token_tree.unpeek(1);
                                (ident, None)
                            }
                            Some(TokenWithSpan { span, token }) => {
                                return Err(NodeParseError::UnexpectedToken(token).span(
                                    token_tree.file_path.clone(),
                                    token_tree.source.clone(),
                                    span,
                                ))
                            }
                        };

                        out.push(e);

                        if !token_tree.peek_is_next(&Token::Punctuator(Punctuator::Comma)) {
                            break;
                        }
                    }

                    if !token_tree.peek_is_next(&Token::Punctuator(Punctuator::Semicolon)) {
                        return Err(NodeParseError::MissingSemicolon.span(
                            token_tree.file_path.clone(),
                            token_tree.source.clone(),
                            span,
                        ));
                    }

                    Some(Self::VarDeclaration(out))
                }
                Keyword::Function => {
                    let ident = peek_identifier!(token_tree);
                    let func = Function::node_parse(token_tree)?;

                    Some(Self::FunctionDeclaration(ident, func))
                }
                _ => None,
            },
            _ => None,
        };

        match statement_opt.is_some() {
            true => token_tree.commit_peek(),
            false => token_tree.reset_peek(),
        }

        Ok(statement_opt)
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum Node {
    Expression(Rc<Expression>),
    Statement(Rc<Statement>),
}

impl NodeParseOpt for Node {
    fn node_parse_opt(token_tree: &mut TokenTree) -> Result<Option<Self>, NodeParseErrorWithSpan> {
        // Deal with semicolons and comments
        while token_tree.peek_is_next(&Token::Punctuator(Punctuator::Semicolon)) || {
            match token_tree.peek_next().map(|x| x.token) {
                Some(Token::Comment(_)) => true,
                x => {
                    if x.is_some() {
                        token_tree.unpeek(1);
                    }
                    false
                }
            }
        } {
            token_tree.commit_peek();
        }

        let res = if let Some(statement) = Statement::node_parse_opt(token_tree)? {
            Some(Self::Statement(Rc::new(statement)))
        } else {
            let res = Expression::node_parse_opt(token_tree)?
                .map(Rc::new)
                .map(Self::Expression);
            match res.is_some() {
                true => token_tree.commit_peek(),
                false => token_tree.reset_peek(),
            }
            res
        };

        assert_eq!(
            token_tree.peek_offset, 0,
            "offset: {}\n peek_offset: {}\nres: {:?}",
            token_tree.offset, token_tree.peek_offset, res
        );

        Ok(res)
    }
}

pub struct NodeTreeHelper {
    pub tree: Rc<NodeTree>,
    pub current: usize,
    end: usize,
}

impl NodeTreeHelper {
    pub fn new(tree: Rc<NodeTree>) -> Self {
        let end = tree.nodes.len();

        Self {
            tree,
            current: 0,
            end,
        }
    }

    pub fn next_node(&mut self) -> Option<&Node> {
        match self.current < self.end {
            true => {
                let r = &self.tree.nodes[self.current];
                self.current += 1;
                Some(r)
            }
            false => None,
        }
    }

    pub fn more_nodes(&self) -> bool {
        self.current < self.end
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct NodeTree {
    pub file_path: Option<Rc<PathBuf>>,
    pub source: Rc<String>,
    pub nodes: Vec<Node>,
}

impl NodeParse for NodeTree {
    fn node_parse(token_tree: &mut TokenTree) -> Result<Self, NodeParseErrorWithSpan> {
        let mut nodes = Vec::new();

        while let Some(node) = Node::node_parse_opt(token_tree)? {
            nodes.push(node);
        }

        assert_eq!(
            token_tree.peek_offset, 0,
            "offset: {}\n peek_offset: {}\nnodes: {:?}",
            token_tree.offset, token_tree.peek_offset, nodes
        );

        {
            let remaining = token_tree.remaining();

            let span = match (remaining.first(), remaining.last()) {
                (Some(f), Some(l)) => Some(Span {
                    source_start: f.span.source_start,
                    source_end: l.span.source_end,
                }),
                _ => None,
            };

            if !remaining.is_empty() {
                return Err(NodeParseErrorWithSpan::new(
                    token_tree.file_path.clone(),
                    token_tree.source.clone(),
                    NodeParseError::LeftoverTokens(remaining.to_vec()),
                    span,
                ));
            }
        }

        Ok(Self {
            nodes,
            file_path: token_tree.file_path.clone(),
            source: token_tree.source.clone(),
        })
    }
}
