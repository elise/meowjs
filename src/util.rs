use std::str::Chars;

use num::bigint::ParseBigIntError;
use num::traits::ParseFloatError;
use num::{BigInt, Num};

pub fn parse_number(mut s: &str) -> Result<f64, ParseFloatError> {
    let is_oct = s.starts_with("0o") || s.starts_with("0O");
    let is_bin = s.starts_with("0b") || s.starts_with("0B");
    let is_hex = s.starts_with("0x") || s.starts_with("0X");

    let radix = if is_hex {
        16
    } else if is_oct {
        8
    } else if is_bin {
        2
    } else {
        10
    };

    if radix != 10 {
        s = &s[2..];
    }

    f64::from_str_radix(s, radix)
}

pub fn parse_bigint(mut s: &str) -> Result<BigInt, ParseBigIntError> {
    let is_oct = s.starts_with("0o") || s.starts_with("0O");
    let is_bin = s.starts_with("0b") || s.starts_with("0B");
    let is_hex = s.starts_with("0x") || s.starts_with("0X");

    let radix = if is_hex {
        16
    } else if is_oct {
        8
    } else if is_bin {
        2
    } else {
        10
    };

    if radix != 10 {
        s = &s[2..];
    }

    BigInt::from_str_radix(s, radix)
}

pub struct EscapedStringIterator<'a>(Chars<'a>);

impl<'a> EscapedStringIterator<'a> {
    pub fn new(s: &'a str) -> Self {
        Self(s.chars())
    }
}

impl<'a> Iterator for EscapedStringIterator<'a> {
    type Item = Result<char, ParseEscapedStringError>;

    fn next(&mut self) -> Option<Self::Item> {
        let n = self.0.next()?;
        if n == '\\' {
            match self.0.next() {
                Some('n') => Some(Ok('\n')),
                Some('r') => Some(Ok('\r')),
                Some('b') => Some(Ok('\u{0008}')),
                Some('t') => Some(Ok('\t')),
                Some('v') => Some(Ok('\u{000B}')),
                Some('f') => Some(Ok('\u{000C}')),
                Some('\\') => Some(Ok('\\')),
                Some('\'') => Some(Ok('\'')),
                Some('\"') => Some(Ok('\"')),
                Some(c) => Some(Err(ParseEscapedStringError::InvalidEscape(c))),
                None => Some(Err(ParseEscapedStringError::MissingSequence)),
            }
        } else {
            Some(Ok(n))
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, thiserror::Error)]
pub enum ParseEscapedStringError {
    #[error("Invalid escape character: {0}")]
    InvalidEscape(char),
    #[error("Escape missing character sequence")]
    MissingSequence,
}

pub fn parse_escaped_string(s: &str) -> Result<String, ParseEscapedStringError> {
    let mut out = String::new();

    for x in EscapedStringIterator::new(s) {
        out.push(x?);
    }

    Ok(out)
}
