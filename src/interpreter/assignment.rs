use std::borrow::Cow;
use std::rc::Rc;

use num::{ToPrimitive, Zero};

use crate::ast::{AssignmentExpression, AssignmentOperator, LeftSideExpression};
use crate::interpreter::variables::{PreferredType, ValueOrRef};

use super::{variables::InterpreterValue, InterpreterError, InterpreterLayer};

impl InterpreterLayer {
    pub fn handle_assignment(
        self: &Rc<Self>,
        ls: &LeftSideExpression,
        op: AssignmentOperator,
        rs: &AssignmentExpression,
    ) -> Result<InterpreterValue, InterpreterError> {
        macro_rules! basic_assignment {
            ($lref:ident, $op:tt, $rs:ident) => {
                {
                    let rval = self.interpret_assignment_expression($rs)?.into_value();
                    let lval = $lref.clone().into_value();

                    let lprim = lval.to_primitive(self, PreferredType::Default)?;
                    let rprim = rval.to_primitive(self, PreferredType::Default)?;

                    match (lprim.as_ref(), rprim.as_ref()) {
                        (InterpreterValue::BigInt(lnum), InterpreterValue::BigInt(rnum)) => {
                            let val = InterpreterValue::BigInt(lnum $op rnum);
                            $lref.set(val.clone());
                            Ok(val)
                        },
                        (InterpreterValue::BigInt(_), _)
                        | (_, InterpreterValue::BigInt(_)) => Err(InterpreterError::TypeMsg(Cow::Borrowed("Attempted operation with 'number' and 'bigint'"))),
                        (lprim, rprim) => {
                            let lnum = lprim.to_number(self)?;
                            let rnum = rprim.to_number(self)?;
                            let val = InterpreterValue::Numeric(lnum $op rnum);
                            $lref.set(val.clone());
                            Ok(val)
                        }
                    }
                }
            };
        }

        macro_rules! bitwise_assignment {
            ($lref:ident, $op:tt, $rs:ident) => {
                {
                    let rval = self.interpret_assignment_expression($rs)?.into_value();
                    let lval = $lref.clone().into_value();

                    let lnum = lval.to_numeric(self)?;
                    let rnum = rval.to_numeric(self)?;

                    match (lnum, rnum) {
                        (InterpreterValue::BigInt(lnum), InterpreterValue::BigInt(rnum)) => {
                            let val = InterpreterValue::BigInt(lnum $op rnum);
                            $lref.set(val.clone());
                            Ok(val)
                        },
                        (InterpreterValue::BigInt(_), _)
                        | (_, InterpreterValue::BigInt(_)) => Err(InterpreterError::TypeMsg(Cow::Borrowed("Attempted bitwise operation with 'number' and 'bigint'"))),
                        (lprim, rprim) => {
                            let lnum = lprim.to_number(self)?;
                            let lnum = match lnum.is_finite() {
                                true => lnum as i32,
                                false => 0,
                            };

                            let rnum = rprim.to_number(self)?;
                            let rnum = match rnum.is_finite() {
                                true => rnum as i32,
                                false => 0,
                            };

                            let val = InterpreterValue::Numeric((lnum $op rnum) as f64);
                            $lref.set(val.clone());
                            Ok(val)
                        }
                    }
                }
            };
        }
        let ls = self.interpret_left_side_expression(ls)?;

        return match ls {
            ValueOrRef::Value(_val) => Err(InterpreterError::InvalidLeftSideAssignment),
            ValueOrRef::Ref(lref) => match op {
                crate::ast::AssignmentOperator::Assign => {
                    let val = self.interpret_assignment_expression(rs)?.into_value();
                    lref.set(val.clone());
                    Ok(val)
                }
                crate::ast::AssignmentOperator::Multiply => basic_assignment!(lref, *, rs),
                crate::ast::AssignmentOperator::Divide => basic_assignment!(lref, /, rs),
                crate::ast::AssignmentOperator::Remainder => {
                    let rval = self.interpret_assignment_expression(rs)?.into_value();
                    let lval = lref.clone().into_value();

                    let lnum = lval.to_numeric(self)?;
                    let rnum = rval.to_numeric(self)?;

                    match (lnum, rnum) {
                        (InterpreterValue::BigInt(lnum), InterpreterValue::BigInt(rnum)) => {
                            let n = lnum;
                            let d = rnum;

                            if d.is_zero() {
                                return Err(InterpreterError::Range(
                                    "Attempted to get remainder of dividing by zero",
                                ));
                            }

                            let val = if n.is_zero() {
                                InterpreterValue::BigInt(n)
                            } else {
                                InterpreterValue::BigInt(n % d)
                            };

                            lref.set(val.clone());
                            Ok(val)
                        }
                        (InterpreterValue::Numeric(lnum), InterpreterValue::Numeric(rnum)) => {
                            let n = lnum;
                            let d = rnum;

                            let val = if n.is_nan() || d.is_nan() || n.is_infinite() {
                                InterpreterValue::Numeric(f64::NAN)
                            } else if d.is_infinite() {
                                InterpreterValue::Numeric(n)
                            } else if d == 0.0 {
                                InterpreterValue::Numeric(f64::NAN)
                            } else if n == 0.0 {
                                InterpreterValue::Numeric(n)
                            } else {
                                let r = n - ((n / d).floor() * d);

                                if r == 0.0 && n < 0.0 {
                                    InterpreterValue::Numeric(-0.0)
                                } else {
                                    InterpreterValue::Numeric(r)
                                }
                            };

                            lref.set(val.clone());
                            Ok(val)
                        }
                        (l, r) => Err(InterpreterError::TypeMsg(Cow::Owned(format!(
                            "Attempted operation with '{}' and '{}",
                            l.type_as_str(),
                            r.type_as_str()
                        )))),
                    }
                }
                crate::ast::AssignmentOperator::Addition => {
                    let rval = self.interpret_assignment_expression(rs)?.into_value();
                    let lval = lref.clone().into_value();

                    let lprim = lval.to_primitive(self, PreferredType::Default)?;
                    let rprim = rval.to_primitive(self, PreferredType::Default)?;

                    if let InterpreterValue::String(lstr) = lprim.as_ref() {
                        let rstr = rprim.to_string(self)?;
                        let res = InterpreterValue::String(Rc::new(format!("{}{}", lstr, rstr)));
                        lref.set(res.clone());

                        return Ok(res);
                    }

                    if let InterpreterValue::String(rstr) = rprim.as_ref() {
                        let lstr = lprim.to_string(self)?;
                        let res = InterpreterValue::String(Rc::new(format!("{}{}", lstr, rstr)));
                        lref.set(res.clone());

                        return Ok(res);
                    }

                    match (lprim.as_ref(), rprim.as_ref()) {
                        (InterpreterValue::BigInt(lnum), InterpreterValue::BigInt(rnum)) => {
                            let val = InterpreterValue::BigInt(lnum + rnum);

                            lref.set(val.clone());

                            Ok(val)
                        }
                        (InterpreterValue::BigInt(_), _) | (_, InterpreterValue::BigInt(_)) => {
                            Err(InterpreterError::TypeMsg(Cow::Borrowed(
                                "Attempted operation with 'number' and 'bigint'",
                            )))
                        }
                        (lprim, rprim) => {
                            let lnum = lprim.to_number(self)?;
                            let rnum = rprim.to_number(self)?;
                            let val = InterpreterValue::Numeric(lnum + rnum);

                            lref.set(val.clone());

                            Ok(val)
                        }
                    }
                }
                crate::ast::AssignmentOperator::Subtraction => basic_assignment!(lref, -, rs),
                crate::ast::AssignmentOperator::LeftShift => {
                    let lval = lref.clone().into_value();
                    let rval = self.interpret_assignment_expression(rs)?.into_value();

                    let lnum = lval.to_numeric(self)?;
                    let rnum = rval.to_numeric(self)?;

                    match (lnum, rnum) {
                        (InterpreterValue::BigInt(lnum), InterpreterValue::BigInt(rnum)) => {
                            let rnum_u128 = rnum
                                .to_u128()
                                .ok_or(InterpreterError::ShiftWithTooLargeRightSide)?;

                            let val = InterpreterValue::BigInt(lnum << rnum_u128);
                            lref.set(val.clone());
                            Ok(val)
                        }
                        (InterpreterValue::BigInt(_), _) | (_, InterpreterValue::BigInt(_)) => {
                            Err(InterpreterError::TypeMsg(Cow::Borrowed(
                                "Attempted operation with 'number' and 'bigint'",
                            )))
                        }
                        (lprim, rprim) => {
                            let lnum = lprim.to_number(self)?;
                            let lnum = match lnum.is_finite() {
                                true => lnum as u32,
                                false => 0,
                            };

                            let rnum = rprim.to_number(self)?;
                            let rnum = match rnum.is_finite() {
                                true => rnum as u32,
                                false => 0,
                            };

                            let val = InterpreterValue::Numeric((lnum << rnum) as f64);
                            lref.set(val.clone());
                            Ok(val)
                        }
                    }
                }
                crate::ast::AssignmentOperator::RightShift => {
                    let lval = lref.clone().into_value();
                    let rval = self.interpret_assignment_expression(rs)?.into_value();

                    let lnum = lval.to_numeric(self)?;
                    let rnum = rval.to_numeric(self)?;

                    match (lnum, rnum) {
                        (InterpreterValue::BigInt(lnum), InterpreterValue::BigInt(rnum)) => {
                            let rnum_i128 = rnum
                                .to_i128()
                                .ok_or(InterpreterError::ShiftWithTooLargeRightSide)?;

                            let val = InterpreterValue::BigInt(lnum >> rnum_i128);
                            lref.set(val.clone());
                            Ok(val)
                        }
                        (InterpreterValue::BigInt(_), _) | (_, InterpreterValue::BigInt(_)) => {
                            Err(InterpreterError::TypeMsg(Cow::Borrowed(
                                "Attempted operation with 'number' and 'bigint'",
                            )))
                        }
                        (lprim, rprim) => {
                            let lnum = lprim.to_number(self)?;
                            let lnum = match lnum.is_finite() {
                                true => lnum as u32,
                                false => 0,
                            };

                            let rnum = rprim.to_number(self)?;
                            let rnum = match rnum.is_finite() {
                                true => rnum as u32,
                                false => 0,
                            };

                            let val = InterpreterValue::Numeric((lnum >> rnum) as f64);
                            lref.set(val.clone());
                            Ok(val)
                        }
                    }
                }
                crate::ast::AssignmentOperator::UnsignedRightShift => {
                    let lval = lref.clone().into_value();
                    let rval = self.interpret_assignment_expression(rs)?.into_value();

                    let lnum = lval.to_number(self)?;
                    let rnum = rval.to_number(self)?;

                    let lnum = match lnum.is_finite() {
                        true => lnum as u32,
                        false => 0,
                    };

                    let rnum = match rnum.is_finite() {
                        true => rnum as u32,
                        false => 0,
                    };

                    let shift_count = rnum % 32;
                    let val = InterpreterValue::Numeric((lnum >> shift_count) as f64);

                    lref.set(val.clone());

                    Ok(val)
                }
                crate::ast::AssignmentOperator::And => bitwise_assignment!(lref, &, rs),
                crate::ast::AssignmentOperator::Xor => bitwise_assignment!(lref, ^, rs),
                crate::ast::AssignmentOperator::Or => bitwise_assignment!(lref, |, rs),
                crate::ast::AssignmentOperator::Exponent => todo!(),
                crate::ast::AssignmentOperator::LogicalAnd => {
                    let rval = self.interpret_assignment_expression(rs)?.into_value();
                    let lval = lref.clone().into_value();

                    let val = InterpreterValue::Boolean(lval.as_bool() && rval.as_bool());

                    lref.set(val.clone());

                    Ok(val)
                }
                crate::ast::AssignmentOperator::LogicalOr => {
                    let rval = self.interpret_assignment_expression(rs)?.into_value();
                    let lval = lref.clone().into_value();

                    let val = InterpreterValue::Boolean(lval.as_bool() || rval.as_bool());

                    lref.set(val.clone());

                    Ok(val)
                }
                crate::ast::AssignmentOperator::Coalesce => {
                    let rval = self.interpret_assignment_expression(rs)?.into_value();
                    let lval = lref.clone().into_value();

                    match lval {
                        InterpreterValue::Undefined | InterpreterValue::Null => {
                            lref.set(rval.clone());
                            Ok(rval)
                        }
                        v => Ok(v),
                    }
                }
            },
        };
    }
}
