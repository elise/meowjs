use std::{borrow::Cow, rc::Rc};

use num::{bigint::Sign, BigInt, ToPrimitive, Zero};

use crate::ast::{
    AdditiveExpression, AssignmentExpression, BitwiseAndExpression, BitwiseOrExpression,
    BitwiseXorExpression, CallExpression, CoalesceExpression, ConditionalExpression,
    EqualityExpression, ExponentiationExpression, Expression, LeftSideExpression,
    LogicalAndExpression, LogicalOrExpression, MemberExpression, MultiplicativeExpression,
    NewExpression, PrimaryExpression, PropertyNameValue, RelationalExpression, ShiftExpression,
    ShortCircuitExpression, UnaryExpression, UpdateExpression,
};

use super::{
    variables::{InterpreterValue, Object, PreferredType, Ref, ValueOrRef},
    InterpreterError, InterpreterLayer,
};

impl InterpreterLayer {
    pub fn interpret_primary_expression(
        self: &Rc<Self>,
        expression: &PrimaryExpression,
    ) -> Result<ValueOrRef, InterpreterError> {
        match expression {
            PrimaryExpression::Identifier(ident) => {
                let ident = PropertyNameValue::Identifier(ident.clone());

                let value = self
                    .fetch_variable(&ident)
                    .ok_or(InterpreterError::UndefinedIdentifierReference(ident))?
                    .value()
                    .clone();

                Ok(ValueOrRef::Ref(Ref::ValRef(value)))
            }
            PrimaryExpression::Value(value) => Ok(ValueOrRef::Value(
                InterpreterValue::from_ast_value(self, value.clone())?,
            )),
            PrimaryExpression::This => todo!(),
            PrimaryExpression::Function(ident, f) => {
                Ok(ValueOrRef::Value(InterpreterValue::Object(Object::new_fn(
                    None,
                    ident.clone(),
                    f.clone(),
                    Some(self.clone()),
                ))))
            }
            PrimaryExpression::Grouping(grouping) => self.interpret_expression(grouping),
        }
    }

    pub fn interpret_member_expression(
        self: &Rc<Self>,
        expression: &MemberExpression,
    ) -> Result<ValueOrRef, InterpreterError> {
        match expression {
            MemberExpression::Primary(p) => self.interpret_primary_expression(p),
            MemberExpression::PropertyAccess(v, r) => {
                let value_or_ref = self.interpret_member_expression(v.as_ref())?;
                let value = value_or_ref.into_value();

                match value {
                    InterpreterValue::Undefined => Err(InterpreterError::TypeMsg(Cow::Borrowed(
                        "Attempted to access a property on 'undefined'",
                    ))),
                    InterpreterValue::Null => Err(InterpreterError::TypeMsg(Cow::Borrowed(
                        "Attempted to access a property on 'null'",
                    ))),
                    InterpreterValue::Boolean(_)
                    | InterpreterValue::String(_)
                    | InterpreterValue::Numeric(_)
                    | InterpreterValue::BigInt(_) => {
                        Ok(ValueOrRef::Value(InterpreterValue::Undefined))
                    }
                    InterpreterValue::Object(o) => {
                        let ident = PropertyNameValue::Identifier(r.clone());
                        match o.get(&ident) {
                            Some(value) => Ok(ValueOrRef::Ref(Ref::ValRef(value))),
                            None => Ok(ValueOrRef::Ref(Ref::PropRef(o, ident))),
                        }
                    }
                }
            }
            MemberExpression::PropertyAccessBrackets(v, r) => {
                let access_value_str = self
                    .interpret_expression(r.as_ref())?
                    .into_value()
                    .to_string(self)?;
                let property_name_value = PropertyNameValue::String(access_value_str);

                let value_or_ref = self.interpret_member_expression(v.as_ref())?;
                let value = value_or_ref.into_value();

                match value {
                    InterpreterValue::Undefined => Err(InterpreterError::TypeMsg(Cow::Borrowed(
                        "Attempted to access a property on 'undefined'",
                    ))),
                    InterpreterValue::Null => Err(InterpreterError::TypeMsg(Cow::Borrowed(
                        "Attempted to access a property on 'null'",
                    ))),
                    InterpreterValue::Boolean(_)
                    | InterpreterValue::String(_)
                    | InterpreterValue::Numeric(_)
                    | InterpreterValue::BigInt(_) => {
                        Ok(ValueOrRef::Value(InterpreterValue::Undefined))
                    }
                    InterpreterValue::Object(o) => match o.get(&property_name_value) {
                        Some(value) => Ok(ValueOrRef::Ref(Ref::ValRef(value))),
                        None => Ok(ValueOrRef::Ref(Ref::PropRef(o, property_name_value))),
                    },
                }
            }
            MemberExpression::PrivatePropertyAccess(_, _) => todo!(),
        }
    }

    pub fn interpret_new_expression(
        self: &Rc<Self>,
        expression: &NewExpression,
    ) -> Result<ValueOrRef, InterpreterError> {
        match expression {
            NewExpression::Member(member) => self.interpret_member_expression(member),
            NewExpression::New(_) => todo!(),
        }
    }

    pub fn interpret_left_side_expression(
        self: &Rc<Self>,
        expression: &LeftSideExpression,
    ) -> Result<ValueOrRef, InterpreterError> {
        match expression {
            LeftSideExpression::New(new) => self.interpret_new_expression(new),
            LeftSideExpression::Call(call) => self.interpret_call_expression(call),
        }
    }

    pub fn interpret_call_expression(
        self: &Rc<Self>,
        expression: &CallExpression,
    ) -> Result<ValueOrRef, InterpreterError> {
        match expression {
            CallExpression::CallMember(member, params) => {
                let callable = self
                    .interpret_member_expression(member)?
                    .into_value()
                    .downcast_to_function()?;

                self.call_function(callable, params.as_slice())
                    .map(ValueOrRef::Value)
            }
            CallExpression::Call(c, params) => {
                let callable = self
                    .interpret_call_expression(c)?
                    .into_value()
                    .downcast_to_function()?;

                self.call_function(callable, params.as_slice())
                    .map(ValueOrRef::Value)
            }
            CallExpression::PropertyAccessBracketsCall(c, idx) => {
                let access_value_str = self
                    .interpret_expression(idx.as_ref())?
                    .into_value()
                    .to_string(self)?;
                let property_name_value = PropertyNameValue::String(access_value_str);

                let value_or_ref = self.interpret_call_expression(c.as_ref())?;
                let value = value_or_ref.into_value();

                match value {
                    InterpreterValue::Undefined => Err(InterpreterError::TypeMsg(Cow::Borrowed(
                        "Attempted to access a property on 'undefined'",
                    ))),
                    InterpreterValue::Null => Err(InterpreterError::TypeMsg(Cow::Borrowed(
                        "Attempted to access a property on 'null'",
                    ))),
                    InterpreterValue::Boolean(_)
                    | InterpreterValue::String(_)
                    | InterpreterValue::Numeric(_)
                    | InterpreterValue::BigInt(_) => {
                        Ok(ValueOrRef::Value(InterpreterValue::Undefined))
                    }
                    InterpreterValue::Object(o) => match o.get(&property_name_value) {
                        Some(value) => Ok(ValueOrRef::Ref(Ref::ValRef(value))),
                        None => Ok(ValueOrRef::Ref(Ref::PropRef(o, property_name_value))),
                    },
                }
            }
            CallExpression::PropertyAccessCall(c, i) => {
                let value_or_ref = self.interpret_call_expression(c.as_ref())?;
                let value = value_or_ref.into_value();

                match value {
                    InterpreterValue::Undefined => Err(InterpreterError::TypeMsg(Cow::Borrowed(
                        "Attempted to access a property on 'undefined'",
                    ))),
                    InterpreterValue::Null => Err(InterpreterError::TypeMsg(Cow::Borrowed(
                        "Attempted to access a property on 'null'",
                    ))),
                    InterpreterValue::Boolean(_)
                    | InterpreterValue::String(_)
                    | InterpreterValue::Numeric(_)
                    | InterpreterValue::BigInt(_) => {
                        Ok(ValueOrRef::Value(InterpreterValue::Undefined))
                    }
                    InterpreterValue::Object(o) => {
                        let ident = PropertyNameValue::Identifier(i.clone());
                        match o.get(&ident) {
                            Some(value) => Ok(ValueOrRef::Ref(Ref::ValRef(value))),
                            None => Ok(ValueOrRef::Ref(Ref::PropRef(o, ident))),
                        }
                    }
                }
            }
            CallExpression::PrivatePropertyAccessCall(_c, _p) => todo!(),
        }
    }

    pub fn interpret_update_expression(
        self: &Rc<Self>,
        expression: &UpdateExpression,
    ) -> Result<ValueOrRef, InterpreterError> {
        match expression {
            UpdateExpression::LeftSide(ls) => self.interpret_left_side_expression(ls),
            UpdateExpression::SuffixIncrement(val) => {
                match self.interpret_left_side_expression(val)? {
                    ValueOrRef::Value(_) => Err(InterpreterError::InvalidUpdateOperand),
                    ValueOrRef::Ref(r) => {
                        let val = r.clone().into_value().to_numeric(self)?;
                        match &val {
                            InterpreterValue::Numeric(n) => {
                                r.set(InterpreterValue::Numeric(n + 1.0));
                            }
                            InterpreterValue::BigInt(b) => {
                                r.set(InterpreterValue::BigInt(b + 1));
                            }
                            _ => unreachable!(),
                        }
                        Ok(ValueOrRef::Value(val))
                    }
                }
            }
            UpdateExpression::SuffixDecrement(val) => {
                match self.interpret_left_side_expression(val)? {
                    ValueOrRef::Value(_) => Err(InterpreterError::InvalidUpdateOperand),
                    ValueOrRef::Ref(r) => {
                        let val = r.clone().into_value().to_numeric(self)?;
                        match &val {
                            InterpreterValue::Numeric(n) => {
                                r.set(InterpreterValue::Numeric(n - 1.0));
                            }
                            InterpreterValue::BigInt(b) => {
                                r.set(InterpreterValue::BigInt(b - 1));
                            }
                            _ => unreachable!(),
                        }
                        Ok(ValueOrRef::Value(val))
                    }
                }
            }
            UpdateExpression::PrefixIncrement(val) => {
                let expr = self.interpret_unary_expression(val.as_ref())?;
                match expr.clone() {
                    ValueOrRef::Value(_) => Err(InterpreterError::InvalidUpdateOperand),
                    ValueOrRef::Ref(r) => {
                        let old_value = expr.into_value().to_numeric(self)?;
                        match old_value {
                            InterpreterValue::Numeric(n) => {
                                let new_val = InterpreterValue::Numeric(n + 1.0);
                                r.set(new_val.clone());
                                Ok(ValueOrRef::Value(new_val))
                            }
                            InterpreterValue::BigInt(b) => {
                                let new_val = InterpreterValue::BigInt(b + 1);
                                r.set(new_val.clone());
                                Ok(ValueOrRef::Value(new_val))
                            }
                            _ => unreachable!(),
                        }
                    }
                }
            }
            UpdateExpression::PrefixDecrement(val) => {
                let expr = self.interpret_unary_expression(val.as_ref())?;
                match expr.clone() {
                    ValueOrRef::Value(_) => Err(InterpreterError::InvalidUpdateOperand),
                    ValueOrRef::Ref(r) => {
                        let old_value = expr.into_value().to_numeric(self)?;
                        match old_value {
                            InterpreterValue::Numeric(n) => {
                                let new_val = InterpreterValue::Numeric(n - 1.0);
                                r.set(new_val.clone());
                                Ok(ValueOrRef::Value(new_val))
                            }
                            InterpreterValue::BigInt(b) => {
                                let new_val = InterpreterValue::BigInt(b - 1);
                                r.set(new_val.clone());
                                Ok(ValueOrRef::Value(new_val))
                            }
                            _ => unreachable!(),
                        }
                    }
                }
            }
        }
    }

    pub fn interpret_unary_expression(
        self: &Rc<Self>,
        expression: &UnaryExpression,
    ) -> Result<ValueOrRef, InterpreterError> {
        match expression {
            UnaryExpression::Update(u) => self.interpret_update_expression(u),
            UnaryExpression::Unary(op, rs) => {
                let expr = self.interpret_unary_expression(rs.as_ref())?.into_value();
                match op {
                    crate::ast::UnaryOperator::BitwiseNot => {
                        let expr = expr.to_numeric(self)?;
                        Ok(ValueOrRef::Value(match expr {
                            InterpreterValue::Numeric(n) => {
                                InterpreterValue::Numeric((!(n as i32)) as f64)
                            }
                            InterpreterValue::BigInt(b) => InterpreterValue::BigInt(!b),
                            _ => unreachable!(),
                        }))
                    }
                    crate::ast::UnaryOperator::LogicalNot => {
                        let expr = expr.to_numeric(self)?.as_bool();
                        Ok(ValueOrRef::Value(InterpreterValue::Boolean(!expr)))
                    }
                    crate::ast::UnaryOperator::Typeof => Ok(ValueOrRef::Value(
                        InterpreterValue::String(Rc::new(expr.type_as_str().to_string())),
                    )),
                    crate::ast::UnaryOperator::Positive => Ok(ValueOrRef::Value(
                        InterpreterValue::Numeric(expr.to_number(self)?),
                    )),
                    crate::ast::UnaryOperator::Negative => {
                        let old_value = expr.to_numeric(self)?;
                        match old_value {
                            InterpreterValue::Numeric(n) => {
                                Ok(ValueOrRef::Value(InterpreterValue::Numeric(-n)))
                            }
                            InterpreterValue::BigInt(b) => {
                                Ok(ValueOrRef::Value(InterpreterValue::BigInt(-b)))
                            }
                            _ => unreachable!(),
                        }
                    }
                }
            }
        }
    }

    pub fn interpret_exponentiation_expression(
        self: &Rc<Self>,
        expression: &ExponentiationExpression,
    ) -> Result<ValueOrRef, InterpreterError> {
        match expression {
            ExponentiationExpression::Unary(u) => self.interpret_unary_expression(u),
            ExponentiationExpression::Exponent(l, r) => {
                let lref = self.interpret_update_expression(l)?;
                let lval = lref.into_value();
                let rref = self.interpret_exponentiation_expression(r.as_ref())?;
                let rval = rref.into_value();

                let lnum = lval.to_numeric(self)?;
                let rnum = rval.to_numeric(self)?;

                match (lnum, rnum) {
                    (InterpreterValue::BigInt(lnum), InterpreterValue::BigInt(rnum)) => {
                        if rnum.sign() == Sign::Minus {
                            return Err(InterpreterError::Range("exponent < 0n"));
                        }

                        match lnum.is_zero() && rnum.is_zero() {
                            true => {
                                Ok(ValueOrRef::Value(InterpreterValue::BigInt(BigInt::from(1))))
                            }
                            false => match rnum.to_u32() {
                                Some(exp) => {
                                    Ok(ValueOrRef::Value(InterpreterValue::BigInt(lnum.pow(exp))))
                                }
                                None => Err(InterpreterError::Range(
                                    "Exponent doesn't fit into a uint32",
                                )),
                            },
                        }
                    }
                    (InterpreterValue::Numeric(lnum), InterpreterValue::Numeric(rnum)) => {
                        if rnum.is_nan() {
                            return Ok(ValueOrRef::Value(InterpreterValue::Numeric(f64::NAN)));
                        }

                        if rnum == 0.0f64 {
                            return Ok(ValueOrRef::Value(InterpreterValue::Numeric(1.0)));
                        }

                        if lnum.is_nan() {
                            return Ok(ValueOrRef::Value(InterpreterValue::Numeric(f64::NAN)));
                        }

                        if lnum.is_infinite() {
                            return Ok(ValueOrRef::Value(InterpreterValue::Numeric(
                                match lnum.is_sign_positive() {
                                    true => match rnum > 0.0 {
                                        true => f64::INFINITY,
                                        false => 0.0,
                                    },
                                    false => match rnum > 0.0 {
                                        true => match rnum.fract() == 0.0 && rnum % 2.0 == 1.0 {
                                            true => -f64::INFINITY,
                                            false => f64::INFINITY,
                                        },
                                        false => match rnum.fract() == 0.0 && rnum % 2.0 == 1.0 {
                                            true => -0.0,
                                            false => 0.0,
                                        },
                                    },
                                },
                            )));
                        }

                        if lnum == 0.0 {
                            return Ok(ValueOrRef::Value(InterpreterValue::Numeric(
                                match lnum.is_sign_positive() {
                                    true => match rnum > 0.0 {
                                        true => f64::INFINITY,
                                        false => 0.0,
                                    },
                                    false => match rnum > 0.0 {
                                        true => match rnum.fract() == 0.0 && rnum % 2.0 == 1.0 {
                                            true => -f64::INFINITY,
                                            false => f64::INFINITY,
                                        },
                                        false => match rnum.fract() == 0.0 && rnum % 2.0 == 1.0 {
                                            true => -0.0,
                                            false => 0.0,
                                        },
                                    },
                                },
                            )));
                        }

                        assert!(lnum.is_finite());
                        assert!(!lnum.is_zero());

                        if rnum.is_infinite() {
                            return Ok(ValueOrRef::Value(InterpreterValue::Numeric(
                                match rnum.is_sign_positive() {
                                    true => {
                                        let abs = lnum.abs();
                                        if abs > 1.0 {
                                            f64::INFINITY
                                        } else if abs == 1.0 {
                                            f64::NAN
                                        } else {
                                            0.0
                                        }
                                    }
                                    false => {
                                        let abs = lnum.abs();
                                        if abs > 1.0 {
                                            0.0
                                        } else if abs == 1.0 {
                                            f64::NAN
                                        } else {
                                            f64::INFINITY
                                        }
                                    }
                                },
                            )));
                        }

                        assert!(rnum.is_finite());
                        assert!(!rnum.is_zero());

                        if lnum < 0.0 && rnum.fract() != 0.0 {
                            return Ok(ValueOrRef::Value(InterpreterValue::Numeric(f64::NAN)));
                        }

                        Ok(ValueOrRef::Value(InterpreterValue::Numeric(
                            lnum.powf(rnum),
                        )))
                    }
                    (l, r) => Err(InterpreterError::TypeMsg(Cow::Owned(format!(
                        "Attempted operation with '{}' and '{}",
                        l.type_as_str(),
                        r.type_as_str()
                    )))),
                }
            }
        }
    }

    pub fn interpret_multiplicative_expression(
        self: &Rc<Self>,
        expression: &MultiplicativeExpression,
    ) -> Result<ValueOrRef, InterpreterError> {
        match expression {
            MultiplicativeExpression::Exponentiation(e) => {
                self.interpret_exponentiation_expression(e)
            }
            MultiplicativeExpression::Multiplicative(l, op, r) => {
                let lref = self.interpret_multiplicative_expression(l)?;
                let lval = lref.into_value();
                let rref = self.interpret_exponentiation_expression(r)?;
                let rval = rref.into_value();

                let lnum = lval.to_numeric(self)?;
                let rnum = rval.to_numeric(self)?;

                match (lnum, rnum) {
                    (InterpreterValue::BigInt(lnum), InterpreterValue::BigInt(rnum)) => match op {
                        crate::ast::MultiplicativeOperator::Multiplication => {
                            Ok(ValueOrRef::Value(InterpreterValue::BigInt(lnum * rnum)))
                        }
                        crate::ast::MultiplicativeOperator::Division => {
                            if rnum.is_zero() {
                                return Err(InterpreterError::Range("Attempted to divide by zero"));
                            }

                            Ok(ValueOrRef::Value(InterpreterValue::BigInt(lnum / rnum)))
                        }
                        crate::ast::MultiplicativeOperator::Remainder => {
                            let n = lnum;
                            let d = rnum;

                            if d.is_zero() {
                                return Err(InterpreterError::Range(
                                    "Attempted to get remainder of dividing by zero",
                                ));
                            }

                            if n.is_zero() {
                                return Ok(ValueOrRef::Value(InterpreterValue::BigInt(n)));
                            }

                            Ok(ValueOrRef::Value(InterpreterValue::BigInt(n % d)))
                        }
                    },
                    (InterpreterValue::Numeric(lnum), InterpreterValue::Numeric(rnum)) => {
                        match op {
                            crate::ast::MultiplicativeOperator::Multiplication => {
                                Ok(ValueOrRef::Value(InterpreterValue::Numeric(lnum * rnum)))
                            }
                            crate::ast::MultiplicativeOperator::Division => {
                                Ok(ValueOrRef::Value(InterpreterValue::Numeric(lnum / rnum)))
                            }
                            crate::ast::MultiplicativeOperator::Remainder => {
                                let n = lnum;
                                let d = rnum;

                                if n.is_nan() || d.is_nan() {
                                    return Ok(ValueOrRef::Value(InterpreterValue::Numeric(
                                        f64::NAN,
                                    )));
                                }

                                if n.is_infinite() {
                                    return Ok(ValueOrRef::Value(InterpreterValue::Numeric(
                                        f64::NAN,
                                    )));
                                }

                                if d.is_infinite() {
                                    return Ok(ValueOrRef::Value(InterpreterValue::Numeric(n)));
                                }

                                if d == 0.0 {
                                    return Ok(ValueOrRef::Value(InterpreterValue::Numeric(
                                        f64::NAN,
                                    )));
                                }

                                if n == 0.0 {
                                    return Ok(ValueOrRef::Value(InterpreterValue::Numeric(n)));
                                }

                                assert!(n.is_finite());
                                assert!(!n.is_zero());
                                assert!(d.is_finite());
                                assert!(!d.is_zero());

                                let r = n - ((n / d).floor() * d);

                                if r == 0.0 && n < 0.0 {
                                    return Ok(ValueOrRef::Value(InterpreterValue::Numeric(-0.0)));
                                }

                                Ok(ValueOrRef::Value(InterpreterValue::Numeric(r)))
                            }
                        }
                    }
                    (l, r) => Err(InterpreterError::TypeMsg(Cow::Owned(format!(
                        "Attempted operation with '{}' and '{}",
                        l.type_as_str(),
                        r.type_as_str()
                    )))),
                }
            }
        }
    }

    pub fn interpret_additive_expression(
        self: &Rc<Self>,
        expression: &AdditiveExpression,
    ) -> Result<ValueOrRef, InterpreterError> {
        match expression {
            AdditiveExpression::Multiplicative(m) => self.interpret_multiplicative_expression(m),
            AdditiveExpression::Additive(l, op, r) => {
                let lref = self.interpret_additive_expression(l.as_ref())?;
                let lval = lref.into_value();
                let rref = self.interpret_multiplicative_expression(r)?;
                let rval = rref.into_value();

                match op {
                    crate::ast::AdditiveOperator::Addition => {
                        let lprim = lval.to_primitive(self, PreferredType::Default)?;
                        let rprim = rval.to_primitive(self, PreferredType::Default)?;

                        if matches!(lprim.as_ref(), InterpreterValue::String(_))
                            || matches!(rprim.as_ref(), InterpreterValue::String(_))
                        {
                            let lstr = lprim.to_string(self)?;
                            let rstr = rprim.to_string(self)?;
                            let concat_res = format!("{}{}", lstr, rstr);

                            Ok(ValueOrRef::Value(InterpreterValue::String(Rc::new(
                                concat_res,
                            ))))
                        } else {
                            let lnum = lprim.to_numeric(self)?;
                            let rnum = rprim.to_numeric(self)?;

                            match (lnum, rnum) {
                                (InterpreterValue::BigInt(l), InterpreterValue::BigInt(r)) => {
                                    Ok(ValueOrRef::Value(InterpreterValue::BigInt(l + r)))
                                }
                                (InterpreterValue::Numeric(l), InterpreterValue::Numeric(r)) => {
                                    let x = l;
                                    let y = r;

                                    if x.is_nan()
                                        || y.is_nan()
                                        || ((x.is_infinite() && x.is_sign_positive())
                                            && (y.is_infinite() && y.is_sign_negative()))
                                        || ((x.is_infinite() && x.is_sign_negative())
                                            && (y.is_infinite() && y.is_sign_positive()))
                                    {
                                        Ok(ValueOrRef::Value(InterpreterValue::Numeric(f64::NAN)))
                                    } else if x.is_infinite() {
                                        Ok(ValueOrRef::Value(InterpreterValue::Numeric(x)))
                                    } else if y.is_infinite() {
                                        Ok(ValueOrRef::Value(InterpreterValue::Numeric(y)))
                                    } else {
                                        Ok(ValueOrRef::Value(InterpreterValue::Numeric(x + y)))
                                    }
                                }
                                (l, r) => Err(InterpreterError::TypeMsg(Cow::Owned(format!(
                                    "Attempted operation with '{}' and '{}",
                                    l.type_as_str(),
                                    r.type_as_str()
                                )))),
                            }
                        }
                    }
                    crate::ast::AdditiveOperator::Subtraction => {
                        let lnum = lval.to_numeric(self)?;
                        let rnum = rval.to_numeric(self)?;

                        match (lnum, rnum) {
                            (InterpreterValue::BigInt(l), InterpreterValue::BigInt(r)) => {
                                Ok(ValueOrRef::Value(InterpreterValue::BigInt(l - r)))
                            }
                            (InterpreterValue::Numeric(l), InterpreterValue::Numeric(r)) => {
                                let x = l;
                                let y = -r;

                                if x.is_nan()
                                    || y.is_nan()
                                    || ((x.is_infinite() && x.is_sign_positive())
                                        && (y.is_infinite() && y.is_sign_negative()))
                                    || ((x.is_infinite() && x.is_sign_negative())
                                        && (y.is_infinite() && y.is_sign_positive()))
                                {
                                    Ok(ValueOrRef::Value(InterpreterValue::Numeric(f64::NAN)))
                                } else if x.is_infinite() {
                                    Ok(ValueOrRef::Value(InterpreterValue::Numeric(x)))
                                } else if y.is_infinite() {
                                    Ok(ValueOrRef::Value(InterpreterValue::Numeric(y)))
                                } else {
                                    Ok(ValueOrRef::Value(InterpreterValue::Numeric(x + y)))
                                }
                            }
                            (l, r) => Err(InterpreterError::TypeMsg(Cow::Owned(format!(
                                "Attempted operation with '{}' and '{}",
                                l.type_as_str(),
                                r.type_as_str()
                            )))),
                        }
                    }
                }
            }
        }
    }

    pub fn interpret_shift_expression(
        self: &Rc<Self>,
        expression: &ShiftExpression,
    ) -> Result<ValueOrRef, InterpreterError> {
        match expression {
            ShiftExpression::Additive(a) => self.interpret_additive_expression(a),
            ShiftExpression::Left(l, r) => {
                let lref = self.interpret_shift_expression(l.as_ref())?;
                let lval = lref.into_value();

                let rref = self.interpret_additive_expression(r)?;
                let rval = rref.into_value();

                let lnum = lval.to_numeric(self)?;
                let rnum = rval.to_numeric(self)?;

                match (lnum, rnum) {
                    (InterpreterValue::BigInt(lnum), InterpreterValue::BigInt(rnum)) => {
                        let rnum_u128 = rnum
                            .to_u128()
                            .ok_or(InterpreterError::ShiftWithTooLargeRightSide)?;

                        Ok(ValueOrRef::Value(InterpreterValue::BigInt(
                            lnum << rnum_u128,
                        )))
                    }
                    (InterpreterValue::BigInt(_), _) | (_, InterpreterValue::BigInt(_)) => {
                        Err(InterpreterError::TypeMsg(Cow::Borrowed(
                            "Attempted operation with 'number' and 'bigint'",
                        )))
                    }
                    (lprim, rprim) => {
                        let lnum = lprim.to_number(self)?;
                        let lnum = match lnum.is_finite() {
                            true => lnum as u32,
                            false => 0,
                        };

                        let rnum = rprim.to_number(self)?;
                        let rnum = match rnum.is_finite() {
                            true => rnum as u32,
                            false => 0,
                        };

                        Ok(ValueOrRef::Value(InterpreterValue::Numeric(
                            (lnum << rnum) as f64,
                        )))
                    }
                }
            }
            ShiftExpression::Right(l, r) => {
                let lref = self.interpret_shift_expression(l.as_ref())?;
                let lval = lref.into_value();

                let rref = self.interpret_additive_expression(r)?;
                let rval = rref.into_value();

                let lnum = lval.to_numeric(self)?;
                let rnum = rval.to_numeric(self)?;

                match (lnum, rnum) {
                    (InterpreterValue::BigInt(lnum), InterpreterValue::BigInt(rnum)) => {
                        let rnum_u128 = rnum
                            .to_u128()
                            .ok_or(InterpreterError::ShiftWithTooLargeRightSide)?;

                        Ok(ValueOrRef::Value(InterpreterValue::BigInt(
                            lnum >> rnum_u128,
                        )))
                    }
                    (InterpreterValue::BigInt(_), _) | (_, InterpreterValue::BigInt(_)) => {
                        Err(InterpreterError::TypeMsg(Cow::Borrowed(
                            "Attempted operation with 'number' and 'bigint'",
                        )))
                    }
                    (lprim, rprim) => {
                        let lnum = lprim.to_number(self)?;
                        let lnum = match lnum.is_finite() {
                            true => lnum as u32,
                            false => 0,
                        };

                        let rnum = rprim.to_number(self)?;
                        let rnum = match rnum.is_finite() {
                            true => rnum as u32,
                            false => 0,
                        };

                        Ok(ValueOrRef::Value(InterpreterValue::Numeric(
                            (lnum >> rnum) as f64,
                        )))
                    }
                }
            }
            ShiftExpression::RightUnsigned(l, r) => {
                let lref = self.interpret_shift_expression(l.as_ref())?;
                let lval = lref.into_value();

                let rref = self.interpret_additive_expression(r)?;
                let rval = rref.into_value();

                let lnum = lval.to_number(self)?;
                let rnum = rval.to_number(self)?;

                let lnum = match lnum.is_finite() {
                    true => lnum as u32,
                    false => 0,
                };

                let rnum = match rnum.is_finite() {
                    true => rnum as u32,
                    false => 0,
                };

                let shift_count = rnum % 32;
                Ok(ValueOrRef::Value(InterpreterValue::Numeric(
                    (lnum >> shift_count) as f64,
                )))
            }
        }
    }

    pub fn interpret_relational_expression(
        self: &Rc<Self>,
        expression: &RelationalExpression,
    ) -> Result<ValueOrRef, InterpreterError> {
        match expression {
            RelationalExpression::Shift(s) => self.interpret_shift_expression(s),
            RelationalExpression::LessThan(l, r) => {
                let lref = self.interpret_relational_expression(l.as_ref())?;
                let lval = lref.into_value();

                let rref = self.interpret_shift_expression(r)?;
                let rval = rref.into_value();

                lval.is_less_than(&rval, true, self).map(|r| {
                    ValueOrRef::Value(match r {
                        InterpreterValue::Undefined => InterpreterValue::Boolean(false),
                        r => r,
                    })
                })
            }
            RelationalExpression::GreaterThan(l, r) => {
                let lref = self.interpret_relational_expression(l.as_ref())?;
                let lval = lref.into_value();

                let rref = self.interpret_shift_expression(r)?;
                let rval = rref.into_value();

                rval.is_less_than(&lval, false, self).map(|r| {
                    ValueOrRef::Value(match r {
                        InterpreterValue::Undefined => InterpreterValue::Boolean(false),
                        r => r,
                    })
                })
            }
            RelationalExpression::LessThanEqual(l, r) => {
                let lref = self.interpret_relational_expression(l.as_ref())?;
                let lval = lref.into_value();

                let rref = self.interpret_shift_expression(r)?;
                let rval = rref.into_value();

                rval.is_less_than(&lval, false, self).map(|r| {
                    ValueOrRef::Value(match r {
                        InterpreterValue::Boolean(true) | InterpreterValue::Undefined => {
                            InterpreterValue::Boolean(false)
                        }
                        _ => InterpreterValue::Boolean(true),
                    })
                })
            }
            RelationalExpression::GreaterThanEqual(l, r) => {
                let lref = self.interpret_relational_expression(l.as_ref())?;
                let lval = lref.into_value();

                let rref = self.interpret_shift_expression(r)?;
                let rval = rref.into_value();

                lval.is_less_than(&rval, true, self).map(|r| {
                    ValueOrRef::Value(match r {
                        InterpreterValue::Boolean(true) | InterpreterValue::Undefined => {
                            InterpreterValue::Boolean(false)
                        }
                        _ => InterpreterValue::Boolean(true),
                    })
                })
            }
            RelationalExpression::Instanceof(_, _) => todo!(),
        }
    }

    pub fn interpret_equality_expression(
        self: &Rc<Self>,
        expression: &EqualityExpression,
    ) -> Result<ValueOrRef, InterpreterError> {
        match expression {
            EqualityExpression::Relational(r) => self.interpret_relational_expression(r),
            EqualityExpression::Equality(l, op, r) => {
                let lref = self.interpret_equality_expression(l.as_ref())?;
                let lval = lref.into_value();

                let rref = self.interpret_relational_expression(r)?;
                let rval = rref.into_value();

                match op {
                    crate::ast::EqualityOperator::Equal => Ok(ValueOrRef::Value(
                        InterpreterValue::Boolean(rval.is_loosely_equal(&lval, self)?),
                    )),
                    crate::ast::EqualityOperator::NotEqual => Ok(ValueOrRef::Value(
                        InterpreterValue::Boolean(!rval.is_loosely_equal(&lval, self)?),
                    )),
                    crate::ast::EqualityOperator::StrictEqual => Ok(ValueOrRef::Value(
                        InterpreterValue::Boolean(rval.is_strictly_equal(&lval)),
                    )),
                    crate::ast::EqualityOperator::StrictNotEqual => Ok(ValueOrRef::Value(
                        InterpreterValue::Boolean(!rval.is_strictly_equal(&lval)),
                    )),
                }
            }
        }
    }

    pub fn interpret_bitwise_and_expression(
        self: &Rc<Self>,
        expression: &BitwiseAndExpression,
    ) -> Result<ValueOrRef, InterpreterError> {
        match expression {
            BitwiseAndExpression::Equality(e) => self.interpret_equality_expression(e),
            BitwiseAndExpression::BitwiseAnd(l, r) => {
                let lref = self.interpret_bitwise_and_expression(l)?;
                let lval = lref.into_value();

                let rref = self.interpret_equality_expression(r)?;
                let rval = rref.into_value();

                let lnum = lval.to_numeric(self)?;
                let rnum = rval.to_numeric(self)?;

                match (lnum, rnum) {
                    (InterpreterValue::BigInt(lnum), InterpreterValue::BigInt(rnum)) => {
                        Ok(ValueOrRef::Value(InterpreterValue::BigInt(lnum & rnum)))
                    }
                    (InterpreterValue::Numeric(lnum), InterpreterValue::Numeric(rnum)) => {
                        Ok(ValueOrRef::Value(InterpreterValue::Numeric(
                            (lnum as i32 & rnum as i32) as f64,
                        )))
                    }
                    (l, r) => Err(InterpreterError::TypeMsg(Cow::Owned(format!(
                        "Attempted operation with '{}' and '{}",
                        l.type_as_str(),
                        r.type_as_str()
                    )))),
                }
            }
        }
    }

    pub fn interpret_bitwise_xor_expression(
        self: &Rc<Self>,
        expression: &BitwiseXorExpression,
    ) -> Result<ValueOrRef, InterpreterError> {
        match expression {
            BitwiseXorExpression::BitwiseAnd(b) => self.interpret_bitwise_and_expression(b),
            BitwiseXorExpression::BitwiseXor(l, r) => {
                let lref = self.interpret_bitwise_xor_expression(l)?;
                let lval = lref.into_value();

                let rref = self.interpret_bitwise_and_expression(r)?;
                let rval = rref.into_value();

                let lnum = lval.to_numeric(self)?;
                let rnum = rval.to_numeric(self)?;

                match (lnum, rnum) {
                    (InterpreterValue::BigInt(lnum), InterpreterValue::BigInt(rnum)) => {
                        Ok(ValueOrRef::Value(InterpreterValue::BigInt(lnum ^ rnum)))
                    }
                    (InterpreterValue::Numeric(lnum), InterpreterValue::Numeric(rnum)) => {
                        Ok(ValueOrRef::Value(InterpreterValue::Numeric(
                            (lnum as i32 ^ rnum as i32) as f64,
                        )))
                    }
                    (l, r) => Err(InterpreterError::TypeMsg(Cow::Owned(format!(
                        "Attempted operation with '{}' and '{}",
                        l.type_as_str(),
                        r.type_as_str()
                    )))),
                }
            }
        }
    }

    pub fn interpret_bitwise_or_expression(
        self: &Rc<Self>,
        expression: &BitwiseOrExpression,
    ) -> Result<ValueOrRef, InterpreterError> {
        match expression {
            BitwiseOrExpression::BitwiseXor(b) => self.interpret_bitwise_xor_expression(b),
            BitwiseOrExpression::BitwiseOr(l, r) => {
                let lref = self.interpret_bitwise_or_expression(l)?;
                let lval = lref.into_value();

                let rref = self.interpret_bitwise_xor_expression(r)?;
                let rval = rref.into_value();

                let lnum = lval.to_numeric(self)?;
                let rnum = rval.to_numeric(self)?;

                match (lnum, rnum) {
                    (InterpreterValue::BigInt(lnum), InterpreterValue::BigInt(rnum)) => {
                        Ok(ValueOrRef::Value(InterpreterValue::BigInt(lnum | rnum)))
                    }
                    (InterpreterValue::Numeric(lnum), InterpreterValue::Numeric(rnum)) => {
                        Ok(ValueOrRef::Value(InterpreterValue::Numeric(
                            (lnum as i32 | rnum as i32) as f64,
                        )))
                    }
                    (l, r) => Err(InterpreterError::TypeMsg(Cow::Owned(format!(
                        "Attempted operation with '{}' and '{}",
                        l.type_as_str(),
                        r.type_as_str()
                    )))),
                }
            }
        }
    }

    pub fn interpret_coalesce_expression(
        self: &Rc<Self>,
        expression: &CoalesceExpression,
    ) -> Result<ValueOrRef, InterpreterError> {
        match expression {
            CoalesceExpression::BitwiseOr(b) => self.interpret_bitwise_or_expression(b),
            CoalesceExpression::Coalesce(l, r) => {
                let lref = self.interpret_coalesce_expression(l.as_ref())?;
                let lval = lref.into_value();

                if lval == InterpreterValue::Undefined || lval == InterpreterValue::Null {
                    let rref = self.interpret_bitwise_or_expression(r)?;
                    Ok(ValueOrRef::Value(rref.into_value()))
                } else {
                    Ok(ValueOrRef::Value(lval))
                }
            }
        }
    }

    pub fn interpret_logical_and_expression(
        self: &Rc<Self>,
        expression: &LogicalAndExpression,
    ) -> Result<ValueOrRef, InterpreterError> {
        match expression {
            LogicalAndExpression::BitwiseOr(b) => self.interpret_bitwise_or_expression(b),
            LogicalAndExpression::LogicalAnd(l, r) => {
                let lref = self.interpret_logical_and_expression(l.as_ref())?;
                let lval = lref.into_value();
                let lbool = lval.as_bool();

                if !lbool {
                    Ok(ValueOrRef::Value(lval))
                } else {
                    let rref = self.interpret_bitwise_or_expression(r)?;
                    Ok(ValueOrRef::Value(rref.into_value()))
                }
            }
        }
    }

    pub fn interpret_logical_or_expression(
        self: &Rc<Self>,
        expression: &LogicalOrExpression,
    ) -> Result<ValueOrRef, InterpreterError> {
        match expression {
            LogicalOrExpression::LogicalAnd(l) => self.interpret_logical_and_expression(l),
            LogicalOrExpression::LogicalOr(l, r) => {
                let lref = self.interpret_logical_or_expression(l.as_ref())?;
                let lval = lref.into_value();
                let lbool = lval.as_bool();

                if lbool {
                    Ok(ValueOrRef::Value(lval))
                } else {
                    let rref = self.interpret_logical_and_expression(r)?;
                    Ok(ValueOrRef::Value(rref.into_value()))
                }
            }
        }
    }

    pub fn interpret_short_circuit_expression(
        self: &Rc<Self>,
        expression: &ShortCircuitExpression,
    ) -> Result<ValueOrRef, InterpreterError> {
        match expression {
            ShortCircuitExpression::LogicalOr(l) => self.interpret_logical_or_expression(l),
            ShortCircuitExpression::Coalesce(c) => self.interpret_coalesce_expression(c),
        }
    }

    pub fn interpret_conditional_expression(
        self: &Rc<Self>,
        expression: &ConditionalExpression,
    ) -> Result<ValueOrRef, InterpreterError> {
        match expression {
            ConditionalExpression::ShortCircuit(sc) => self.interpret_short_circuit_expression(sc),
            ConditionalExpression::Conditional(cond_expr, true_expr, false_expr) => {
                let lref = self.interpret_short_circuit_expression(cond_expr)?;
                let lval = lref.into_value().as_bool();
                if lval {
                    let true_ref = self.interpret_assignment_expression(true_expr.as_ref())?;
                    Ok(ValueOrRef::Value(true_ref.into_value()))
                } else {
                    let false_ref = self.interpret_assignment_expression(false_expr.as_ref())?;
                    Ok(ValueOrRef::Value(false_ref.into_value()))
                }
            }
        }
    }

    pub fn interpret_assignment_expression(
        self: &Rc<Self>,
        expression: &AssignmentExpression,
    ) -> Result<ValueOrRef, InterpreterError> {
        match expression {
            AssignmentExpression::Conditional(c) => self.interpret_conditional_expression(c),
            AssignmentExpression::Assign(l, op, r) => self
                .handle_assignment(l, *op, r.as_ref())
                .map(ValueOrRef::Value),
        }
    }

    pub fn interpret_expression(
        self: &Rc<Self>,
        expression: &Expression,
    ) -> Result<ValueOrRef, InterpreterError> {
        match expression {
            Expression::Assignment(a) => self.interpret_assignment_expression(a),
            Expression::Comma(e, r) => {
                self.interpret_expression(e.as_ref())?.into_value();
                Ok(ValueOrRef::Value(
                    self.interpret_assignment_expression(r)?.into_value(),
                ))
            }
        }
        /*
        match expression.as_ref() {
            Expression::Identifier(ident) => {
                let ident = PropertyNameValue::Identifier(ident.clone());

                let value = self
                    .fetch_variable(&ident)
                    .ok_or(InterpreterError::UndefinedIdentifierReference(ident))?
                    .value()
                    .clone();

                Ok(ValueOrRef::Ref(Ref::ValRef(value)))
            }
            Expression::Value(value) => Ok(ValueOrRef::Value(InterpreterValue::from_ast_value(
                self,
                value.clone(),
            )?)),
            Expression::FunctionCall(func_ref_expr, params) => {
                let callable = self
                    .interpret_expression(func_ref_expr.clone())?
                    .into_value()
                    .downcast_to_function()?;

                self.call_function(callable, params.as_slice())
                    .map(ValueOrRef::Value)
            }
            Expression::Grouping(e) => self.interpret_expression(e.clone()),
            Expression::PropertyAccess(v, r) => {
                let value_or_ref = self.interpret_expression(v.clone())?;
                let value = value_or_ref.into_value();

                match value {
                    InterpreterValue::Undefined => Err(InterpreterError::TypeMsg(
                        "Attempted to access a property on 'undefined'",
                    )),
                    InterpreterValue::Null => Err(InterpreterError::TypeMsg(
                        "Attempted to access a property on 'null'",
                    )),
                    InterpreterValue::Boolean(_)
                    | InterpreterValue::String(_)
                    | InterpreterValue::Numeric(_)
                    | InterpreterValue::BigInt(_) => {
                        Ok(ValueOrRef::Value(InterpreterValue::Undefined))
                    }
                    InterpreterValue::Object(o) => {
                        let ident = PropertyNameValue::Identifier(r.clone());
                        match o.get(&ident) {
                            Some(value) => Ok(ValueOrRef::Ref(Ref::ValRef(value))),
                            None => Ok(ValueOrRef::Ref(Ref::PropRef(o, ident))),
                        }
                    }
                }
            }
            Expression::PropertyAccessBrackets(v, r) => {
                let access_value_str = self
                    .interpret_expression(r.clone())?
                    .into_value()
                    .to_string(self)?;
                let property_name_value = PropertyNameValue::String(access_value_str);

                let value_or_ref = self.interpret_expression(v.clone())?;
                let value = value_or_ref.into_value();

                match value {
                    InterpreterValue::Undefined => Err(InterpreterError::TypeMsg(
                        "Attempted to access a property on 'undefined'",
                    )),
                    InterpreterValue::Null => Err(InterpreterError::TypeMsg(
                        "Attempted to access a property on 'null'",
                    )),
                    InterpreterValue::Boolean(_)
                    | InterpreterValue::String(_)
                    | InterpreterValue::Numeric(_)
                    | InterpreterValue::BigInt(_) => {
                        Ok(ValueOrRef::Value(InterpreterValue::Undefined))
                    }
                    InterpreterValue::Object(o) => match o.get(&property_name_value) {
                        Some(value) => Ok(ValueOrRef::Ref(Ref::ValRef(value))),
                        None => Ok(ValueOrRef::Ref(Ref::PropRef(o, property_name_value))),
                    },
                }
            }
            Expression::Equality(ls, op, rs) => {
                let ls = self.interpret_expression(ls.clone())?.into_value();
                let rs = self.interpret_expression(rs.clone())?.into_value();

                Ok(ValueOrRef::Value(InterpreterValue::Boolean(match op {
                    crate::ast::EqualityOperator::Equal => ls.is_loosely_equal(&rs, self)?,
                    crate::ast::EqualityOperator::NotEqual => !ls.is_loosely_equal(&rs, self)?,
                    crate::ast::EqualityOperator::StrictEqual => ls.is_strictly_equal(&rs),
                    crate::ast::EqualityOperator::StrictNotEqual => !ls.is_strictly_equal(&rs),
                })))
            }
            Expression::Assignment(ls, op, rs) => self
                .handle_assignment(ls.clone(), *op, rs.clone())
                .map(ValueOrRef::Value),
            Expression::Multiplicative(ls, op, rs) => todo!(),
            Expression::Additive(ls, op, rs) => todo!(),
            Expression::Exponentiation(ls, rs) => todo!(),
            Expression::Unary(op, rs) => {

            }
        }*/
    }
}
