use std::fmt::Display;
use std::{borrow::Cow, rc::Rc};

use crate::ast::{Function, NodeParse, NodeParseErrorWithSpan, PropertyNameValue};
use crate::tt::TokenWithSpan;
use crate::{
    parser::{Parse, Parser, ParserError},
    tt::{Keyword, Token, TokenTree},
};

use crate::interpreter::handler_function::closure_fn;

use super::{
    handler_function::RuntimeFuntion,
    variables::{InterpreterValue, Object},
};

#[derive(Debug, PartialEq, Clone)]
pub enum ParentError {
    NodeParse(NodeParseErrorWithSpan),
    Parser(ParserError),
}

impl From<NodeParseErrorWithSpan> for ParentError {
    fn from(e: NodeParseErrorWithSpan) -> Self {
        Self::NodeParse(e)
    }
}

impl From<ParserError> for ParentError {
    fn from(e: ParserError) -> Self {
        Self::Parser(e)
    }
}

impl Display for ParentError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ParentError::NodeParse(NodeParseErrorWithSpan {
                error,
                span: Some(span),
                ..
            }) => f.write_fmt(format_args!(
                "Node parse error: {} (start: {}, end: {})",
                error, span.source_start, span.source_end
            )),
            ParentError::NodeParse(NodeParseErrorWithSpan { error, .. }) => {
                f.write_fmt(format_args!("Node parse error: {}", error))
            }
            ParentError::Parser(p) => f.write_fmt(format_args!("Parser error: {}", p)),
        }
    }
}

impl std::error::Error for ParentError {}

fn property_name(s: &str) -> PropertyNameValue {
    PropertyNameValue::String(Rc::new(s.to_string()))
}

macro_rules! create_property {
    ($o:ident, $name:literal, $value:expr) => {
        create_property!($o, $name, $value, true)
    };
    ($o:ident, $name:literal, $value:expr, $writeable:literal) => {{
        let name = property_name($name);
        $o.set(name.clone(), $value);
        let writable: bool = $writeable;
        if !writable {
            $o.write_ignore(name);
        }
    }};
}

fn assign_rust_function<F: RuntimeFuntion>(object: &Object, name: &str, f: F) {
    object.set(
        property_name(name),
        InterpreterValue::Object(Object::new_rust_fn(Some(Rc::new(object.clone())), f)),
    );
}

fn make_js_function(s: &str) -> Result<Object, ParentError> {
    let mut parser = Parser::new(None, Cow::Borrowed(s));
    let mut token_tree = TokenTree::parse(&mut parser)?.unwrap();

    assert!(token_tree.peek_is_next(&Token::Keyword(Keyword::Function)));

    let ident = match token_tree.peek_next() {
        Some(TokenWithSpan {
            token: Token::Identifier(ident),
            ..
        }) => Some(ident),
        Some(_) => {
            token_tree.unpeek(1);
            None
        }
        None => None,
    };

    Ok(Object::new_fn(
        None,
        ident,
        Function::node_parse(&mut token_tree)?,
        None,
    ))
}

pub fn global_object() -> Result<Object, ParentError> {
    let global = Object::new(None);

    create_property!(
        global,
        "Infinity",
        InterpreterValue::Numeric(f64::INFINITY),
        false
    );
    create_property!(global, "NaN", InterpreterValue::Numeric(f64::NAN), false);
    create_property!(global, "undefined", InterpreterValue::Undefined, false);
    create_property!(
        global,
        "test",
        InterpreterValue::Object(make_js_function(
            r#"function(x) {
        return "test";
    }"#
        )?)
    );

    assign_rust_function(
        &global,
        "eval",
        closure_fn(|_ls, _rs| Ok(InterpreterValue::Undefined)),
    );

    Ok(global)
}

pub fn console_object(global: Rc<Object>) -> Object {
    let console_object = Object::new(Some(global));

    assign_rust_function(
        &console_object,
        "log",
        closure_fn(|parameters, layer| {
            let mut vals = Vec::new();

            for param in parameters {
                vals.push(
                    layer
                        .interpret_assignment_expression(param.as_ref())?
                        .into_value()
                        .to_string(layer)?,
                );
            }

            let final_i = vals.len() - 1;
            for (i, v) in vals.into_iter().enumerate() {
                if i != final_i {
                    print!("{} ", v);
                } else {
                    print!("{}", v);
                }
            }
            println!();

            Ok(InterpreterValue::Undefined)
        }),
    );

    console_object
}
