use std::rc::Rc;

use crate::ast::{PropertyNameValue, Statement};

use super::{
    error::InterpreterError,
    variables::{InterpreterValue, Object, ValueOrRef, Variable, VariableType},
    InterpreterLayer, State,
};

impl InterpreterLayer {
    pub fn interpret_statement(
        self: &Rc<Self>,
        statement: Rc<Statement>,
    ) -> Result<State, InterpreterError> {
        match statement.as_ref() {
            Statement::Block(block) => {
                return self
                    .new_child_layer(None, None, false)
                    .interpret_tree(block.child_tree.clone());
            }
            Statement::SingleDeclaration(decs) => {
                for (ident, expr) in decs {
                    let val = match expr.as_ref() {
                        Some(expr) => self.interpret_assignment_expression(expr)?.into_value(),
                        None => InterpreterValue::Undefined,
                    };

                    self.set_variable(
                        PropertyNameValue::Identifier(ident.clone()),
                        Variable::new(val, VariableType::Let),
                    )?;
                }
            }
            Statement::Return(ret_exp) => {
                return Ok(State::Return(match ret_exp.as_ref() {
                    Some(value) => self.interpret_expression(value)?.into_value(),
                    None => InterpreterValue::Undefined,
                }))
            }
            Statement::FunctionDeclaration(ident, func) => {
                self.set_variable(
                    PropertyNameValue::Identifier(ident.clone()),
                    Variable::new(
                        InterpreterValue::Object(Object::new_fn(
                            None,
                            None,
                            func.clone(),
                            Some(self.clone()),
                        )),
                        VariableType::FuncDec,
                    ),
                )?;
            }
            Statement::VarDeclaration(decs) => {
                for (ident, expr) in decs {
                    let val = match expr.as_ref() {
                        Some(expr) => self.interpret_assignment_expression(expr)?.into_value(),
                        None => InterpreterValue::Undefined,
                    };

                    self.set_variable(
                        PropertyNameValue::Identifier(ident.clone()),
                        Variable::new(val, VariableType::Var),
                    )?;
                }
            }
            Statement::ConstDeclaration(decs) => {
                for (ident, expr) in decs {
                    let val = self.interpret_assignment_expression(expr)?.into_value();

                    self.set_variable(
                        PropertyNameValue::Identifier(ident.clone()),
                        Variable::new(val, VariableType::Const),
                    )?;
                }
            }
            Statement::If {
                condition,
                body,
                else_body,
            } => {
                let condition_val = self.interpret_expression(condition)?.into_value().as_bool();

                if condition_val {
                    return self.interpret_statement(body.clone());
                } else if let Some(else_body) = else_body {
                    return self.interpret_statement(else_body.clone());
                }
            }
            Statement::While { condition, body } => {
                while self.interpret_expression(condition)?.into_value().as_bool() {
                    if let State::Return(v) = self.interpret_statement(body.clone())? {
                        return Ok(State::Return(v));
                    }
                }
            }
        }

        Ok(State::ValueOrRef(ValueOrRef::Value(
            InterpreterValue::Undefined,
        )))
    }
}
