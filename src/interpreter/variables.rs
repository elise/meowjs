use std::{
    borrow::Cow,
    cell::RefCell,
    fmt::Debug,
    mem::{discriminant, Discriminant},
    rc::Rc,
};

use indexmap::{IndexMap, IndexSet};
use lazy_static::lazy_static;
use num::{BigInt, FromPrimitive, ToPrimitive, Zero};
use uuid::Uuid;

use crate::{
    ast::{AssignmentExpression, AstValue, Function, PropertyNameValue},
    tt::Identifier,
};

use super::{error::InterpreterError, handler_function::RuntimeFuntion, InterpreterLayer};

#[derive(Clone)]
pub enum Callable {
    Native {
        function: Function,
        layer: Option<Rc<InterpreterLayer>>,
    },
    Rust {
        handler: Rc<Box<dyn RuntimeFuntion>>,
    },
}

impl Debug for Callable {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Native { function, layer } => f
                .debug_struct("JSFunction")
                .field("function", function)
                .field("layer", layer)
                .finish(),
            Self::Rust { .. } => f.debug_struct("RustFunction").finish(),
        }
    }
}

#[derive(Clone)]
pub struct Object {
    pub id: Uuid,
    pub parent: Option<Rc<Self>>,
    store: Rc<RefCell<IndexMap<PropertyNameValue, WrappedValue>>>,
    callable: Option<Callable>,
    write_ignore: Rc<RefCell<IndexSet<PropertyNameValue>>>,
}

impl Debug for Object {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Object")
            .field("id", &self.id)
            .field("has_parent", &self.parent.is_some())
            .field("store", &self.store)
            .field("callable", &self.callable)
            .field("write_ignore", &self.write_ignore)
            .finish()
    }
}

impl PartialEq for Object {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl Object {
    pub fn new_rust_fn<T: RuntimeFuntion>(parent: Option<Rc<Self>>, function: T) -> Self {
        Self {
            id: Uuid::new_v4(),
            parent,
            store: Rc::new(RefCell::new(IndexMap::new())),
            callable: Some(Callable::Rust {
                handler: Rc::new(Box::new(function) as Box<_>),
            }),
            write_ignore: Rc::new(RefCell::new(IndexSet::new())),
        }
    }

    pub fn to_variable_map(&self) -> IndexMap<PropertyNameValue, Variable> {
        let write_ignore_store = self.write_ignore.borrow();
        let store = IndexMap::clone(&*self.store.borrow());

        store
            .into_iter()
            .map(|(name, value)| {
                let vartype = match write_ignore_store.contains(&name) {
                    true => VariableType::SilentlyIgnoreWrites,
                    false => VariableType::Var,
                };

                (name, Variable::new(value.into_value(), vartype))
            })
            .collect()
    }

    pub fn new(parent: Option<Rc<Self>>) -> Self {
        Self {
            id: Uuid::new_v4(),
            parent,
            store: Rc::new(RefCell::new(IndexMap::new())),
            callable: None,
            write_ignore: Rc::new(RefCell::new(IndexSet::new())),
        }
    }

    pub fn new_with_store(
        parent: Option<Rc<Self>>,
        store: IndexMap<PropertyNameValue, WrappedValue>,
    ) -> Self {
        Self {
            id: Uuid::new_v4(),
            parent,
            store: Rc::new(RefCell::new(store)),
            callable: None,
            write_ignore: Rc::new(RefCell::new(IndexSet::new())),
        }
    }

    pub fn new_array(
        parent: Option<Rc<Self>>,
        store: IndexMap<PropertyNameValue, WrappedValue>,
    ) -> Self {
        Self {
            id: Uuid::new_v4(),
            parent,
            store: Rc::new(RefCell::new(store)),
            callable: None,
            write_ignore: Rc::new(RefCell::new(IndexSet::new())),
        }
    }

    pub fn new_fn(
        parent: Option<Rc<Self>>,
        identifier: Option<Identifier>,
        function: Function,
        layer: Option<Rc<InterpreterLayer>>,
    ) -> Self {
        let res = Self {
            parent,
            store: Rc::new(RefCell::new(IndexMap::new())),
            callable: Some(Callable::Native { function, layer }),
            id: Uuid::new_v4(),
            write_ignore: Rc::new(RefCell::new(IndexSet::new())),
        };

        if let Some(identifier) = identifier {
            let cloned = res.clone();
            res.set(
                PropertyNameValue::Identifier(identifier),
                InterpreterValue::Object(cloned),
            );
        }

        res
    }

    pub fn into_function(self) -> Option<Callable> {
        self.callable
    }

    pub fn get_function(&self) -> Option<&Callable> {
        self.callable.as_ref()
    }

    fn get_internal(&self, index: &PropertyNameValue) -> Option<WrappedValue> {
        let value_opt = self.store.borrow().get(index).cloned();

        match value_opt {
            Some(store) => Some(store),
            None => match self.parent.as_ref() {
                Some(parent) => parent.get_internal(index),
                None => None,
            },
        }
    }

    pub fn get(&self, index: &PropertyNameValue) -> Option<WrappedValue> {
        let normalised = index.normalise();
        self.get_internal(normalised.as_ref())
    }

    pub fn set(&self, index: PropertyNameValue, value: InterpreterValue) {
        let index = index.normalise().into_owned();

        if self.write_ignore.borrow().contains(&index) {
            return;
        }

        self.store
            .borrow_mut()
            .insert(index, WrappedValue::new(value));
    }

    pub fn write_ignore(&self, index: PropertyNameValue) {
        self.write_ignore.borrow_mut().insert(index);
    }

    pub fn get_method(&self, method: &str) -> Option<Callable> {
        let index = PropertyNameValue::String(Rc::new(method.to_string()));
        let index = index.normalise();
        {
            let borrow = self.store.borrow();
            let res = borrow.get(index.as_ref())?;

            if res.is_object() {
                let res = res.clone().into_value();
                drop(borrow);

                if let InterpreterValue::Object(o) = res {
                    return o.get_function().cloned();
                }
            } else {
                drop(borrow);
            }
        }

        None
    }
}

#[derive(Debug, Clone)]
pub enum InterpreterValue {
    /// Only has one value: `undefined`
    Undefined,
    /// Only has one value: `null`
    Null,
    /// Has two possible values:
    ///
    /// 1. `true`
    /// 2. `false`
    Boolean(bool),
    /// UTF-16 String
    String(Rc<String>),
    /// f64
    Numeric(f64),
    /// BigInt
    BigInt(BigInt),
    /// Object
    Object(Object),
}

impl Default for InterpreterValue {
    fn default() -> Self {
        Self::Undefined
    }
}

impl PartialEq for InterpreterValue {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Boolean(l0), Self::Boolean(r0)) => l0 == r0,
            (Self::String(l0), Self::String(r0)) => l0 == r0,
            (Self::Numeric(l0), Self::Numeric(r0)) => l0 == r0,
            (Self::BigInt(l0), Self::BigInt(r0)) => l0 == r0,
            (Self::Object(l0), Self::Object(r0)) => l0 == r0,
            _ => core::mem::discriminant(self) == core::mem::discriminant(other),
        }
    }
}

lazy_static! {
    static ref NUM_DIS: Discriminant<InterpreterValue> =
        discriminant(&InterpreterValue::Numeric(0.0));
    static ref BIGINT_DIS: Discriminant<InterpreterValue> =
        discriminant(&InterpreterValue::BigInt(BigInt::default()));
    static ref BOOL_DIS: Discriminant<InterpreterValue> =
        discriminant(&InterpreterValue::Boolean(false));
    static ref STR_DIS: Discriminant<InterpreterValue> =
        discriminant(&InterpreterValue::String(Rc::new(String::new())));
    static ref OBJ_DIS: Discriminant<InterpreterValue> =
        discriminant(&InterpreterValue::Object(Object::new(None)));
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum PreferredType {
    String,
    Number,
    Default,
}

impl PreferredType {
    pub fn as_str(self) -> &'static str {
        match self {
            PreferredType::String => "string",
            PreferredType::Number => "number",
            PreferredType::Default => "default",
        }
    }
}

impl InterpreterValue {
    pub fn is_less_than(
        &self,
        y: &Self,
        left_first: bool,
        layer: &Rc<InterpreterLayer>,
    ) -> Result<Self, InterpreterError> {
        let (px, py) = match left_first {
            true => (
                self.to_primitive(layer, PreferredType::Number)?,
                y.to_primitive(layer, PreferredType::Number)?,
            ),
            false => {
                let py = y.to_primitive(layer, PreferredType::Number)?;
                (self.to_primitive(layer, PreferredType::Number)?, py)
            }
        };

        match (px.as_ref(), py.as_ref()) {
            (InterpreterValue::String(px), InterpreterValue::String(py)) => {
                if py.starts_with(px.as_str()) {
                    Ok(Self::Boolean(false))
                } else if px.starts_with(py.as_str()) {
                    Ok(Self::Boolean(true))
                } else {
                    for (m, n) in px.chars().zip(py.chars()) {
                        if m != n {
                            return Ok(Self::Boolean(m < n));
                        }
                    }

                    unreachable!("Neither PX starts with PY, nor PY starts with PX, therefore PX != PY and this is unreachable");
                }
            }
            (InterpreterValue::BigInt(px), InterpreterValue::String(py)) => {
                match crate::util::parse_bigint(py.as_str()) {
                    Ok(ny) => Ok(Self::Boolean(px < &ny)),
                    Err(_) => Ok(Self::Undefined),
                }
            }
            (InterpreterValue::String(px), InterpreterValue::BigInt(py)) => {
                match crate::util::parse_bigint(px.as_str()) {
                    Ok(nx) => Ok(Self::Boolean(&nx < py)),
                    Err(_) => Ok(Self::Undefined),
                }
            }
            (px, py) => {
                let nx = px.to_numeric(layer)?;
                let ny = py.to_numeric(layer)?;

                match (nx, ny) {
                    (InterpreterValue::Numeric(nx), InterpreterValue::Numeric(ny)) => {
                        Ok(Self::Boolean(nx < ny))
                    }
                    (InterpreterValue::BigInt(nx), InterpreterValue::BigInt(ny)) => {
                        Ok(Self::Boolean(nx < ny))
                    }
                    (InterpreterValue::BigInt(nx), InterpreterValue::Numeric(ny)) => {
                        if ny.is_nan() {
                            Ok(Self::Undefined)
                        } else if ny.is_infinite() {
                            Ok(Self::Boolean(ny.is_sign_positive()))
                        } else {
                            let nxi = nx.to_f64().expect("Unfailiable comparison for BigInt");
                            Ok(Self::Boolean(nxi < ny))
                        }
                    }
                    (InterpreterValue::Numeric(nx), InterpreterValue::BigInt(ny)) => {
                        if nx.is_nan() {
                            Ok(Self::Undefined)
                        } else if nx.is_infinite() {
                            Ok(Self::Boolean(nx.is_sign_negative()))
                        } else {
                            let nyi = ny.to_f64().expect("Unfailiable comparison for BigInt");
                            Ok(Self::Boolean(nx < nyi))
                        }
                    }
                    _ => unreachable!("nx and ny are both numeric"),
                }
            }
        }
    }

    pub fn to_numeric(&self, layer: &Rc<InterpreterLayer>) -> Result<Self, InterpreterError> {
        let prim = self.to_primitive(layer, PreferredType::Number)?;

        match prim.into_owned() {
            InterpreterValue::BigInt(b) => Ok(InterpreterValue::BigInt(b)),
            v => v.to_number(layer).map(InterpreterValue::Numeric),
        }
    }

    pub fn to_string(&self, layer: &Rc<InterpreterLayer>) -> Result<Rc<String>, InterpreterError> {
        match self {
            InterpreterValue::Undefined => Ok(Rc::new(String::from("undefined"))),
            InterpreterValue::Null => Ok(Rc::new(String::from("null"))),
            InterpreterValue::Boolean(b) => Ok(Rc::new(String::from(match b {
                true => "true",
                false => "false",
            }))),
            InterpreterValue::Numeric(n) => Ok(Rc::new(n.to_string())),
            InterpreterValue::String(s) => Ok(s.clone()),
            InterpreterValue::BigInt(b) => Ok(Rc::new(b.to_str_radix(10))),
            InterpreterValue::Object(_) => self
                .to_primitive(layer, PreferredType::String)?
                .as_ref()
                .to_string(layer),
        }
    }

    pub fn to_number(&self, layer: &Rc<InterpreterLayer>) -> Result<f64, InterpreterError> {
        match self {
            InterpreterValue::Undefined => Ok(f64::NAN),
            InterpreterValue::Null => Ok(0.0f64),
            InterpreterValue::Boolean(b) => Ok(match b {
                true => 1.0f64,
                false => 0.0f64,
            }),
            InterpreterValue::String(s) => {
                Ok(crate::util::parse_number(s.as_str()).unwrap_or(f64::NAN))
            }
            InterpreterValue::Numeric(s) => Ok(*s),
            InterpreterValue::BigInt(_) => Err(InterpreterError::Type(
                AstValue::BIGINT_STR,
                AstValue::NUMERIC_STR,
            )),
            InterpreterValue::Object(_) => self
                .to_primitive(layer, PreferredType::Number)?
                .to_number(layer),
        }
    }

    pub fn is_object(&self) -> bool {
        matches!(self, Self::Object(_))
    }

    pub fn to_primitive<'s>(
        &'s self,
        layer: &Rc<InterpreterLayer>,
        preferred_type: PreferredType,
    ) -> Result<Cow<'s, Self>, InterpreterError> {
        match self {
            InterpreterValue::Object(obj) => {
                let to_primitive_method_opt = obj.get_method("toPrimitive");

                if let Some(callable) = to_primitive_method_opt {
                    let res = layer.call_function(
                        callable,
                        &[Rc::new(AssignmentExpression::from(AstValue::String(
                            Rc::new(String::from(preferred_type.as_str())),
                        )))],
                    )?;

                    return match res.is_object() {
                        true => Err(InterpreterError::Type("primitive", res.type_as_str())),
                        false => Ok(Cow::Owned(res)),
                    };
                }

                let method_names: &[&'static str; 2] = match preferred_type {
                    PreferredType::String => &["toString", "valueOf"],
                    PreferredType::Number | PreferredType::Default => &["valueOf", "toString"],
                };

                for &method in method_names {
                    if let Some(callable) = obj.get_method(method) {
                        let res = layer.call_function(callable, &[])?;

                        if !res.is_object() {
                            return Ok(Cow::Owned(res));
                        }
                    }
                }

                Err(InterpreterError::Type("primitive", AstValue::OBJECT_STR))
            }
            p => Ok(Cow::Borrowed(p)),
        }
    }

    pub fn is_loosely_equal(
        &self,
        other: &Self,
        layer: &Rc<InterpreterLayer>,
    ) -> Result<bool, InterpreterError> {
        if discriminant(self) == discriminant(other) {
            return Ok(self == other);
        }

        let ls_undefined_or_null =
            self == &InterpreterValue::Undefined || self == &InterpreterValue::Null;
        let rs_undefined_or_null =
            other == &InterpreterValue::Undefined || other == &InterpreterValue::Null;

        if ls_undefined_or_null && rs_undefined_or_null {
            return Ok(true);
        }

        if discriminant(self) == *NUM_DIS {
            if let Self::String(other) = other {
                return self.is_loosely_equal(
                    &Self::Numeric(crate::util::parse_number(other.as_str()).unwrap_or(f64::NAN)),
                    layer,
                );
            }
        }

        if let Self::String(s) = self {
            if discriminant(other) == *NUM_DIS {
                return other.is_loosely_equal(
                    &Self::Numeric(crate::util::parse_number(s.as_str()).unwrap_or(f64::NAN)),
                    layer,
                );
            }
        }

        if discriminant(self) == *BIGINT_DIS {
            if let Self::String(other) = other {
                let bigint = match crate::util::parse_bigint(other.as_str()) {
                    Ok(bigint) => bigint,
                    Err(_) => return Ok(false),
                };

                return self.is_loosely_equal(&Self::BigInt(bigint), layer);
            }
        }

        if discriminant(self) == *STR_DIS && discriminant(other) == *BIGINT_DIS {
            return other.is_loosely_equal(self, layer);
        }

        for &(l, r) in &[(self, other), (other, self)] {
            if discriminant(l) == *BOOL_DIS {
                return Self::Numeric(l.to_number(layer)?).is_loosely_equal(r, layer);
            }
        }

        if discriminant(self) == *BOOL_DIS {
            return Self::Numeric(self.to_number(layer)?).is_loosely_equal(other, layer);
        }

        if discriminant(other) == *BOOL_DIS {
            return Self::Numeric(other.to_number(layer)?).is_loosely_equal(self, layer);
        }

        for &(l, r) in &[(self, other), (other, self)] {
            if (discriminant(l) == *STR_DIS
                || discriminant(l) == *NUM_DIS
                || discriminant(l) == *BIGINT_DIS)
                && discriminant(r) == *OBJ_DIS
            {
                return l.is_loosely_equal(
                    r.clone()
                        .to_primitive(layer, PreferredType::Default)?
                        .as_ref(),
                    layer,
                );
            }
        }

        for (n, b) in &[(self, other), (other, self)] {
            if let Self::Numeric(n) = n {
                if let Self::BigInt(b) = b {
                    if !n.is_finite() || n.is_nan() || n.fract() != 0.0 {
                        return Ok(false);
                    }

                    if let Some(n_b) = BigInt::from_f64(*n) {
                        return Ok(b == &n_b);
                    };
                }
            }
        }

        Ok(false)
    }

    pub fn same_value_non_number(&self, other: &Self) -> bool {
        assert_eq!(discriminant(self), discriminant(other));
        assert_ne!(discriminant(self), *NUM_DIS);

        match (self, other) {
            (InterpreterValue::BigInt(b1), InterpreterValue::BigInt(b2)) => b1 == b2,
            (InterpreterValue::Undefined, InterpreterValue::Undefined)
            | (InterpreterValue::Null, InterpreterValue::Null) => true,
            (InterpreterValue::String(s1), InterpreterValue::String(s2)) => s1 == s2,
            (InterpreterValue::Boolean(b1), InterpreterValue::Boolean(b2)) => b1 == b2,
            (InterpreterValue::Object(x), InterpreterValue::Object(y)) => x.id == y.id,
            _ => unreachable!(),
        }
    }

    pub fn is_strictly_equal(&self, other: &Self) -> bool {
        match (self, other) {
            (InterpreterValue::Numeric(f1), InterpreterValue::Numeric(f2)) => f1 == f2,
            (InterpreterValue::Undefined, InterpreterValue::Undefined)
            | (InterpreterValue::Null, InterpreterValue::Null)
            | (InterpreterValue::String(_), InterpreterValue::String(_))
            | (InterpreterValue::BigInt(_), InterpreterValue::BigInt(_))
            | (InterpreterValue::Object(_), InterpreterValue::Object(_)) => {
                self.same_value_non_number(other)
            }
            _ => false,
        }
    }

    pub fn from_ast_value(
        layer: &Rc<InterpreterLayer>,
        ast_value: AstValue,
    ) -> Result<Self, InterpreterError> {
        match ast_value {
            AstValue::Undefined => Ok(Self::Undefined),
            AstValue::Null => Ok(Self::Null),
            AstValue::Boolean(b) => Ok(Self::Boolean(b)),
            AstValue::String(s) => Ok(Self::String(s)),
            AstValue::Numeric(n) => Ok(Self::Numeric(n)),
            AstValue::BigInt(b) => Ok(Self::BigInt(b)),
            AstValue::Object(o) => {
                let mut store = IndexMap::new();

                for object_literal in o.items.into_iter() {
                    match object_literal {
                        crate::ast::ObjectLiteralItem::Identifier(ident) => {
                            let property_name_value = PropertyNameValue::Identifier(ident.clone());
                            let value = layer
                                .fetch_variable(&property_name_value)
                                .ok_or_else(|| {
                                    InterpreterError::UndefinedIdentifierReference(
                                        property_name_value.clone(),
                                    )
                                })?
                                .value()
                                .clone()
                                .into_value();

                            store.insert(property_name_value, WrappedValue::new(value));
                        }
                        crate::ast::ObjectLiteralItem::Assignment(ls, rs) => {
                            let property_name_value = PropertyNameValue::String(
                                layer
                                    .interpret_primary_expression(&ls)?
                                    .into_value()
                                    .to_string(layer)?,
                            );
                            let value = layer.interpret_assignment_expression(&rs)?.into_value();

                            store.insert(property_name_value, WrappedValue::new(value));
                        }
                        crate::ast::ObjectLiteralItem::ComputedPropertyNameAssignment(ls, rs) => {
                            let property_name_value = PropertyNameValue::String(
                                layer
                                    .interpret_assignment_expression(&ls)?
                                    .into_value()
                                    .to_string(layer)?,
                            );
                            let value = layer.interpret_assignment_expression(&rs)?.into_value();

                            store.insert(property_name_value, WrappedValue::new(value));
                        }
                    }
                }

                Ok(Self::Object(Object::new_with_store(None, store)))
            }
            AstValue::Function(f) => Ok(Self::Object(Object::new_fn(
                None,
                None,
                f,
                Some(layer.clone()),
            ))),
            AstValue::Array(vals) => {
                let mut store = IndexMap::new();

                for (idx, value) in vals.items.into_iter().enumerate() {
                    let property_name_value = PropertyNameValue::String(Rc::new(idx.to_string()));
                    let value = layer
                        .interpret_assignment_expression(value.as_ref())?
                        .into_value();

                    store.insert(property_name_value, WrappedValue::new(value));
                }

                Ok(Self::Object(Object::new_array(None, store)))
            }
        }
    }

    pub fn as_bool(&self) -> bool {
        match self {
            InterpreterValue::Undefined => false,
            InterpreterValue::Null => false,
            InterpreterValue::Boolean(b) => *b,
            InterpreterValue::String(s) => s.is_empty(),
            InterpreterValue::Numeric(val) => !val.is_zero(),
            InterpreterValue::BigInt(val) => !val.is_zero(),
            InterpreterValue::Object(_) => true,
        }
    }
}

impl InterpreterValue {
    pub fn type_as_str(&self) -> &'static str {
        match self {
            InterpreterValue::Undefined => AstValue::UNDEFINED_STR,
            InterpreterValue::Null => AstValue::NULL_STR,
            InterpreterValue::Boolean(_) => AstValue::BOOLEAN_STR,
            InterpreterValue::String(_) => AstValue::STRING_STR,
            InterpreterValue::Numeric(_) => AstValue::NUMERIC_STR,
            InterpreterValue::BigInt(_) => AstValue::BIGINT_STR,
            InterpreterValue::Object(o) if o.get_function().is_some() => AstValue::FUNCTION_STR,
            InterpreterValue::Object(_) => AstValue::OBJECT_STR,
        }
    }

    pub fn downcast_to_function(self) -> Result<Callable, InterpreterError> {
        match self {
            InterpreterValue::Object(o) if o.get_function().is_some() => o.into_function().ok_or(
                InterpreterError::Type(AstValue::FUNCTION_STR, AstValue::OBJECT_STR),
            ),
            v => Err(InterpreterError::Type(
                AstValue::FUNCTION_STR,
                v.type_as_str(),
            )),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum VariableType {
    Var,
    Let,
    Const,
    FuncDec,
    SilentlyIgnoreWrites,
}

#[derive(Debug, Clone)]
pub struct WrappedValue(Rc<RefCell<InterpreterValue>>);

impl WrappedValue {
    pub fn new(value: InterpreterValue) -> Self {
        Self(Rc::new(RefCell::new(value)))
    }

    pub fn into_value(self) -> InterpreterValue {
        match Rc::try_unwrap(self.0) {
            Ok(l) => l.into_inner(),
            Err(rc) => RefCell::clone(&rc).into_inner(),
        }
    }

    pub fn set(&self, value: InterpreterValue) {
        *self.0.borrow_mut() = value;
    }

    pub fn is_object(&self) -> bool {
        self.0.borrow().is_object()
    }
}

#[derive(Debug, Clone)]
pub struct Variable {
    value: WrappedValue,
    variable_type: VariableType,
}

impl Variable {
    pub fn new(value: InterpreterValue, variable_type: VariableType) -> Self {
        Self {
            value: WrappedValue::new(value),
            variable_type,
        }
    }

    pub fn variable_type(&self) -> VariableType {
        self.variable_type
    }

    pub fn value(&self) -> &WrappedValue {
        &self.value
    }

    pub fn set(&self, value: InterpreterValue) -> Result<(), InterpreterError> {
        match self.variable_type {
            VariableType::Const => Err(InterpreterError::AssignToConstant),
            _ => {
                self.value.set(value);
                Ok(())
            }
        }
    }
}

#[derive(Debug, Default)]
pub struct VariableStore {
    store: RefCell<IndexMap<PropertyNameValue, Variable>>,
}

impl VariableStore {
    pub fn new(initial: Option<IndexMap<PropertyNameValue, Variable>>) -> Self {
        Self {
            store: RefCell::new(initial.unwrap_or_default()),
        }
    }

    pub fn set(
        &self,
        index: PropertyNameValue,
        variable: Variable,
    ) -> Result<(), InterpreterError> {
        let index = index.normalise().into_owned();
        if let Some(prev) = self.store.borrow_mut().insert(index.clone(), variable) {
            match prev.variable_type() {
                VariableType::Let => {
                    return Err(InterpreterError::RedeclerationOfLet(match index {
                        PropertyNameValue::Identifier(i) => i,
                        v => unreachable!("RedeclerationOfLet with {:?}", v),
                    }))
                }
                VariableType::Const => {
                    return Err(InterpreterError::RedeclerationOfConst(match index {
                        PropertyNameValue::Identifier(i) => i,
                        v => unreachable!("RedeclerationOfConst with {:?}", v),
                    }))
                }
                _ => {}
            }
        }

        Ok(())
    }

    pub fn get(&self, index: &PropertyNameValue) -> Option<Variable> {
        let index = index.normalise();
        self.store.borrow().get(index.as_ref()).map(Clone::clone)
    }

    pub fn update(
        &self,
        index: &PropertyNameValue,
        value: InterpreterValue,
    ) -> Result<(), InterpreterError> {
        let index = index.normalise();

        if let Some(prev) = self.store.borrow_mut().get_mut(index.as_ref()) {
            prev.set(value)?;
        }

        Err(InterpreterError::UndefinedIdentifierReference(
            index.into_owned(),
        ))
    }

    pub fn exists(&self, index: &PropertyNameValue) -> bool {
        let index = index.normalise();
        self.store.borrow().contains_key(index.as_ref())
    }
}

#[derive(Debug, Clone)]
pub enum Ref {
    PropRef(Object, PropertyNameValue),
    ValRef(WrappedValue),
}

impl Ref {
    pub fn into_value(self) -> InterpreterValue {
        match self {
            Ref::PropRef(o, n) => o.get(&n).map(|x| x.into_value()).unwrap_or_default(),
            Ref::ValRef(v) => v.into_value(),
        }
    }

    pub fn set(&self, value: InterpreterValue) {
        match self {
            Ref::PropRef(o, n) => o.set(n.clone(), value),
            Ref::ValRef(r) => r.set(value),
        }
    }

    pub fn is_object(&self) -> bool {
        match self {
            Ref::PropRef(o, n) => o.get(n).map(|x| x.is_object()).unwrap_or_default(),
            Ref::ValRef(r) => r.is_object(),
        }
    }
}

#[derive(Debug, Clone)]
pub enum ValueOrRef {
    Value(InterpreterValue),
    Ref(Ref),
}

impl ValueOrRef {
    pub fn into_value(self) -> InterpreterValue {
        match self {
            ValueOrRef::Value(v) => v,
            ValueOrRef::Ref(r) => r.into_value(),
        }
    }
}
