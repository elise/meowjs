use std::rc::Rc;

use crate::ast::AssignmentExpression;

use super::{error::InterpreterError, variables::InterpreterValue, InterpreterLayer};

impl<F> RuntimeFuntion for F
where
    F: Fn(
            &[Rc<AssignmentExpression>],
            &Rc<InterpreterLayer>,
        ) -> Result<InterpreterValue, InterpreterError>
        + Send
        + 'static,
{
    fn handle_call(
        &self,
        parameters: &[Rc<AssignmentExpression>],
        layer: &Rc<InterpreterLayer>,
    ) -> Result<InterpreterValue, InterpreterError> {
        self(parameters, layer)
    }
}

pub fn closure_fn<F>(f: F) -> ClosureHandler<F>
where
    F: Fn(
            &[Rc<AssignmentExpression>],
            &Rc<InterpreterLayer>,
        ) -> Result<InterpreterValue, InterpreterError>
        + Send
        + 'static,
{
    ClosureHandler(f)
}

pub struct ClosureHandler<F>(pub(super) F)
where
    F: Fn(
            &[Rc<AssignmentExpression>],
            &Rc<InterpreterLayer>,
        ) -> Result<InterpreterValue, InterpreterError>
        + Send
        + 'static;

impl<F> RuntimeFuntion for ClosureHandler<F>
where
    F: Fn(
            &[Rc<AssignmentExpression>],
            &Rc<InterpreterLayer>,
        ) -> Result<InterpreterValue, InterpreterError>
        + Send
        + 'static,
{
    fn handle_call(
        &self,
        parameters: &[Rc<AssignmentExpression>],
        layer: &Rc<InterpreterLayer>,
    ) -> Result<InterpreterValue, InterpreterError> {
        self.0(parameters, layer)
    }
}

pub trait RuntimeFuntion: Send + 'static {
    fn handle_call(
        &self,
        parameters: &[Rc<AssignmentExpression>],
        layer: &Rc<InterpreterLayer>,
    ) -> Result<InterpreterValue, InterpreterError>;
}
