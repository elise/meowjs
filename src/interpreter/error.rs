use std::borrow::Cow;

use crate::{ast::PropertyNameValue, tt::Identifier};

use super::parent_objects;

#[derive(Debug, Clone, PartialEq, thiserror::Error)]
pub enum InterpreterError {
    #[error("Identifier reference `{0:?}` is undefined")]
    UndefinedIdentifierReference(PropertyNameValue),
    #[error("Attempted to redeclare `const {0:?}`")]
    RedeclerationOfConst(Identifier),
    #[error("Attempted to assign to a const")]
    AssignToConstant,
    #[error("Attempted to redeclare `let {0:?}`")]
    RedeclerationOfLet(Identifier),
    #[error("Type Error: expected {0}, found {1}")]
    Type(&'static str, &'static str),
    #[error("Type Error: {0}")]
    TypeMsg(Cow<'static, str>),
    #[error("Inalid left side for assignment")]
    InvalidLeftSideAssignment,
    #[error("Attempted bitwise shift with too large right number")]
    ShiftWithTooLargeRightSide,
    #[error("Range Error: {0}")]
    Range(&'static str),
    #[error("Attempted to assign property on invalid type")]
    AssignPropertyOnInvalidType,
    #[error("Error whilst generating parent object: {0}")]
    Parent(#[from] parent_objects::ParentError),
    #[error("SyntaxError: Invalid update expression operand")]
    InvalidUpdateOperand,
}
