use std::path::PathBuf;

use clap::Parser;

#[derive(Debug, Parser)]
#[clap(author, version, about, long_about = None)]
pub struct Args {
    pub file: PathBuf,
}

impl Args {
    pub fn read() -> Self {
        Args::parse()
    }
}
