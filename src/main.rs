pub mod args;
pub mod ast;
pub mod interpreter;
pub mod parser;
pub mod tt;
pub mod util;

use std::process::ExitCode;
use std::rc::Rc;

use crate::ast::{NodeParse, NodeTree};
use crate::{
    parser::{Parse, Parser, ParserError},
    tt::TokenTree,
};
use interpreter::Interpreter;
use tracing::metadata::LevelFilter;
use tracing_subscriber::{prelude::__tracing_subscriber_SubscriberExt, util::SubscriberInitExt};

#[derive(Debug, thiserror::Error)]
pub enum RuntimeError {
    #[error(transparent)]
    Parser(#[from] ParserError),
    #[error(transparent)]
    Node(#[from] crate::ast::NodeParseErrorWithSpan),
    #[error(transparent)]
    Interpreter(#[from] interpreter::error::InterpreterError),
    #[error("Failed to setup logging: {0}")]
    Tracing(#[from] tracing_subscriber::util::TryInitError),
    #[error(transparent)]
    Io(#[from] std::io::Error),
}

fn actual_main() -> Result<(), RuntimeError> {
    tracing_subscriber::registry()
        .with(tracing_subscriber::fmt::layer())
        .with(
            tracing_subscriber::EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        .try_init()?;

    let args = args::Args::read();

    let file_content = std::fs::read_to_string(args.file.as_path())?;

    let mut parser = Parser::new(Some(Rc::new(args.file)), file_content);
    let mut token_tree = TokenTree::parse(&mut parser)?.unwrap();

    tracing::debug!("Token tree: {:?}", token_tree);

    let ast = NodeTree::node_parse(&mut token_tree)?;

    tracing::debug!("AST: {:?}", ast);

    let interpreter = Interpreter::new(Rc::new(ast));

    interpreter.run()?;

    Ok(())
}

fn main() -> ExitCode {
    if let Err(e) = actual_main() {
        eprintln!("{}", e);
        ExitCode::FAILURE
    } else {
        ExitCode::SUCCESS
    }
}
