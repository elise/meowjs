use std::{
    borrow::Cow,
    fmt::{Debug, Display},
    path::PathBuf,
    rc::Rc,
};

use num::{bigint::ParseBigIntError, traits::ParseFloatError, BigInt, Num};

use crate::{
    tt::{
        Identifier, Keyword, Literal, PrivateIdentifier, Punctuator, Span, Token, TokenTree,
        TokenWithSpan,
    },
    util::{parse_escaped_string, ParseEscapedStringError},
};

#[derive(Debug)]
pub enum ParserErrorVariant {
    MissingSemicolon,
    InvalidSource,
    StringLiteralMissingClosing,
    MultiLineCommentMissingClosing,
    PrivateIdentifierPrefixMissingIdentifier,
    BigIntParse(u32, String, ParseBigIntError),
    FloatParse(u32, String, ParseFloatError),
    NumericLiteralPrefixMissingNumbers,
    NumericLiteralExponentMissingRs,
    NumericLiteralTrailingUnderscore,
    NumericLiteralMultipleParallelUnderscores,

    EscapedString(ParseEscapedStringError),

    RegexLiteralUnsupported,
    TempalteStringLiteralUnsupported,
}

impl Clone for ParserErrorVariant {
    fn clone(&self) -> Self {
        match self {
            Self::MissingSemicolon => Self::MissingSemicolon,
            Self::InvalidSource => Self::InvalidSource,
            Self::StringLiteralMissingClosing => Self::StringLiteralMissingClosing,
            Self::MultiLineCommentMissingClosing => Self::MultiLineCommentMissingClosing,
            Self::PrivateIdentifierPrefixMissingIdentifier => {
                Self::PrivateIdentifierPrefixMissingIdentifier
            }
            Self::BigIntParse(arg0, arg1, arg2) => {
                Self::BigIntParse(*arg0, arg1.clone(), arg2.clone())
            }
            Self::FloatParse(arg0, arg1, arg2) => Self::FloatParse(
                *arg0,
                arg1.clone(),
                ParseFloatError {
                    kind: match arg2.kind {
                        num::traits::FloatErrorKind::Empty => num::traits::FloatErrorKind::Empty,
                        num::traits::FloatErrorKind::Invalid => {
                            num::traits::FloatErrorKind::Invalid
                        }
                    },
                },
            ),
            Self::NumericLiteralPrefixMissingNumbers => Self::NumericLiteralPrefixMissingNumbers,
            Self::NumericLiteralExponentMissingRs => Self::NumericLiteralExponentMissingRs,
            Self::NumericLiteralTrailingUnderscore => Self::NumericLiteralTrailingUnderscore,
            Self::NumericLiteralMultipleParallelUnderscores => {
                Self::NumericLiteralMultipleParallelUnderscores
            }
            Self::RegexLiteralUnsupported => Self::RegexLiteralUnsupported,
            Self::TempalteStringLiteralUnsupported => Self::TempalteStringLiteralUnsupported,
            Self::EscapedString(s) => Self::EscapedString(*s),
        }
    }
}

impl PartialEq for ParserErrorVariant {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::BigIntParse(l0, l1, l2), Self::BigIntParse(r0, r1, r2)) => {
                l0 == r0 && l1 == r1 && l2 == r2
            }
            (Self::FloatParse(l0, l1, l2), Self::FloatParse(r0, r1, r2)) => {
                l0 == r0
                    && l1 == r1
                    && match l2.kind {
                        num::traits::FloatErrorKind::Empty => match r2.kind {
                            num::traits::FloatErrorKind::Empty => true,
                            num::traits::FloatErrorKind::Invalid => false,
                        },
                        num::traits::FloatErrorKind::Invalid => match r2.kind {
                            num::traits::FloatErrorKind::Empty => false,
                            num::traits::FloatErrorKind::Invalid => true,
                        },
                    }
            }
            _ => core::mem::discriminant(self) == core::mem::discriminant(other),
        }
    }
}

impl Eq for ParserErrorVariant {}

impl ParserErrorVariant {
    pub fn msg(&self) -> Cow<'static, str> {
        match self {
            ParserErrorVariant::MissingSemicolon => Cow::Borrowed("Missing semicolon"),
            ParserErrorVariant::InvalidSource => Cow::Borrowed("Failed to continue parsing..."),
            ParserErrorVariant::StringLiteralMissingClosing => {
                Cow::Borrowed("String literal missing closing")
            }
            ParserErrorVariant::MultiLineCommentMissingClosing => {
                Cow::Borrowed("Multi-Line comment missing closing")
            }
            ParserErrorVariant::PrivateIdentifierPrefixMissingIdentifier => {
                Cow::Borrowed("Private identifier prefix found without identifier following")
            }
            ParserErrorVariant::BigIntParse(base, src, err) => Cow::Owned(format!(
                "internal BigInt parse error (base: {}, src: {:?}): {}",
                base, src, err
            )),
            ParserErrorVariant::FloatParse(base, src, err) => Cow::Owned(format!(
                "internal f64 parse error (base: {}, src: {:?}): {}",
                base, src, err
            )),
            ParserErrorVariant::NumericLiteralPrefixMissingNumbers => {
                Cow::Borrowed("Numeric literal prefix found without numbers following")
            }
            ParserErrorVariant::NumericLiteralExponentMissingRs => {
                Cow::Borrowed("Numeric literal missing right side of exponent")
            }
            ParserErrorVariant::NumericLiteralTrailingUnderscore => {
                Cow::Borrowed("Numeric literal cannot end with an underscore")
            }
            ParserErrorVariant::NumericLiteralMultipleParallelUnderscores => {
                Cow::Borrowed("Numeric literal cannot contain multiple underscores in a row")
            }
            ParserErrorVariant::RegexLiteralUnsupported => {
                Cow::Borrowed("Regex literals are unsupported")
            }
            ParserErrorVariant::TempalteStringLiteralUnsupported => {
                Cow::Borrowed("Template string literals are unsupported")
            }
            ParserErrorVariant::EscapedString(err) => match err {
                ParseEscapedStringError::InvalidEscape(s) => {
                    Cow::Owned(format!("Invalid escape character: {}", s))
                }
                ParseEscapedStringError::MissingSequence => {
                    Cow::Borrowed("Missing escape sequence")
                }
            },
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ParserError {
    pub line: usize,
    pub col: usize,
    pub variant: ParserErrorVariant,
    pub message: Cow<'static, str>,
}

impl ParserError {
    pub fn new(line: usize, col: usize, variant: ParserErrorVariant) -> Self {
        let message = variant.msg();
        Self {
            line,
            col,
            variant,
            message,
        }
    }
}

impl Display for ParserError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!(
            "Syntax error @ line {} col {}: {}",
            self.line, self.col, self.message
        ))
    }
}

impl std::error::Error for ParserError {}

pub struct Parser {
    pub file_path: Option<Rc<PathBuf>>,
    pub source: Rc<String>,
    pub offset: usize,
    pub peek_offset: usize,
}

impl Parser {
    pub fn new<T: Into<String>>(file_path: Option<Rc<PathBuf>>, source: T) -> Self {
        Self {
            file_path,
            source: Rc::new(source.into()),
            offset: 0,
            peek_offset: 0,
        }
    }

    const NEWLINE_ALLOWED_TOKENS: &'static [Token] = &[
        Token::Punctuator(Punctuator::Semicolon),
        Token::Punctuator(Punctuator::LeftBrace),
        Token::Punctuator(Punctuator::RightBrace),
        Token::Punctuator(Punctuator::LeftBracket),
        Token::Punctuator(Punctuator::RightBracket),
        Token::Keyword(Keyword::Function),
        Token::Keyword(Keyword::Class),
        Token::Punctuator(Punctuator::Comma),
    ];

    const NEWLINE_ALLOWED_IDENTIFIER_PREFIX_TOKENS: &'static [Token] = &[
        Token::Punctuator(Punctuator::LeftBracket),
        Token::Punctuator(Punctuator::RightBracket),
        Token::Punctuator(Punctuator::Comma),
        Token::Keyword(Keyword::Function),
        Token::Keyword(Keyword::Class),
    ];

    pub fn next_char(&mut self) -> Option<char> {
        let res = self.source.chars().nth(self.offset)?;
        self.offset += 1;
        self.peek_offset = 0;
        Some(res)
    }

    pub fn back_count(&self, c: char) -> usize {
        let mut count = 0;
        while self
            .source
            .chars()
            .nth(self.offset + self.peek_offset - count)
            == Some(c)
        {
            count += 1
        }
        count
    }

    pub fn peek_count(&mut self, count: usize) -> Option<&str> {
        let offset = self.offset + self.peek_offset;
        let res = &self.source.as_ref()[offset..offset + count];
        self.peek_offset += count;
        Some(res)
    }

    pub fn peek_forward_str_matches_followed_by_non_alphanumeric(&mut self, s: &str) -> bool {
        let mut count = 0;

        for c in s.chars() {
            if self
                .source
                .chars()
                .nth(self.offset + self.peek_offset + count)
                .map(|x| x == c)
                .unwrap_or_default()
            {
                count += 1;
            } else {
                return false;
            }
        }

        if self
            .source
            .chars()
            .nth(self.offset + self.peek_offset + count)
            .map(char::is_alphanumeric)
            .unwrap_or_default()
        {
            return false;
        }

        self.peek_offset += count;

        true
    }

    pub fn peek_previous(&self) -> Option<char> {
        self.source.chars().nth(self.offset + self.peek_offset - 1)
    }

    pub fn peek_current_char(&self) -> Option<char> {
        self.source.chars().nth(self.offset + self.peek_offset)
    }

    pub fn peek_next_char(&mut self) -> Option<char> {
        let res = self.source.chars().nth(self.offset + self.peek_offset)?;
        self.peek_offset += 1;
        Some(res)
    }

    pub fn try_peek_next(&mut self, c: char) -> bool {
        let res = self.source.chars().nth(self.offset + self.peek_offset) == Some(c);
        if res {
            self.peek_offset += 1;
        }
        res
    }

    pub fn reset_peek(&mut self) {
        self.peek_offset = 0;
    }

    pub fn unpeek(&mut self, count: usize) {
        self.peek_offset -= count;
    }

    pub fn commit_peek(&mut self) {
        self.offset += self.peek_offset;
        self.peek_offset = 0;
    }

    pub fn nom_whitespace(&mut self, allow_newlines: bool) {
        while self.try_peek_next(' ')
            || self.try_peek_next('\t')
            || (allow_newlines && (self.try_peek_next('\r') || self.try_peek_next('\n')))
        {}
        self.commit_peek();
    }

    pub fn peek_until(
        &mut self,
        value: char,
        ensure_unescaped: bool,
        allow_newlines: bool,
    ) -> Option<&str> {
        let start_offset = self.offset + self.peek_offset;

        let n = self.peek_next_char()?;
        if n == value {
            return Some("");
        }

        let mut count = 1;
        while let Some(n) = self.peek_next_char() {
            match n == value {
                true if !ensure_unescaped || self.back_count('\\') & 1 == 0 => break,
                false if !allow_newlines && (n == '\r' || n == '\n') => return None,
                _ => count += 1,
            }
        }
        Some(&self.source.as_ref()[start_offset..start_offset + count])
    }

    pub fn expect_newline(&mut self) -> bool {
        self.nom_whitespace(false);
        self.try_peek_next(';')
    }

    pub fn dbgln<T: Display>(&self, msg: T) {
        if cfg!(debug_assertions) {
            let mut line = 1;
            let mut chars_before_line_start = 0;
            let mut current_line_chars = 0;
            for (iter, c) in self.source.chars().enumerate() {
                if iter >= self.offset {
                    break;
                }
                current_line_chars += 1;
                if c == '\n' {
                    line += 1;
                    chars_before_line_start += current_line_chars;
                    current_line_chars = 0;
                }
            }
            tracing::debug!(
                "[Line {} Col {}]: {}",
                line,
                self.offset + 1 - chars_before_line_start,
                msg
            );
        }
    }

    #[must_use]
    pub fn raise_error(&self, error: ParserErrorVariant) -> ParserError {
        let mut line = 1;
        let mut chars_before_line_start = 0;
        let mut current_line_chars = 0;
        for (iter, c) in self.source.chars().enumerate() {
            if iter >= self.offset {
                break;
            }
            current_line_chars += 1;
            if c == '\n' {
                line += 1;
                chars_before_line_start += current_line_chars;
                current_line_chars = 0;
            }
        }

        ParserError::new(line, self.offset + 1 - chars_before_line_start, error)
    }
}

pub trait Parse: Sized {
    fn parse(parser: &mut Parser) -> Result<Option<Self>, ParserError>;
}

impl Parse for Punctuator {
    fn parse(parser: &mut Parser) -> Result<Option<Self>, ParserError> {
        parser.nom_whitespace(false);
        let c = match parser.peek_next_char() {
            Some(c) => c,
            None => return Ok(None),
        };

        let p = match c {
            '{' => Some(Self::LeftBrace),
            '}' => Some(Self::RightBrace),
            '(' => Some(Self::LeftBracket),
            ')' => Some(Self::RightBracket),
            '[' => Some(Self::LeftSquareBracket),
            ']' => Some(Self::RightSquareBracket),
            '.' => match parser.try_peek_next('.') && parser.try_peek_next('.') {
                true => Some(Self::Spread),
                false => Some(Self::Dot),
            },
            ';' => Some(Self::Semicolon),
            ',' => Some(Self::Comma),
            '<' => {
                if parser.try_peek_next('<') {
                    match parser.try_peek_next('=') {
                        true => Some(Self::LeftShiftAssignment),
                        false => Some(Self::LeftShift),
                    }
                } else if parser.try_peek_next('=') {
                    Some(Self::LessThanEqual)
                } else {
                    Some(Self::LessThan)
                }
            }
            '>' => {
                if parser.try_peek_next('>') {
                    if parser.try_peek_next('>') {
                        match parser.try_peek_next('=') {
                            true => Some(Self::UnsignedRightShiftAssignment),
                            false => Some(Self::UnsignedRightShift),
                        }
                    } else {
                        match parser.try_peek_next('=') {
                            true => Some(Self::RightShiftAssignment),
                            false => Some(Self::RightShift),
                        }
                    }
                } else if parser.try_peek_next('=') {
                    Some(Self::GreaterThanEqual)
                } else {
                    Some(Self::GreaterThan)
                }
            }
            '=' => {
                if parser.try_peek_next('=') {
                    if parser.try_peek_next('=') {
                        Some(Self::StrictEquality)
                    } else {
                        Some(Self::Equality)
                    }
                } else if parser.try_peek_next('>') {
                    Some(Self::Arrow)
                } else {
                    Some(Self::Assignment)
                }
            }
            '!' => {
                if parser.try_peek_next('=') {
                    if parser.try_peek_next('=') {
                        Some(Self::StrictInequality)
                    } else {
                        Some(Self::Inequality)
                    }
                } else {
                    Some(Self::LogicalNot)
                }
            }
            '+' => {
                if parser.try_peek_next('+') {
                    Some(Self::Increment)
                } else if parser.try_peek_next('=') {
                    Some(Self::AdditionAssignment)
                } else {
                    Some(Self::Addition)
                }
            }
            '-' => {
                if parser.try_peek_next('-') {
                    Some(Self::Decrement)
                } else if parser.try_peek_next('=') {
                    Some(Self::SubtractionAssignment)
                } else {
                    Some(Self::Subtraction)
                }
            }
            '*' => {
                if parser.try_peek_next('*') {
                    if parser.try_peek_next('=') {
                        Some(Self::ExponentiationAssignment)
                    } else {
                        Some(Self::Exponentiation)
                    }
                } else if parser.try_peek_next('=') {
                    Some(Self::MultiplicationAssignment)
                } else {
                    Some(Self::Multiplication)
                }
            }
            '%' => match parser.try_peek_next('=') {
                true => Some(Self::RemainderAssignment),
                false => Some(Self::Remainder),
            },
            '&' => {
                if parser.try_peek_next('&') {
                    if parser.try_peek_next('=') {
                        Some(Self::LogicalAndAssignment)
                    } else {
                        Some(Self::LogicalAnd)
                    }
                } else if parser.try_peek_next('=') {
                    Some(Self::BitwiseAndAssignment)
                } else {
                    Some(Self::BitwiseAnd)
                }
            }
            '|' => {
                if parser.try_peek_next('|') {
                    if parser.try_peek_next('=') {
                        Some(Self::LogicalOrAssignment)
                    } else {
                        Some(Self::LogicalOr)
                    }
                } else if parser.try_peek_next('=') {
                    Some(Self::BitwiseOrAssignment)
                } else {
                    Some(Self::BitwiseOr)
                }
            }
            '^' => match parser.try_peek_next('=') {
                true => Some(Self::BitwiseXorAssignment),
                false => Some(Self::BitwiseXor),
            },
            '~' => Some(Self::BitwiseNot),
            '?' => {
                if parser.try_peek_next('?') {
                    if parser.try_peek_next('=') {
                        Some(Self::NullishCoalescingAssignment)
                    } else {
                        Some(Self::NullishCoalescing)
                    }
                } else if parser.try_peek_next('.') {
                    Some(Self::OptionalChaining)
                } else {
                    None
                }
            }
            ':' => Some(Self::Colon),
            '/' => match parser.try_peek_next('=') {
                true => Some(Self::DivisionAssignment),
                false => Some(Self::Division),
            },
            _ => None,
        };

        if p.is_some() {
            parser.commit_peek();
        } else {
            parser.reset_peek();
        }

        Ok(p)
    }
}

impl Parse for Literal {
    fn parse(parser: &mut Parser) -> Result<Option<Self>, ParserError> {
        parser.nom_whitespace(false);
        let c = match parser.peek_next_char() {
            Some(c) => c,
            None => return Ok(None),
        };

        let lit = match c {
            '\'' | '"' => {
                let string_literal = match parser.peek_until(c, true, true) {
                    Some(string_literal) => {
                        parse_escaped_string(string_literal).map_err(|why| {
                            parser.raise_error(ParserErrorVariant::EscapedString(why))
                        })?
                    }
                    None => {
                        return Err(
                            parser.raise_error(ParserErrorVariant::StringLiteralMissingClosing)
                        )
                    }
                };
                Some(Self::String(string_literal))
            }
            'u' => match parser.peek_forward_str_matches_followed_by_non_alphanumeric("ndefined") {
                true => Some(Self::Null),
                false => None,
            },
            'n' => match parser.peek_forward_str_matches_followed_by_non_alphanumeric("ull") {
                true => Some(Self::Null),
                false => None,
            },
            'f' => match parser.peek_forward_str_matches_followed_by_non_alphanumeric("alse") {
                true => Some(Self::Boolean(false)),
                false => None,
            },
            't' => match parser.peek_forward_str_matches_followed_by_non_alphanumeric("rue") {
                true => Some(Self::Boolean(true)),
                false => None,
            },
            '.' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | '0' => {
                let mut count = 1;
                let mut dot_occured = c == '.';
                let mut exponent_offset = None;

                let hex_literal =
                    c == '0' && (parser.try_peek_next('x') || parser.try_peek_next('X'));
                let binary_literal = !hex_literal
                    && c == '0'
                    && (parser.try_peek_next('b') || parser.try_peek_next('B'));
                let octal_literal = !binary_literal
                    && !hex_literal
                    && c == '0'
                    && (parser.try_peek_next('o') || parser.try_peek_next('O'));

                if hex_literal || binary_literal || octal_literal {
                    count += match c == '0' {
                        true => 1,
                        false => 2,
                    };
                }

                let mut prev_underscore = false;

                while parser.try_peek_next('_')
                    || parser.try_peek_next('0')
                    || parser.try_peek_next('1')
                    || (!binary_literal
                        && (parser.try_peek_next('2')
                            || parser.try_peek_next('3')
                            || parser.try_peek_next('4')
                            || parser.try_peek_next('5')
                            || parser.try_peek_next('6')
                            || parser.try_peek_next('7')
                            || parser.try_peek_next('8')
                            || parser.try_peek_next('9')
                            || (exponent_offset.is_none()
                                && hex_literal
                                && (parser.try_peek_next('a')
                                    || parser.try_peek_next('b')
                                    || parser.try_peek_next('c')
                                    || parser.try_peek_next('d')
                                    || parser.try_peek_next('e')
                                    || parser.try_peek_next('f')
                                    || parser.try_peek_next('A')
                                    || parser.try_peek_next('B')
                                    || parser.try_peek_next('C')
                                    || parser.try_peek_next('D')
                                    || parser.try_peek_next('E')
                                    || parser.try_peek_next('F')))
                            || (!dot_occured
                                && !hex_literal
                                && !octal_literal
                                && exponent_offset.is_none()
                                && parser.try_peek_next('.'))
                            || (exponent_offset.is_none()
                                && !hex_literal
                                && !octal_literal
                                && (parser.try_peek_next('e') || parser.try_peek_next('E')))
                            || (exponent_offset == Some(count - 1)
                                && (parser.try_peek_next('-') || parser.try_peek_next('+')))))
                {
                    let prev = parser.peek_previous().expect("peek_previous returned None");

                    if prev == '.' {
                        dot_occured = true;
                    } else if exponent_offset.is_none()
                        && !hex_literal
                        && !octal_literal
                        && (prev == 'e' || prev == 'E')
                    {
                        exponent_offset = Some(count);
                    }

                    if prev == '_' {
                        if prev_underscore {
                            return Err(parser.raise_error(
                                ParserErrorVariant::NumericLiteralMultipleParallelUnderscores,
                            ));
                        }
                        prev_underscore = true
                    } else {
                        prev_underscore = false;
                    }

                    count += 1;
                }

                if count == 1 && (dot_occured || exponent_offset.is_some()) {
                    parser.reset_peek();
                    return Ok(None);
                }

                if parser.peek_previous().expect("peek_previous returned None") == '_' {
                    return Err(
                        parser.raise_error(ParserErrorVariant::NumericLiteralTrailingUnderscore)
                    );
                }

                parser.unpeek(count);
                let nstr = parser.peek_count(count).expect("peek_count returned None");

                if let Some(exponent_offset) = exponent_offset {
                    let exponent_ls = &nstr[..exponent_offset];
                    let exponent_rs = &nstr[exponent_offset + 1..];

                    if exponent_rs.is_empty() {
                        return Err(
                            parser.raise_error(ParserErrorVariant::NumericLiteralExponentMissingRs)
                        );
                    }

                    let exponent_ls = match f64::from_str_radix(exponent_ls, 10) {
                        Ok(f) => f,
                        Err(e) => {
                            let ex = exponent_ls.to_string();
                            return Err(
                                parser.raise_error(ParserErrorVariant::FloatParse(10, ex, e))
                            );
                        }
                    };

                    let exponent_rs = match f64::from_str_radix(exponent_rs, 10) {
                        Ok(f) => f,
                        Err(e) => {
                            let ex = exponent_rs.to_string();
                            return Err(
                                parser.raise_error(ParserErrorVariant::FloatParse(10, ex, e))
                            );
                        }
                    };

                    Some(Self::Numeric(
                        exponent_ls * (10f64.powf(exponent_rs) as f64),
                    ))
                } else {
                    let radix = if binary_literal {
                        2
                    } else if hex_literal {
                        16
                    } else if octal_literal {
                        8
                    } else {
                        10
                    };

                    let raw_num_offset = match radix == 10 {
                        true => 0,
                        false => 2,
                    };

                    if radix != 10 && nstr.len() == raw_num_offset {
                        return Err(parser
                            .raise_error(ParserErrorVariant::NumericLiteralPrefixMissingNumbers));
                    }

                    let nstr = nstr[raw_num_offset..].to_string();

                    assert!(exponent_offset.is_none());
                    let big_int_suffix = parser.try_peek_next('n');

                    if big_int_suffix {
                        Some(Self::BigInt(
                            BigInt::from_str_radix(nstr.as_str(), radix).map_err(|e| {
                                parser.raise_error(ParserErrorVariant::BigIntParse(radix, nstr, e))
                            })?,
                        ))
                    } else {
                        Some(Self::Numeric(
                            f64::from_str_radix(nstr.as_str(), radix).map_err(|e| {
                                parser.raise_error(ParserErrorVariant::FloatParse(radix, nstr, e))
                            })?,
                        ))
                    }
                }
            }
            //'/' => return Err(parser.raise_error(ParserErrorVariant::RegexLiteralUnsupported)),
            '`' => {
                return Err(parser.raise_error(ParserErrorVariant::TempalteStringLiteralUnsupported))
            }
            _ => None,
        };

        if lit.is_some() {
            parser.commit_peek()
        } else {
            parser.reset_peek()
        }

        Ok(lit)
    }
}

// TODO: Unicode escape sequences
impl Parse for PrivateIdentifier {
    fn parse(parser: &mut Parser) -> Result<Option<Self>, ParserError> {
        parser.nom_whitespace(false);
        let c = match parser.peek_next_char() {
            Some(c) => c,
            None => return Ok(None),
        };

        if c == '#' {
            let mut count = 0;

            while let Some(c) = parser.peek_next_char() {
                if PrivateIdentifier::validate_char(count + 1, c) {
                    count += 1;
                } else {
                    parser.unpeek(1);
                    break;
                }
            }

            // Return to first char after prefix
            parser.unpeek(count);

            if count == 0 {
                return Err(parser
                    .raise_error(ParserErrorVariant::PrivateIdentifierPrefixMissingIdentifier));
            }

            let identifier = Rc::new(
                parser
                    .peek_count(count)
                    .expect("peek_count returned None whilst parsing private identifiers")
                    .to_string(),
            );

            parser.commit_peek();

            Ok(Some(Self(identifier)))
        } else {
            parser.reset_peek();
            Ok(None)
        }
    }
}

// TODO: Unicode escape sequences
impl Parse for Identifier {
    fn parse(parser: &mut Parser) -> Result<Option<Self>, ParserError> {
        parser.nom_whitespace(false);
        let mut count = 0;

        while let Some(c) = parser.peek_next_char() {
            if Identifier::validate_char(count, c) {
                count += 1;
            } else {
                parser.unpeek(1);
                break;
            }
        }

        parser.unpeek(count);

        if count == 0 {
            parser.reset_peek();
            return Ok(None);
        }

        let identifier = match parser.peek_count(count) {
            Some(identifier) => Rc::new(identifier.to_string()),
            None => {
                parser.reset_peek();
                return Ok(None);
            }
        };

        parser.commit_peek();

        Ok(Some(Self(identifier)))
    }
}

impl Parse for Keyword {
    fn parse(parser: &mut Parser) -> Result<Option<Self>, ParserError> {
        parser.nom_whitespace(false);
        let res = if parser.peek_forward_str_matches_followed_by_non_alphanumeric("await") {
            Some(Self::Await)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("break") {
            Some(Self::Break)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("case") {
            Some(Self::Case)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("catch") {
            Some(Self::Catch)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("class") {
            Some(Self::Class)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("const") {
            Some(Self::Const)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("continue") {
            Some(Self::Continue)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("debugger") {
            Some(Self::Debugger)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("default") {
            Some(Self::Default)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("delete") {
            Some(Self::Delete)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("do") {
            Some(Self::Do)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("else") {
            Some(Self::Else)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("enum") {
            Some(Self::Enum)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("export") {
            Some(Self::Export)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("extends") {
            Some(Self::Extends)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("false") {
            Some(Self::False)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("finally") {
            Some(Self::Finally)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("for") {
            Some(Self::For)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("function") {
            Some(Self::Function)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("if") {
            Some(Self::If)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("import") {
            Some(Self::Import)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("in") {
            Some(Self::In)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("instanceof") {
            Some(Self::Instanceof)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("new") {
            Some(Self::New)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("return") {
            Some(Self::Return)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("super") {
            Some(Self::Super)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("switch") {
            Some(Self::Switch)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("this") {
            Some(Self::This)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("throw") {
            Some(Self::Throw)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("true") {
            Some(Self::True)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("try") {
            Some(Self::Try)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("typeof") {
            Some(Self::Typeof)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("var") {
            Some(Self::Var)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("void") {
            Some(Self::Void)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("while") {
            Some(Self::While)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("with") {
            Some(Self::With)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("yeild") {
            Some(Self::Yeild)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("let") {
            Some(Self::Let)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("static") {
            Some(Self::Static)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("implements") {
            Some(Self::Implements)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("interface") {
            Some(Self::Interface)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("package") {
            Some(Self::Package)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("private") {
            Some(Self::Private)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("protected") {
            Some(Self::Protected)
        } else if parser.peek_forward_str_matches_followed_by_non_alphanumeric("public") {
            Some(Self::Public)
        } else {
            None
        };

        match res.is_some() {
            true => parser.commit_peek(),
            false => parser.reset_peek(),
        }

        Ok(res)
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Comment {
    SingleLine(String),
    MultiLine(String),
}

impl Parse for Comment {
    fn parse(parser: &mut Parser) -> Result<Option<Self>, ParserError> {
        let c = match parser.peek_next_char() {
            Some(c) => c,
            None => return Ok(None),
        };

        if c == '/' {
            if parser.try_peek_next('/') {
                let mut count = 0;

                while let Some(c) = parser.peek_next_char() {
                    if c == '\n' {
                        parser.unpeek(1);
                        break;
                    }

                    count += 1;
                }

                parser.unpeek(count);
                let cmt = parser
                    .peek_count(count)
                    .expect("peek_count returned none on single-line comment")
                    .trim_end_matches('\r')
                    .to_string();

                let next = parser.peek_next_char();
                if let Some(next) = next {
                    assert_eq!(next, '\n');
                }

                parser.commit_peek();
                return Ok(Some(Self::SingleLine(cmt)));
            } else if parser.try_peek_next('*') {
                let mut count = 0;
                let mut end_found = false;

                while let Some(c) = parser.peek_next_char() {
                    if c == '*' {
                        if let Some(c) = parser.peek_next_char() {
                            if c == '/' {
                                end_found = true;
                                parser.unpeek(2);
                                break;
                            }
                            parser.unpeek(1);
                        }
                    }

                    count += 1;
                }

                if !end_found {
                    return Err(
                        parser.raise_error(ParserErrorVariant::MultiLineCommentMissingClosing)
                    );
                }

                parser.unpeek(count);
                let cmt = parser
                    .peek_count(count)
                    .expect("peek_count returned none on multi-line comment")
                    .trim_end_matches('\r')
                    .to_string();

                let next = parser.peek_next_char();
                assert_eq!(next, Some('*'));
                let next = parser.peek_next_char();
                assert_eq!(next, Some('/'));

                parser.commit_peek();
                return Ok(Some(Self::MultiLine(cmt)));
            }
        }

        parser.reset_peek();
        Ok(None)
    }
}

impl Parse for Token {
    fn parse(parser: &mut Parser) -> Result<Option<Self>, ParserError> {
        let res = if let Some(comment) = Comment::parse(parser)? {
            Some(Self::Comment(comment))
        } else if let Some(literal) = Literal::parse(parser)? {
            Some(Self::Literal(literal))
        } else if let Some(punctuator) = Punctuator::parse(parser)? {
            Some(Self::Punctuator(punctuator))
        } else if let Some(keyword) = Keyword::parse(parser)? {
            Some(Self::Keyword(keyword))
        } else if let Some(private_identifier) = PrivateIdentifier::parse(parser)? {
            Some(Self::PrivateIdentifier(private_identifier))
        } else {
            Identifier::parse(parser)?.map(Self::Identifier)
        };

        assert_eq!(
            parser.peek_offset, 0,
            "offset: {}\n peek_offset: {}\nres: {:#?}",
            parser.offset, parser.peek_offset, res
        );

        Ok(res)
    }
}

impl Parse for TokenTree {
    fn parse(parser: &mut Parser) -> Result<Option<Self>, ParserError> {
        let mut tokens = Vec::new();
        let mut prev_none = false;
        parser.nom_whitespace(true);

        loop {
            let pnone = prev_none;
            let mut prev_loc = parser.offset;
            while let Some(token) = Token::parse(parser)? {
                let loc = parser.offset;
                prev_none = false;

                let span = Span {
                    source_start: prev_loc,
                    source_end: loc,
                };

                parser.nom_whitespace(
                    matches!(token, Token::Comment(_))
                        || Parser::NEWLINE_ALLOWED_TOKENS.contains(&token)
                        || (matches!(token, Token::Identifier(_))
                            && Parser::NEWLINE_ALLOWED_IDENTIFIER_PREFIX_TOKENS.contains(&token)),
                );

                prev_loc = parser.offset;

                tokens.push(TokenWithSpan { token, span });
            }

            assert_eq!(
                parser.peek_offset, 0,
                "offset: {}\n peek_offset: {}\tokens: {:#?}",
                parser.offset, parser.peek_offset, tokens
            );

            parser.nom_whitespace(true);
            if parser.offset != parser.source.len() {
                match parser.peek_next_char() {
                    Some('}') | Some(')') | Some(']') => {
                        parser.unpeek(1);
                    }
                    Some(_) => {
                        parser.unpeek(1);

                        parser.dbgln(format!("Token Tree: {:#?}", tokens));

                        return Err(parser.raise_error(ParserErrorVariant::MissingSemicolon));
                    }
                    None => unreachable!(),
                };
            }

            if pnone && prev_none {
                break;
            }

            prev_none = true;
        }

        Ok(Some(Self::new(
            parser.file_path.clone(),
            parser.source.clone(),
            tokens,
        )))
    }
}
