pub mod assignment;
pub mod error;
pub mod expressions;
pub mod handler_function;
mod parent_objects;
pub mod statements;
pub mod variables;

use std::{cell::RefCell, rc::Rc};

use crate::ast::AssignmentExpression;
use indexmap::IndexMap;
use uuid::Uuid;

use crate::ast::{Function, NodeTree, NodeTreeHelper, PropertyNameValue};

use self::{
    error::InterpreterError,
    handler_function::RuntimeFuntion,
    variables::{
        Callable, InterpreterValue, Object, ValueOrRef, Variable, VariableStore, VariableType,
    },
};

pub struct Interpreter {
    pub tree: Rc<NodeTree>,
}

#[derive(Debug)]
pub struct InterpreterLayer {
    id: Uuid,
    pub this_binding: Option<Rc<Object>>,
    pub parent: Option<Rc<Self>>,
    pub variables: VariableStore,
    pub made_call: RefCell<bool>,
    pub call_target: bool,
}

impl PartialEq for InterpreterLayer {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

#[derive(Debug, Clone)]
pub enum State {
    ValueOrRef(ValueOrRef),
    Return(InterpreterValue),
}

impl State {
    pub fn into_value(self) -> InterpreterValue {
        match self {
            State::ValueOrRef(v) => v.into_value(),
            State::Return(v) => v,
        }
    }
}

impl InterpreterLayer {
    pub fn new(
        parent: Option<Rc<Self>>,
        this_binding: Option<Rc<Object>>,
        variables: Option<IndexMap<PropertyNameValue, Variable>>,
        call_target: bool,
    ) -> Rc<Self> {
        Rc::new(Self {
            parent,
            variables: VariableStore::new(variables),
            made_call: RefCell::new(false),
            call_target,
            id: Uuid::new_v4(),
            this_binding,
        })
    }

    pub fn get_made_call(self: &Rc<Self>) -> bool {
        *self.made_call.borrow()
    }

    pub fn set_made_call(self: &Rc<Self>, value: bool) {
        *self.made_call.borrow_mut() = value;
    }

    pub fn call_function(
        self: &Rc<Self>,
        callable: Callable,
        params: &[Rc<AssignmentExpression>],
    ) -> Result<InterpreterValue, InterpreterError> {
        match callable {
            Callable::Native { function, layer } => {
                self.call_native_function(layer.as_ref(), function, params)
            }
            Callable::Rust { handler } => self.call_rust_function(&handler, params),
        }
    }

    pub fn call_rust_function(
        self: &Rc<Self>,
        function: &Rc<Box<dyn RuntimeFuntion>>,
        params: &[Rc<AssignmentExpression>],
    ) -> Result<InterpreterValue, InterpreterError> {
        function.handle_call(params, self)
    }

    pub fn call_native_function(
        self: &Rc<Self>,
        parent: Option<&Rc<Self>>,
        function: Function,
        params: &[Rc<AssignmentExpression>],
    ) -> Result<InterpreterValue, InterpreterError> {
        let mut fparams = function.params.clone();
        let mut extra_expressions = Vec::new();
        let fparams_len = fparams.len();

        for (idx, param) in params.iter().enumerate() {
            if idx < fparams_len {
                fparams[idx].value = param.clone();
            } else {
                extra_expressions.push(param);
            }
        }

        let mut fparam_values = IndexMap::with_capacity(fparams.len());

        for p in fparams {
            let res = self.interpret_assignment_expression(p.value.as_ref())?;

            fparam_values.insert(
                PropertyNameValue::Identifier(p.ident),
                Variable::new(res.into_value(), VariableType::Var),
            );
        }

        self.set_made_call(true);

        let res = parent
            .unwrap_or(self)
            .new_child_layer(None, Some(fparam_values), true)
            .interpret_tree(function.body.child_tree)
            .map(State::into_value)?;

        self.set_made_call(false);

        Ok(res)
    }

    pub fn fetch_variable(self: &Rc<Self>, name: &PropertyNameValue) -> Option<Variable> {
        if let Some(e) = self.variables.get(name) {
            Some(e)
        } else if let Some(parent) = self.parent.as_ref() {
            parent.fetch_variable(name)
        } else {
            None
        }
    }

    pub fn update_variable(
        self: &Rc<Self>,
        name: &PropertyNameValue,
        value: InterpreterValue,
    ) -> Result<(), InterpreterError> {
        if self.variables.exists(name) {
            self.variables.update(name, value)?;
            Ok(())
        } else if let Some(parent) = self.parent.as_ref() {
            parent.update_variable(name, value)
        } else {
            Err(InterpreterError::UndefinedIdentifierReference(name.clone()))
        }
    }

    pub fn get_running_execution_scope(self: &Rc<Self>) -> Rc<Self> {
        match self.parent.as_ref() {
            Some(parent) if !self.call_target => parent.get_running_execution_scope(),
            _ => self.clone(),
        }
    }

    pub fn set_variable(
        self: &Rc<Self>,
        name: PropertyNameValue,
        variable: Variable,
    ) -> Result<(), InterpreterError> {
        if matches!(variable.variable_type(), VariableType::Var) {
            return self
                .get_running_execution_scope()
                .variables
                .set(name, variable);
        }

        self.variables.set(name, variable)?;

        Ok(())
    }

    pub fn new_child_layer(
        self: &Rc<Self>,
        this_binding: Option<Rc<Object>>,
        variables: Option<IndexMap<PropertyNameValue, Variable>>,
        call_target: bool,
    ) -> Rc<Self> {
        InterpreterLayer::new(Some(self.clone()), this_binding, variables, call_target)
    }

    pub fn interpret_tree(self: Rc<Self>, tree: Rc<NodeTree>) -> Result<State, InterpreterError> {
        let mut tree = NodeTreeHelper::new(tree);

        while let Some(node) = tree.next_node() {
            match node {
                crate::ast::Node::Expression(expression) => {
                    let res = self.interpret_expression(expression)?.into_value();
                    if !tree.more_nodes() {
                        return Ok(State::ValueOrRef(ValueOrRef::Value(res)));
                    }
                }
                crate::ast::Node::Statement(statement) => {
                    match self.interpret_statement(statement.clone())? {
                        State::ValueOrRef(value) => {
                            if !tree.more_nodes() {
                                return Ok(State::ValueOrRef(ValueOrRef::Value(
                                    value.into_value(),
                                )));
                            }
                        }
                        State::Return(value) => match self.get_made_call() {
                            true => {
                                self.set_made_call(false);
                            }
                            false => return Ok(State::Return(value)),
                        },
                    }
                }
            }
        }

        Ok(State::ValueOrRef(ValueOrRef::Value(
            InterpreterValue::Undefined,
        )))
    }
}

impl Interpreter {
    pub fn new(tree: Rc<NodeTree>) -> Self {
        Self { tree }
    }

    pub fn run(self) -> Result<InterpreterValue, InterpreterError> {
        let global = Rc::new(parent_objects::global_object()?);
        let console = parent_objects::console_object(global.clone());
        let property_name_value = PropertyNameValue::String(Rc::new(String::from("console")));
        global.set(
            property_name_value.clone(),
            InterpreterValue::Object(console),
        );
        global.write_ignore(property_name_value);

        let layer = InterpreterLayer::new(
            None,
            Some(global.clone()),
            Some(global.as_ref().to_variable_map()),
            true,
        );
        let interpret_res = layer.interpret_tree(self.tree)?;

        Ok(interpret_res.into_value())
    }
}
