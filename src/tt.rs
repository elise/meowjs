use std::{path::PathBuf, rc::Rc};

use num::BigInt;

use crate::parser::Comment;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Symbol {
    AsyncIterator,
    HasInstance,
    IsConcatSpreadable,
    Iterator,
    Match,
    MatchAll,
    Replace,
    Search,
    Species,
    Split,
    ToPrimitive,
    ToStringTag,
    Unscopables,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Keyword {
    Await,
    Break,
    Case,
    Catch,
    Class,
    Const,
    Continue,
    Debugger,
    Default,
    Delete,
    Do,
    Else,
    Enum,
    Export,
    Extends,
    False,
    Finally,
    For,
    Function,
    If,
    Import,
    In,
    Instanceof,
    New,
    Return,
    Super,
    Switch,
    This,
    Throw,
    True,
    Try,
    Typeof,
    Var,
    Void,
    While,
    With,
    Yeild,
    Let,
    Static,
    Implements,
    Interface,
    Package,
    Private,
    Protected,
    Public,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Punctuator {
    /// `{`
    LeftBrace,
    /// `}`
    RightBrace,
    /// `(`
    LeftBracket,
    /// `)`
    RightBracket,
    /// `[`
    LeftSquareBracket,
    /// `]`
    RightSquareBracket,
    /// `.`
    Dot,
    /// `...`
    Spread,
    /// `;`
    Semicolon,
    /// `,`
    Comma,
    /// `<`
    LessThan,
    /// `>`
    GreaterThan,
    /// `<=`
    LessThanEqual,
    /// `>=`
    GreaterThanEqual,
    /// `==`
    Equality,
    /// `!=`
    Inequality,
    /// `===`
    StrictEquality,
    /// `!==`
    StrictInequality,
    /// `+`
    Addition,
    /// `-`
    Subtraction,
    /// `*`
    Multiplication,
    /// `%`
    Remainder,
    /// `**`
    Exponentiation,
    /// `++`
    Increment,
    /// `--`
    Decrement,
    /// `<<`
    LeftShift,
    /// `>>`
    RightShift,
    /// `>>>`
    UnsignedRightShift,
    /// `&`
    BitwiseAnd,
    /// `|`
    BitwiseOr,
    /// `^`
    BitwiseXor,
    /// `!`
    LogicalNot,
    /// `~`
    BitwiseNot,
    /// `&&`
    LogicalAnd,
    /// `||`
    LogicalOr,
    /// `??`
    NullishCoalescing,
    /// `?`
    Conditional,
    /// `?.`
    OptionalChaining,
    /// `:`
    Colon,
    /// `=`
    Assignment,
    /// `+=`
    AdditionAssignment,
    /// `-=`
    SubtractionAssignment,
    /// `*=`
    MultiplicationAssignment,
    /// `%=`
    RemainderAssignment,
    /// `**=`
    ExponentiationAssignment,
    /// `<<=`
    LeftShiftAssignment,
    /// `>>=`
    RightShiftAssignment,
    /// `>>>=`
    UnsignedRightShiftAssignment,
    /// `&=`
    BitwiseAndAssignment,
    /// `|=`
    BitwiseOrAssignment,
    /// `^=`
    BitwiseXorAssignment,
    /// `&&=`
    LogicalAndAssignment,
    /// `||=`
    LogicalOrAssignment,
    /// `??=`
    NullishCoalescingAssignment,
    /// `=>`
    Arrow,
    /// `/`
    Division,
    /// `/=`
    DivisionAssignment,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Literal {
    Undefined,
    Null,
    Boolean(bool),
    String(String),
    Numeric(f64),
    BigInt(BigInt),
    /// Unsupported
    Regex,
    /// Unsupported
    TemplateString,
}

impl Default for Literal {
    fn default() -> Self {
        Self::Undefined
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct PrivateIdentifier(pub Rc<String>);

impl PrivateIdentifier {
    pub fn validate_char(idx: usize, c: char) -> bool {
        (idx == 0 && c == '#')
            || (idx != 0 && c == '$')
            || (idx == 1 && (c == '_' || unicode_id::UnicodeID::is_id_start(c)))
            || (idx > 1
                && (unicode_id::UnicodeID::is_id_continue(c) || /* ZWNJ */ c == '\u{200C}' || /* ZWJ */ c == '\u{200D}'))
    }

    pub fn new(input: Rc<String>) -> Option<Self> {
        for (i, c) in input.chars().enumerate() {
            if !Self::validate_char(i, c) {
                return None;
            }
        }

        Some(Self(input))
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Identifier(pub Rc<String>);

impl Identifier {
    pub fn validate_char(idx: usize, c: char) -> bool {
        c == '$'
            || (idx == 0 && (c == '_' || unicode_id::UnicodeID::is_id_start(c)))
            || (idx != 0
                && (unicode_id::UnicodeID::is_id_continue(c) || /* ZWNJ */ c == '\u{200C}' || /* ZWJ */ c == '\u{200D}'))
    }

    pub fn new(input: Rc<String>) -> Option<Self> {
        for (i, c) in input.chars().enumerate() {
            if !Self::validate_char(i, c) {
                return None;
            }
        }

        Some(Self(input))
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum PeekUntilError {
    NotFound,
    DisallowedToken,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Token {
    Comment(Comment),
    Literal(Literal),
    Punctuator(Punctuator),
    Keyword(Keyword),
    PrivateIdentifier(PrivateIdentifier),
    Identifier(Identifier),
}

#[derive(Debug, Clone, PartialEq)]
pub struct TokenWithSpan {
    pub token: Token,
    pub span: Span,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Default)]
pub struct Span {
    pub source_start: usize,
    pub source_end: usize,
}

#[derive(Debug, Clone, PartialEq)]
pub struct TokenTree {
    pub file_path: Option<Rc<PathBuf>>,
    pub source: Rc<String>,
    pub tree: Vec<TokenWithSpan>,
    pub end: usize,
    pub offset: usize,
    pub peek_offset: usize,
}

impl TokenTree {
    pub fn new(
        file_path: Option<Rc<PathBuf>>,
        source: Rc<String>,
        tree: Vec<TokenWithSpan>,
    ) -> Self {
        let end = tree.len();
        Self {
            file_path,
            source,
            tree,
            offset: 0,
            peek_offset: 0,
            end,
        }
    }

    pub fn peek_next(&mut self) -> Option<TokenWithSpan> {
        let idx = self.offset + self.peek_offset;

        match idx < self.end {
            true => {
                self.peek_offset += 1;
                Some(self.tree[idx].clone())
            }
            false => None,
        }
    }

    pub fn peek_prev(&mut self) -> Option<TokenWithSpan> {
        let idx = self.offset + self.peek_offset;

        match idx < self.end {
            true => Some(self.tree[idx].clone()),
            false => None,
        }
    }

    pub fn remaining(&self) -> &[TokenWithSpan] {
        let idx = self.offset + self.peek_offset;

        match idx < self.end {
            true => &self.tree[idx..],
            false => &[],
        }
    }

    pub fn peek_is_next(&mut self, token: &Token) -> bool {
        let idx = self.offset + self.peek_offset;

        if idx < self.end {
            let tree_token_ref = &self.tree[idx];

            if &tree_token_ref.token == token {
                self.peek_offset += 1;
                return true;
            }
        }

        false
    }

    pub fn peek_is_next_span(&mut self, token: &Token) -> Option<Span> {
        let idx = self.offset + self.peek_offset;

        if idx < self.end {
            let tree_token_ref = &self.tree[idx];

            if &tree_token_ref.token == token {
                self.peek_offset += 1;
                return Some(tree_token_ref.span);
            }
        }

        None
    }

    pub fn peek_until(
        &mut self,
        token: &Token,
        decrease_depth: Option<&Token>,
        disallowed: &[&Token],
    ) -> Result<Vec<TokenWithSpan>, PeekUntilError> {
        let mut idx = self.offset + self.peek_offset;

        let mut depth = 0u32;
        let mut out = vec![];
        while idx < self.end {
            let tree_token_ref = &self.tree[idx];
            self.peek_offset += 1;
            if &tree_token_ref.token == token {
                if depth == 0 {
                    return Ok(out);
                }

                depth -= 1;
            } else if decrease_depth
                .map(|decrease_token| decrease_token == &tree_token_ref.token)
                .unwrap_or_default()
            {
                depth += 1;
            } else if disallowed.contains(&&tree_token_ref.token) {
                self.unpeek(idx + 1);
                return Err(PeekUntilError::DisallowedToken);
            }
            out.push(tree_token_ref.clone());
            idx += 1;
        }

        self.unpeek(idx);
        Err(PeekUntilError::NotFound)
    }

    pub fn unpeek(&mut self, count: usize) {
        self.peek_offset -= count;
    }

    pub fn commit_peek(&mut self) {
        self.offset += self.peek_offset;
        self.peek_offset = 0;
    }

    pub fn reset_peek(&mut self) {
        self.peek_offset = 0;
    }
}
