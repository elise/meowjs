#!/bin/sh
# This is obviously non-representitive due to V8 using a JIT but it is still fun to look at

for f in ./test-js/*.js
do
  echo "\n--- Testing $f ---"
  ORIG_RUST_LOG=$RUST_LOG
  export RUST_LOG="Error"
  echo "Node: "
  time node $f
  echo "\nmeowjs:"
  time meowjs $f
  export RUST_LOG=$ORIG_RUST_LOG
done
