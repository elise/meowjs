let two = 1 + 1;
let five = 3 * 2 - 1;
let seven = 1 + 3 * 2;
let eight = 2 * (1 + 3);
let nine_two_two_nine = (6 + 7) + 2 * 20 / (5 * 3) * 3456;

console.log("Two:", two, "\nFive:", five, "\nSeven:", seven, "\nEight:", eight, "\nNine-thousand-two-hundred-twenty-nine:", nine_two_two_nine);
return two == 2 && five == 5 && seven == 7 && eight == 8 && nine_two_two_nine == 9229;