function print_number(iter, value) {
    console.log("Fibbonaci number", (iter + 1) + ":", value);
}

function fibbonaci_iter(max, iter, prev1, prev2) {
    if (iter >= max) {
        return prev1 + prev2;
    }

    let next = prev1 + prev2;
    print_number(iter, next);
    return fibbonaci_iter(max, iter + 1, prev2, next);
}

function fibbonaci(max=10) {
    if (max > 0) {
        print_number(0, 0);
    }

    if (max > 1) {
        print_number(1, 1);
    }

    return fibbonaci_iter(max, 2, 0, 1);
}

fibbonaci();
