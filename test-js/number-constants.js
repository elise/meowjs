if (Infinity != Infinity) {
    return "Error: Infinity != Infinity";
} else if (NaN == NaN) {
    return "Error: NaN == NaN";
}

return Infinity;