function meow() {
    const a = "Meow from meow2 inside scope of meow";

    function meow2() {
        return a;
    }

    return meow2;
}

var meow_ref = meow;
return meow_ref()();
