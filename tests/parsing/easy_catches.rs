use std::rc::Rc;

use meowjs::{
    parser::Comment,
    tt::{Identifier, Literal, Punctuator, Token},
};

use crate::{parse_eq_test, parsing::span};

parse_eq_test!(
    lit_one_decrement_lit_one,
    "1--1",
    Ok(vec![
        span(Token::Literal(Literal::Numeric(1f64)), 0, 1),
        span(Token::Punctuator(Punctuator::Decrement), 1, 3),
        span(Token::Literal(Literal::Numeric(1f64)), 3, 4),
    ])
);

parse_eq_test!(
    lit_one_sub_ml_comment_neg_signed_lit_one,
    "1-/* Meow */-1",
    Ok(vec![
        span(Token::Literal(Literal::Numeric(1f64)), 0, 1),
        span(Token::Punctuator(Punctuator::Subtraction), 1, 2),
        span(
            Token::Comment(Comment::MultiLine(String::from(" Meow "))),
            2,
            12
        ),
        span(Token::Punctuator(Punctuator::Subtraction), 12, 13),
        span(Token::Literal(Literal::Numeric(1f64)), 13, 14),
    ])
);

parse_eq_test!(
    lit_one_increment_lit_one,
    "1++1",
    Ok(vec![
        span(Token::Literal(Literal::Numeric(1f64)), 0, 1),
        span(Token::Punctuator(Punctuator::Increment), 1, 3),
        span(Token::Literal(Literal::Numeric(1f64)), 3, 4)
    ])
);

parse_eq_test!(
    lit_one_add_ml_comment_pos_signed_lit_one,
    "1+/* Meow */+1",
    Ok(vec![
        span(Token::Literal(Literal::Numeric(1f64)), 0, 1),
        span(Token::Punctuator(Punctuator::Addition), 1, 2),
        span(
            Token::Comment(Comment::MultiLine(String::from(" Meow "))),
            2,
            12
        ),
        span(Token::Punctuator(Punctuator::Addition), 12, 13),
        span(Token::Literal(Literal::Numeric(1f64)), 13, 14),
    ])
);

parse_eq_test!(
    lit_one_subtract_pos_signed_lit_one,
    "1-+1",
    Ok(vec![
        span(Token::Literal(Literal::Numeric(1f64)), 0, 1),
        span(Token::Punctuator(Punctuator::Subtraction), 1, 2),
        span(Token::Punctuator(Punctuator::Addition), 2, 3),
        span(Token::Literal(Literal::Numeric(1f64)), 3, 4),
    ])
);

parse_eq_test!(
    lit_one_add_neg_signed_lit_one,
    "1+-1",
    Ok(vec![
        span(Token::Literal(Literal::Numeric(1.0)), 0, 1),
        span(Token::Punctuator(Punctuator::Addition), 1, 2),
        span(Token::Punctuator(Punctuator::Subtraction), 2, 3),
        span(Token::Literal(Literal::Numeric(1f64)), 3, 4),
    ])
);

parse_eq_test!(
    ident_starting_with_keyword,
    "awaitmeow",
    Ok(vec![span(
        Token::Identifier(Identifier(Rc::new(String::from("awaitmeow")))),
        0,
        9
    )])
);
