use meowjs::{
    parser::ParserErrorVariant,
    tt::{Literal, Punctuator, Token},
};

use crate::parsing::{error, span};

crate::parse_eq_test!(
    literal_number_exponent_missing_rs,
    "1.2e",
    error(ParserErrorVariant::NumericLiteralExponentMissingRs)
);

crate::parse_eq_test!(
    literal_number_bin_prefix,
    "0b0101",
    Ok(vec![span(
        Token::Literal(Literal::Numeric(0b0101u32 as f64)),
        0,
        6
    )])
);

crate::parse_eq_test!(
    literal_negative_number_bin_prefix,
    "-0b0101",
    Ok(vec![
        span(Token::Punctuator(Punctuator::Subtraction), 0, 1),
        span(Token::Literal(Literal::Numeric(0b0101u32 as f64)), 1, 7)
    ])
);
crate::parse_eq_test!(
    literal_number_bin_prefix_missing_numbers,
    "0b",
    error(ParserErrorVariant::NumericLiteralPrefixMissingNumbers)
);
crate::parse_eq_test!(
    literal_number_capital_bin_prefix_missing_numbers,
    "0B",
    error(ParserErrorVariant::NumericLiteralPrefixMissingNumbers)
);
crate::parse_eq_test!(
    literal_number_hex_prefix,
    "0xDEAD",
    Ok(vec![span(
        Token::Literal(Literal::Numeric(0xDEADu32 as f64)),
        0,
        6
    )])
);
crate::parse_eq_test!(
    literal_number_hex_prefix_missing_numbers,
    "0x",
    error(ParserErrorVariant::NumericLiteralPrefixMissingNumbers)
);
crate::parse_eq_test!(
    literal_number_capital_hex_prefix_missing_numbers,
    "0X",
    error(ParserErrorVariant::NumericLiteralPrefixMissingNumbers)
);
crate::parse_eq_test!(
    literal_number_oct_prefix_missing_numbers,
    "0o",
    error(ParserErrorVariant::NumericLiteralPrefixMissingNumbers)
);
crate::parse_eq_test!(
    literal_number_capital_oct_prefix_missing_numbers,
    "0O",
    error(ParserErrorVariant::NumericLiteralPrefixMissingNumbers)
);
