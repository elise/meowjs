use meowjs::{
    parser::{Comment, ParserErrorVariant},
    tt::{Span, Token, TokenWithSpan},
};

use crate::{parse_eq_test, parsing::error};

parse_eq_test!(
    literal_single_line_comment,
    "// Meow",
    Ok(vec![TokenWithSpan {
        token: Token::Comment(Comment::SingleLine(String::from(" Meow"))),
        span: Span {
            source_start: 0,
            source_end: 7
        }
    }])
);
parse_eq_test!(
    literal_multi_line_comment,
    "/* Meow */",
    Ok(vec![TokenWithSpan {
        token: Token::Comment(Comment::MultiLine(String::from(" Meow "))),
        span: Span {
            source_start: 0,
            source_end: 10
        }
    }])
);
parse_eq_test!(
    literal_never_ending_multi_line_comment,
    "/* Meow",
    error(ParserErrorVariant::MultiLineCommentMissingClosing)
);

parse_eq_test!(
    multi_line_comment_2,
    "/* Meow\nMeow */",
    Ok(vec![TokenWithSpan {
        token: Token::Comment(Comment::MultiLine(String::from(" Meow\nMeow ",))),
        span: Span {
            source_start: 0,
            source_end: 15
        },
    }])
);
