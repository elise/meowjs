use meowjs::{
    parser::{ParserError, ParserErrorVariant},
    tt::{Span, Token, TokenWithSpan},
};

pub mod assignments;
pub mod comments;
pub mod easy_catches;
pub mod literals;

#[macro_export(crate)]
macro_rules! parse_eq_test {
    ($name:ident, $script:literal, $expected:expr) => {
        #[test]
        pub fn $name() {
            {
                use meowjs::parser::Parse;
                assert_eq!(
                    meowjs::tt::TokenTree::parse(&mut meowjs::parser::Parser::new(None, $script))
                        .map(Option::unwrap),
                    $expected.map(|x| meowjs::tt::TokenTree::new(
                        None,
                        std::rc::Rc::new(String::from($script)),
                        x
                    ))
                );
            }
        }
    };
}

fn error(v: ParserErrorVariant) -> Result<Vec<TokenWithSpan>, ParserError> {
    Err(ParserError::new(1, 1, v))
}

fn span(token: Token, start: usize, end: usize) -> TokenWithSpan {
    TokenWithSpan {
        token,
        span: Span {
            source_start: start,
            source_end: end,
        },
    }
}
