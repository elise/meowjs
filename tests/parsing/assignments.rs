use std::rc::Rc;

use meowjs::tt::{Identifier, Keyword, Literal, PrivateIdentifier, Punctuator, Token};
use num::{BigInt, FromPrimitive};

use crate::{parse_eq_test, parsing::span};

parse_eq_test!(
    bigint_decimal_assignment,
    "var meow = 123420n;",
    Ok(vec![
        span(Token::Keyword(Keyword::Var), 0, 3),
        span(
            Token::Identifier(Identifier(Rc::new(String::from("meow")))),
            4,
            8
        ),
        span(Token::Punctuator(Punctuator::Assignment), 9, 10),
        span(
            Token::Literal(Literal::BigInt(BigInt::from_u32(123420).unwrap())),
            11,
            18
        ),
        span(Token::Punctuator(Punctuator::Semicolon), 18, 19),
    ])
);

parse_eq_test!(
    signed_positive_bigint_decimal_assignment,
    "var meow = +123420n;",
    Ok(vec![
        span(Token::Keyword(Keyword::Var), 0, 3),
        span(
            Token::Identifier(Identifier(Rc::new(String::from("meow")))),
            4,
            8
        ),
        span(Token::Punctuator(Punctuator::Assignment), 9, 10),
        span(Token::Punctuator(Punctuator::Addition), 11, 12),
        span(
            Token::Literal(Literal::BigInt(BigInt::from_u32(123420).unwrap())),
            12,
            19
        ),
        span(Token::Punctuator(Punctuator::Semicolon), 19, 20),
    ])
);

parse_eq_test!(
    signed_negative_bigint_decimal_assignment,
    "var meow = -123420n;",
    Ok(vec![
        span(Token::Keyword(Keyword::Var), 0, 3),
        span(
            Token::Identifier(Identifier(Rc::new(String::from("meow")))),
            4,
            8
        ),
        span(Token::Punctuator(Punctuator::Assignment), 9, 10),
        span(Token::Punctuator(Punctuator::Subtraction), 11, 12),
        span(
            Token::Literal(Literal::BigInt(BigInt::from_u32(123420).unwrap())),
            12,
            19
        ),
        span(Token::Punctuator(Punctuator::Semicolon), 19, 20),
    ])
);

parse_eq_test!(
    numeric_decimal_assignment,
    "var meow = 0;",
    Ok(vec![
        span(Token::Keyword(Keyword::Var), 0, 3),
        span(
            Token::Identifier(Identifier(Rc::new(String::from("meow")))),
            4,
            8
        ),
        span(Token::Punctuator(Punctuator::Assignment), 9, 10),
        span(Token::Literal(Literal::Numeric(0.0)), 11, 12),
        span(Token::Punctuator(Punctuator::Semicolon), 12, 13),
    ])
);

parse_eq_test!(
    signed_positive_numeric_decimal_assignment,
    "var meow = +1;",
    Ok(vec![
        span(Token::Keyword(Keyword::Var), 0, 3),
        span(
            Token::Identifier(Identifier(Rc::new(String::from("meow")))),
            4,
            8
        ),
        span(Token::Punctuator(Punctuator::Assignment), 9, 10),
        span(Token::Punctuator(Punctuator::Addition), 11, 12),
        span(Token::Literal(Literal::Numeric(1.0)), 12, 13),
        span(Token::Punctuator(Punctuator::Semicolon), 13, 14),
    ])
);

parse_eq_test!(
    signed_negative_numeric_decimal_assignment,
    "var meow = -1;",
    Ok(vec![
        span(Token::Keyword(Keyword::Var), 0, 3),
        span(
            Token::Identifier(Identifier(Rc::new(String::from("meow")))),
            4,
            8
        ),
        span(Token::Punctuator(Punctuator::Assignment), 9, 10),
        span(Token::Punctuator(Punctuator::Subtraction), 11, 12),
        span(Token::Literal(Literal::Numeric(1.0)), 12, 13),
        span(Token::Punctuator(Punctuator::Semicolon), 13, 14),
    ])
);

parse_eq_test!(
    numeric_float_assignment,
    "var meow = 123.456;",
    Ok(vec![
        span(Token::Keyword(Keyword::Var), 0, 3),
        span(
            Token::Identifier(Identifier(Rc::new(String::from("meow")))),
            4,
            8
        ),
        span(Token::Punctuator(Punctuator::Assignment), 9, 10),
        span(Token::Literal(Literal::Numeric(123.456)), 11, 18),
        span(Token::Punctuator(Punctuator::Semicolon), 18, 19),
    ])
);

parse_eq_test!(
    numeric_exponent_assignment,
    "var meow = 123.456e2;",
    Ok(vec![
        span(Token::Keyword(Keyword::Var), 0, 3),
        span(
            Token::Identifier(Identifier(Rc::new(String::from("meow")))),
            4,
            8
        ),
        span(Token::Punctuator(Punctuator::Assignment), 9, 10),
        span(Token::Literal(Literal::Numeric(12345.6)), 11, 20),
        span(Token::Punctuator(Punctuator::Semicolon), 20, 21),
    ])
);

parse_eq_test!(
    numeric_negative_exponent_assignment,
    "var meow = 123.456e-2;",
    Ok(vec![
        span(Token::Keyword(Keyword::Var), 0, 3),
        span(
            Token::Identifier(Identifier(Rc::new(String::from("meow")))),
            4,
            8
        ),
        span(Token::Punctuator(Punctuator::Assignment), 9, 10),
        span(Token::Literal(Literal::Numeric(1.23456)), 11, 21),
        span(Token::Punctuator(Punctuator::Semicolon), 21, 22)
    ])
);

parse_eq_test!(
    signed_positive_numeric_negative_exponent_assignment,
    "var meow = +123.456e-2;",
    Ok(vec![
        span(Token::Keyword(Keyword::Var), 0, 3),
        span(
            Token::Identifier(Identifier(Rc::new(String::from("meow")))),
            4,
            8
        ),
        span(Token::Punctuator(Punctuator::Assignment), 9, 10),
        span(Token::Punctuator(Punctuator::Addition), 11, 12),
        span(Token::Literal(Literal::Numeric(1.23456)), 12, 22),
        span(Token::Punctuator(Punctuator::Semicolon), 22, 23)
    ])
);

parse_eq_test!(
    signed_negative_numeric_negative_exponent_assignment,
    "var meow = -123.456e-2;",
    Ok(vec![
        span(Token::Keyword(Keyword::Var), 0, 3),
        span(
            Token::Identifier(Identifier(Rc::new(String::from("meow")))),
            4,
            8
        ),
        span(Token::Punctuator(Punctuator::Assignment), 9, 10),
        span(Token::Punctuator(Punctuator::Subtraction), 11, 12),
        span(Token::Literal(Literal::Numeric(1.23456)), 12, 22),
        span(Token::Punctuator(Punctuator::Semicolon), 22, 23)
    ])
);

parse_eq_test!(
    underscore_variable_name,
    "var _test_ = 0;",
    Ok(vec![
        span(Token::Keyword(Keyword::Var), 0, 3),
        span(
            Token::Identifier(Identifier(Rc::new(String::from("_test_")))),
            4,
            10
        ),
        span(Token::Punctuator(Punctuator::Assignment), 11, 12),
        span(Token::Literal(Literal::Numeric(0.0)), 13, 14),
        span(Token::Punctuator(Punctuator::Semicolon), 14, 15)
    ])
);

parse_eq_test!(
    dollar_variable_name,
    "var $$test = 0;",
    Ok(vec![
        span(Token::Keyword(Keyword::Var), 0, 3),
        span(
            Token::Identifier(Identifier(Rc::new(String::from("$$test")))),
            4,
            10
        ),
        span(Token::Punctuator(Punctuator::Assignment), 11, 12),
        span(Token::Literal(Literal::Numeric(0.0)), 13, 14),
        span(Token::Punctuator(Punctuator::Semicolon), 14, 15),
    ])
);

parse_eq_test!(
    private_identifier,
    "var #privident = 0;",
    Ok(vec![
        span(Token::Keyword(Keyword::Var), 0, 3),
        span(
            Token::PrivateIdentifier(PrivateIdentifier(Rc::new(String::from("privident")))),
            4,
            14
        ),
        span(Token::Punctuator(Punctuator::Assignment), 15, 16),
        span(Token::Literal(Literal::Numeric(0.0)), 17, 18),
        span(Token::Punctuator(Punctuator::Semicolon), 18, 19),
    ])
);

parse_eq_test!(
    single_quote_string_assignment,
    "var meow = 'meow';",
    Ok(vec![
        span(Token::Keyword(Keyword::Var), 0, 3),
        span(
            Token::Identifier(Identifier(Rc::new(String::from("meow")))),
            4,
            8
        ),
        span(Token::Punctuator(Punctuator::Assignment), 9, 10),
        span(
            Token::Literal(Literal::String(String::from("meow"))),
            11,
            17
        ),
        span(Token::Punctuator(Punctuator::Semicolon), 17, 18),
    ])
);

parse_eq_test!(
    double_quote_string_assignment,
    r#"var meow = "meow";"#,
    Ok(vec![
        span(Token::Keyword(Keyword::Var), 0, 3),
        span(
            Token::Identifier(Identifier(Rc::new(String::from("meow")))),
            4,
            8
        ),
        span(Token::Punctuator(Punctuator::Assignment), 9, 10),
        span(
            Token::Literal(Literal::String(String::from("meow"))),
            11,
            17
        ),
        span(Token::Punctuator(Punctuator::Semicolon), 17, 18)
    ])
);

parse_eq_test!(
    function_assignment,
    "var meow = function() { return 'meow' };",
    Ok(vec![
        span(Token::Keyword(Keyword::Var), 0, 3),
        span(
            Token::Identifier(Identifier(Rc::new(String::from("meow")))),
            4,
            8
        ),
        span(Token::Punctuator(Punctuator::Assignment), 9, 10),
        span(Token::Keyword(Keyword::Function), 11, 19),
        span(Token::Punctuator(Punctuator::LeftBracket), 19, 20),
        span(Token::Punctuator(Punctuator::RightBracket), 20, 21),
        span(Token::Punctuator(Punctuator::LeftBrace), 22, 23),
        span(Token::Keyword(Keyword::Return), 24, 30),
        span(
            Token::Literal(Literal::String(String::from("meow"))),
            31,
            37
        ),
        span(Token::Punctuator(Punctuator::RightBrace), 38, 39),
        span(Token::Punctuator(Punctuator::Semicolon), 39, 40)
    ])
);

parse_eq_test!(
    arrow_function_assignment,
    "var meow = () => { return 'meow' };",
    Ok(vec![
        span(Token::Keyword(Keyword::Var), 0, 3),
        span(
            Token::Identifier(Identifier(Rc::new(String::from("meow")))),
            4,
            8
        ),
        span(Token::Punctuator(Punctuator::Assignment), 9, 10),
        span(Token::Punctuator(Punctuator::LeftBracket), 11, 12),
        span(Token::Punctuator(Punctuator::RightBracket), 12, 13),
        span(Token::Punctuator(Punctuator::Arrow), 14, 16),
        span(Token::Punctuator(Punctuator::LeftBrace), 17, 18),
        span(Token::Keyword(Keyword::Return), 19, 25),
        span(
            Token::Literal(Literal::String(String::from("meow"))),
            26,
            32
        ),
        span(Token::Punctuator(Punctuator::RightBrace), 33, 34),
        span(Token::Punctuator(Punctuator::Semicolon), 34, 35),
    ])
);
