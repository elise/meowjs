use meowjs::{
    ast::{AssignmentOperator, AstValue, Node, NodeParse, NodeTree},
    tt::{Identifier, Literal, Punctuator, Span, Token, TokenTree},
};

macro_rules! assignment_test {
    ($name:ident, $punctuator:expr, $op:expr, $astval:expr, $literal:expr) => {
        #[test]
        pub fn $name() {
            let identifier = Identifier(std::rc::Rc::new(String::from("x")));
            let source = std::rc::Rc::new(String::from("this is a test and there is no source"));
            let expected: NodeTree = NodeTree {
                source: source.clone(),
                file_path: None,
                nodes: vec![Node::Expression(std::rc::Rc::new(
                    meowjs::ast::Expression::Assignment(meowjs::ast::AssignmentExpression::Assign(
                        meowjs::ast::LeftSideExpression::New(meowjs::ast::NewExpression::Member(
                            meowjs::ast::MemberExpression::Primary(
                                meowjs::ast::PrimaryExpression::Identifier(identifier.clone()),
                            ),
                        )),
                        $op,
                        std::rc::Rc::new(meowjs::ast::AssignmentExpression::from($astval)),
                    )),
                ))],
            };
            let span = Span {
                source_start: 0,
                source_end: source.len(),
            };
            let v = vec![
                meowjs::tt::TokenWithSpan {
                    token: Token::Identifier(identifier),
                    span,
                },
                meowjs::tt::TokenWithSpan {
                    token: Token::Punctuator($punctuator),
                    span,
                },
                meowjs::tt::TokenWithSpan {
                    token: Token::Literal($literal),
                    span,
                },
            ];
            let mut token_tree: TokenTree = TokenTree::new(None, source, v);

            assert_eq!(NodeTree::node_parse(&mut token_tree), Ok(expected));
        }
    };
}

assignment_test!(
    test_assignment,
    Punctuator::Assignment,
    AssignmentOperator::Assign,
    AstValue::Numeric(123f64),
    Literal::Numeric(123f64)
);

assignment_test!(
    test_add_assignment,
    Punctuator::AdditionAssignment,
    AssignmentOperator::Addition,
    AstValue::Numeric(123f64),
    Literal::Numeric(123f64)
);

assignment_test!(
    test_sub_assignment,
    Punctuator::SubtractionAssignment,
    AssignmentOperator::Subtraction,
    AstValue::Numeric(123f64),
    Literal::Numeric(123f64)
);

assignment_test!(
    test_mul_assignment,
    Punctuator::MultiplicationAssignment,
    AssignmentOperator::Multiply,
    AstValue::Numeric(123f64),
    Literal::Numeric(123f64)
);

assignment_test!(
    test_div_assignment,
    Punctuator::DivisionAssignment,
    AssignmentOperator::Divide,
    AstValue::Numeric(123f64),
    Literal::Numeric(123f64)
);

assignment_test!(
    test_mod_assignment,
    Punctuator::RemainderAssignment,
    AssignmentOperator::Remainder,
    AstValue::Numeric(123f64),
    Literal::Numeric(123f64)
);

assignment_test!(
    test_lshift_assignment,
    Punctuator::LeftShiftAssignment,
    AssignmentOperator::LeftShift,
    AstValue::Numeric(123f64),
    Literal::Numeric(123f64)
);

assignment_test!(
    test_rshift_assignment,
    Punctuator::RightShiftAssignment,
    AssignmentOperator::RightShift,
    AstValue::Numeric(123f64),
    Literal::Numeric(123f64)
);

assignment_test!(
    test_urshift_assignment,
    Punctuator::UnsignedRightShiftAssignment,
    AssignmentOperator::UnsignedRightShift,
    AstValue::Numeric(123f64),
    Literal::Numeric(123f64)
);

assignment_test!(
    test_bitwise_and_assignment,
    Punctuator::BitwiseAndAssignment,
    AssignmentOperator::And,
    AstValue::Numeric(123f64),
    Literal::Numeric(123f64)
);

assignment_test!(
    test_bitwise_xor_assignment,
    Punctuator::BitwiseXorAssignment,
    AssignmentOperator::Xor,
    AstValue::Numeric(123f64),
    Literal::Numeric(123f64)
);

assignment_test!(
    test_bitwise_or_assignment,
    Punctuator::BitwiseOrAssignment,
    AssignmentOperator::Or,
    AstValue::Numeric(123f64),
    Literal::Numeric(123f64)
);

assignment_test!(
    test_logical_and_assignment,
    Punctuator::LogicalAndAssignment,
    AssignmentOperator::LogicalAnd,
    AstValue::Numeric(123f64),
    Literal::Numeric(123f64)
);

assignment_test!(
    test_logical_or_assignment,
    Punctuator::LogicalOrAssignment,
    AssignmentOperator::LogicalOr,
    AstValue::Numeric(123f64),
    Literal::Numeric(123f64)
);

assignment_test!(
    test_coalescing_assignment,
    Punctuator::NullishCoalescingAssignment,
    AssignmentOperator::Coalesce,
    AstValue::Numeric(123f64),
    Literal::Numeric(123f64)
);
